﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindManager : MonoBehaviour {
    public static WindManager instance;
    WindZone wind;
    public float globalWindMultiplier;
    public float windNoise = 10;
    AudioSource audioSource;

    public float updateStep = 0.1f;
    public int sampleDataLength = 1024;

    private float currentUpdateTime = 0f;

    public float clipLoudness;
    private float[] clipSampleData;

    float windValue;

    void Awake()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();

        clipSampleData = new float[sampleDataLength];
    }
    void Start() {
        wind = GetComponent<WindZone>();
    }

    void Update() {
        PollAudioClip();
        wind.windMain = windValue; //curve.Evaluate((Time.time * animPlaySpeed) % 1f);
    }

    public static Vector3 GetCurrentWind()
    {
        Vector3 temp = instance.transform.forward * instance.windValue;//instance.curve.Evaluate((Time.time * instance.animPlaySpeed) % 1f);
        return temp;
    }

    void PollAudioClip()
    {
        currentUpdateTime += Time.deltaTime;
        if (currentUpdateTime >= updateStep)
        {
            currentUpdateTime = 0f;
            audioSource.clip.GetData(clipSampleData, audioSource.timeSamples); //I read 1024 samples, which is about 80 ms on a 44khz stereo clip, beginning at the current sample position of the clip.
            clipLoudness = 0f;
            foreach (var sample in clipSampleData)
            {
                clipLoudness += Mathf.Abs(sample);
            }
            clipLoudness /= sampleDataLength; //clipLoudness is what you are looking for
            clipLoudness *= globalWindMultiplier;
            windValue = Mathf.Lerp(windValue, clipLoudness, Time.deltaTime * windNoise);
        }

    }
}
