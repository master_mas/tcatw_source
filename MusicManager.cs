﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour {
    public static MusicManager instance;
    public enum MusicState
    {
        forrest,
        busier,
        Harpsichord,
        village,
        darkForrest,
        harpFlute
    }

    public MusicState startingTrack = MusicState.forrest;
    public AudioMixerGroup musicMixerGroup;
    MusicState current = MusicState.forrest;

    public AudioClip elsieThemeClip, wolfThemeClip, busierThemeClip, harpsichordThemeClip, villageClip, darkForrestSoundscapeClip, harpFluteThemeClip;
    AudioSource elsieTheme, wolfTheme, busierTheme, harpsichordTheme, villageTheme, darkForrestSoundscape, harpFluteTheme;
    AudioSource currentlyPlaying;
	// Use this for initialization
	void Start ()
    {
        instance = this;

        elsieTheme = gameObject.AddComponent<AudioSource>();
        wolfTheme = gameObject.AddComponent<AudioSource>();
        busierTheme = gameObject.AddComponent<AudioSource>();
        harpsichordTheme = gameObject.AddComponent<AudioSource>();
        villageTheme = gameObject.AddComponent<AudioSource>();
        darkForrestSoundscape = gameObject.AddComponent<AudioSource>();
        harpFluteTheme = gameObject.AddComponent<AudioSource>();

        elsieTheme.outputAudioMixerGroup = musicMixerGroup;
        wolfTheme.outputAudioMixerGroup = musicMixerGroup;
        busierTheme.outputAudioMixerGroup = musicMixerGroup;
        harpsichordTheme.outputAudioMixerGroup = musicMixerGroup;
        villageTheme.outputAudioMixerGroup = musicMixerGroup;
        darkForrestSoundscape.outputAudioMixerGroup = musicMixerGroup;
        harpFluteTheme.outputAudioMixerGroup = musicMixerGroup;

        elsieTheme.clip = elsieThemeClip;
        wolfTheme.clip = wolfThemeClip;
        busierTheme.clip = busierThemeClip;
        harpsichordTheme.clip = harpsichordThemeClip;
        villageTheme.clip = villageClip;
        darkForrestSoundscape.clip = darkForrestSoundscapeClip;
        harpFluteTheme.clip = harpFluteThemeClip;

        elsieTheme.volume = 0;
        wolfTheme.volume = 0;
        busierTheme.volume = 0;
        harpsichordTheme.volume = 0;
        villageTheme.volume = 0;
        darkForrestSoundscape.volume = 0;
        harpFluteTheme.volume = 0;
        
        elsieTheme.loop = true;
        wolfTheme.loop = true;
        busierTheme.loop = true;
        harpsichordTheme.loop = true;
        villageTheme.loop = true;
        darkForrestSoundscape.loop = true;
        harpFluteTheme.loop = true;

        elsieTheme.Play();
        wolfTheme.Play();
        busierTheme.Play();
        harpsichordTheme.Play();
        villageTheme.Play();
        darkForrestSoundscape.Play();
        harpFluteTheme.Play();


        current = startingTrack;
        switch (current)
        {
            case MusicState.forrest:
                currentlyPlaying = elsieTheme;
                elsieTheme.volume = 1;
                break;
            case MusicState.busier:
                currentlyPlaying = busierTheme;
                busierTheme.volume = 1;
                break;
            case MusicState.Harpsichord:
                currentlyPlaying = harpsichordTheme;
                harpsichordTheme.volume = 1;
                break;
            case MusicState.village:
                currentlyPlaying = villageTheme;
                villageTheme.volume = 1;
                break;
            case MusicState.darkForrest:
                currentlyPlaying = darkForrestSoundscape;
                darkForrestSoundscape.volume = 1;
                break;
            case MusicState.harpFlute:
                currentlyPlaying = harpFluteTheme;
                harpFluteTheme.volume = 1;
                break;
            default:
                break;
        }
    }

    public void ChangeMusicTo(MusicState newState, bool changeBattle = false)
    {
        AudioSource to = null;

        switch (newState)
        {
            case MusicState.forrest:
                if ((current == MusicState.forrest) || changeBattle)
                {
                    if (PlayerManager.instance.playerIsHuman)
                    {
                        to = elsieTheme;
                    }
                    else
                    {
                        to = wolfTheme;
                    }
                }
                break;
            case MusicState.busier:
                to = busierTheme;
                break;
            case MusicState.Harpsichord:
                to = harpsichordTheme;
                break;
            case MusicState.village:
                    to = villageTheme;
                break;
            case MusicState.darkForrest:
                to = darkForrestSoundscape;
                break;
            case MusicState.harpFlute:
                to = harpFluteTheme;
                break;
            default:
                to = elsieTheme;
                break;
        }
        if (to!= null)
        {
            current = newState;
            transCorout = StartCoroutine(transition(currentlyPlaying, to));
        }
    }

    Coroutine transCorout;
    IEnumerator transition (AudioSource from, AudioSource to)
    {
        if (from == to)
        {
            yield break;
        }
        from.volume = 1;
        to.volume = 0;
        currentlyPlaying = to;
        float t = 0;
        while (t <= 1)
        {
            from.volume = 1-t;
            to.volume = t;
            t += Time.deltaTime;
            yield return null;
        }
        currentlyPlaying = to;
    }
}
