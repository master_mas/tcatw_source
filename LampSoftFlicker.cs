﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampSoftFlicker : MonoBehaviour {
    public AnimationCurve lightflicker;
    Light theLight;
    public float loopSpeed;
    float startingintense, offset;

    
	// Use this for initialization
	void Start () {
        theLight = GetComponent<Light>();
        startingintense = theLight.intensity;
        offset = Random.value;
    }
	
	// Update is called once per frame
	void Update () {
        
        theLight.intensity = startingintense * lightflicker.Evaluate( (((Time.time + offset) * loopSpeed) % 1.0f));
	}
}
