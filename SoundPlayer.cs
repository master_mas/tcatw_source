﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class is attached to a gameobject with an audiosource on it.
/// calling play on this class instead of the audiosource will handle playing 
/// random clips, with a pitch and volume variation.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    AudioSource source;
    [SerializeField]
    List<AudioClip> sounds = new List<AudioClip>();

    List<AudioClip> grabBag = new List<AudioClip>();

    float defaultPitch, defaultVolume;
    [Range(0.0f, 3.0f)]
    public float randomPitchRange;
    [Range(0.0f, 1.0f)]
    public float randomVolumeRange;
    
    public bool playOnStart;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        if (source.clip != null && !sounds.Contains(source.clip))
        {
            sounds.Add(source.clip);
        }
        if (sounds.Count == 0)
        {
            Debug.LogWarning(gameObject.name + " has no sounds on it, its being set to inactive.");
            source.enabled = false;
            this.enabled = false;
        }
        defaultVolume = source.volume;
        defaultPitch = source.pitch;
    }

    private void Start()
    {
        if (playOnStart)
            Play();
    }

    public void Play ()
    {
        if (grabBag.Count == 0)
        {
            foreach (AudioClip item in sounds)
            {
                grabBag.Add(item);
            }
        }
        int clipChosen = Random.Range(0, grabBag.Count);
        source.volume = defaultVolume + Random.Range(-randomVolumeRange, randomVolumeRange);
        source.pitch = defaultPitch + Random.Range(-randomPitchRange, randomPitchRange);
        source.PlayOneShot(grabBag[clipChosen]);
        grabBag.Remove(grabBag[clipChosen]);
    }
}
