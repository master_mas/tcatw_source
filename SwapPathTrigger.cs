﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapPathTrigger : MonoBehaviour {

    public bool onPath;
    public bool turnMoralityOff;

	// Use this for initialization
	void Start () {
        GetComponent<MeshRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (turnMoralityOff)
            {
                MoralityController.Self.moralityChangeOverTime = false;
            }
            else
            {
                MoralityController.Self.moralityChangeOverTime = true;
            }
            MoralityController.Self.PlayerOnPath = onPath;
        }
    }
}
