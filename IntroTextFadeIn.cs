﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Author: Sam Murphy
 */

public class IntroTextFadeIn : MonoBehaviour
{
    private bool allowCounter = true;
    private int currentText;

    private float currentWaitTime;

    public float delay = 3;
    public Color[] fadeColors = {Color.black, Color.white};

    public int fadeCount = 10;
    public string moveToScene;
    public float reverseWaitTime = 3;
    public List<StringStorageContainer> text = new List<StringStorageContainer>();
    public Text textArea;

    public Font changeToOnLastMessage;

    private void Awake()
    {
        textArea = GetComponentInChildren<Text>();
        text.ForEach(x => x.textArea = textArea);
    }

    private void Update()
    {
        if (allowCounter)
        {
            currentWaitTime += Time.deltaTime;

            if (currentWaitTime >= delay)
            {
                allowCounter = false;
                currentWaitTime = 0;

                if (currentText >= text.Count)
                {
                    CreditsFlow creditsFlow = null;
                    if ((creditsFlow = GetComponent<CreditsFlow>()) != null)
                        creditsFlow.Disable = false;
                    else
                        SceneManager.LoadScene(moveToScene, LoadSceneMode.Single);
                }
                else
                {
                    text[currentText++].start(fadeColors, fadeCount, reverseWaitTime, () => { allowCounter = true; });
                    if (currentText == text.Count)
                        textArea.font = changeToOnLastMessage;
                }
            }
        }

        if (currentText > 0 && currentText - 1 < text.Count)
            text[currentText - 1].update(Time.deltaTime);
    }

    [Serializable]
    public class StringStorageContainer
    {
        private readonly List<CharacterStorageContainer> characterStorage = new List<CharacterStorageContainer>();

        private Action callback;

        private Color[] colors;
        private float currentSpawnDelay;
        private float currentWaiting;

        private int inital = 0;

        private bool reverse;
        private int reverseCount;
        private float reverseWaitTime;

        private float spawnDelay;
        public string text;

        [HideInInspector] public Text textArea;

        private int totalFades;

        private bool waiting;

        public void start(Color[] colors, int totalFades, float reverseWaitTime, Action callback)
        {
            this.colors = colors;
            this.totalFades = totalFades;
            this.reverseWaitTime = reverseWaitTime;
            this.callback = callback;

            addToBuffer(text);
        }

        public void update(float delta)
        {
            if (inital < characterStorage.Count)
            {
                currentSpawnDelay += delta;

                if (currentSpawnDelay >= spawnDelay)
                {
                    characterStorage[inital].disable = false;
                    inital++;
                    currentSpawnDelay = 0;
                }

                if (inital >= characterStorage.Count)
                    waiting = true;
            }

            if (waiting)
            {
                currentWaiting += delta;
                if (currentWaiting >= reverseWaitTime)
                {
                    waiting = false;
                    reverse = true;
                    currentSpawnDelay = 0;
                }
            }

            if (reverse)
            {
                currentSpawnDelay += delta;

                if (currentSpawnDelay >= spawnDelay)
                {
                    var container = characterStorage[reverseCount++];
                    container.Reverse = true;

                    currentSpawnDelay = 0;
                    if (reverseCount >= characterStorage.Count)
                    {
                        callback.Invoke();
                        reverse = false;
                    }
                }
            }

            characterStorage.ForEach(x => x.update(delta));

            textArea.text = convertListToString(characterStorage);
        }

        private string convertListToString(List<CharacterStorageContainer> list)
        {
            var data = "";
            list.ForEach(x =>
            {
                if (x != null)
                    data += x;
            });
            return data;
        }

        private void addToBuffer(string data)
        {
            foreach (var c in data)
                characterStorage.Add(new CharacterStorageContainer(colors, totalFades, c));
        }
    }

    [Serializable]
    private class CharacterStorageContainer
    {
        private readonly char character;
        private readonly Color[] colors;
        private readonly int fadeCount;
        private bool _reverse;
        private string color;

        private float currentFadeCount;

        public bool disable = true;
        private float movingTo = 1;

        public CharacterStorageContainer(Color[] colors, int fadeCount, char character)
        {
            this.colors = colors;
            this.fadeCount = fadeCount;
            this.character = character;

            processNewColor(colors[0]);
        }

        public bool Reverse
        {
            private get { return _reverse; }
            set
            {
                _reverse = value;

                if (_reverse)
                {
                    disable = false;
                    currentFadeCount = 0;
                    movingTo = colors.Length;
                }
            }
        }

        public void update(float delta)
        {
            if (disable)
                return;

            if (Reverse)
            {
                if (currentFadeCount <= 0)
                    if (movingTo <= 1)
                    {
                        processNewColor(colors[0]);
                        disable = true;
                    }
                    else
                    {
                        movingTo--;
                        currentFadeCount = fadeCount;
                    }
            }
            else
            {
                if (currentFadeCount > fadeCount)
                    if (movingTo >= colors.Length - 1)
                    {
                        processNewColor(colors[colors.Length - 1]);
                        disable = true;
                    }
                    else
                    {
                        movingTo++;
                        currentFadeCount = 0;
                    }
            }

            if (disable)
                return;

            var gradient = new Gradient();
            var gck = new GradientColorKey[2];
            var gak = new GradientAlphaKey[2];

            if (Reverse)
            {
                gck[0].color = colors[(int) (movingTo - 1)];
                gck[0].time = 0;
                try
                {
                    gck[1].color = colors[(int) movingTo];
                }
                catch (IndexOutOfRangeException)
                {
                    Debug.Log(movingTo - 1);
                }
                gck[1].time = 1;

                gak[0].alpha = /*(currentFadeCount / fadeCount / 10) + (movingTo) / colors.Length*/ 1;
                gak[0].time = 0;
                gak[1].alpha = /*(currentFadeCount / fadeCount / 10) + (movingTo - 1) / colors.Length*/ 0;
                gak[1].time = 1;
            }
            else
            {
                gck[0].color = colors[(int) (movingTo - 1)];
                gck[0].time = 0;
                gck[1].color = colors[(int) movingTo];
                gck[1].time = 1;

                gak[0].alpha = currentFadeCount / fadeCount / 10 + (movingTo - 1) / colors.Length;
                gak[0].time = 0;
                gak[1].alpha = currentFadeCount / fadeCount / 10 + movingTo / colors.Length;
                gak[1].time = 1;
            }

            gradient.SetKeys(gck, gak);

            processNewColor(gradient.Evaluate(currentFadeCount / fadeCount));

            if (Reverse)
                currentFadeCount--;
            else
                currentFadeCount++;
        }

        private void processNewColor(Color color)
        {
            this.color = ColorUtility.ToHtmlStringRGB(color);
        }

        public override string ToString()
        {
            return "<color=#" + color + ">" + character + "</color>";
        }
    }
}