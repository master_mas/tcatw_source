﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCreator : MonoBehaviour {

    public enum Gender
    {
        MALE,
        FEMALE
    }

    public enum SkinColour
    {
        DARK,
        PALE,
        TAN
    }

    public enum HairColour
    {
        BLONDE,
        BRUNETTE,
        BLACK,
        GINGER
    }

    public enum MaleHair
    {
        BUZZCUT,
        PONYTAIL,
        SHORT
    }

    public enum FemaleHair
    {
        BUN,
        PONYTAIL,
        PIGTAILS,
        PIGTAILS_WITH_BUN
    }

    public enum MaleClothes
    {
        BLUE,
        BROWN,
        GREEN
    }

    public enum FemaleClothes
    {
        BROWN,
        BURGURDY,
        GREEN,
        PURPLE
    }

    public enum Weapons
    {
        HOE,
        PITCH_FORK,
        SICKLE
    }

    public Gender gender;
    public SkinColour skinColour;
    public HairColour hairColour;
    public MaleHair maleHair;
    public FemaleHair femaleHair;
    public MaleClothes maleClothes;
    public FemaleClothes femaleClothes;
    public Weapons weapon;


    // Gameobject References
    public GameObject male;
    public GameObject female;
    public GameObject malePonytail;
    public GameObject maleShort;
    public GameObject femaleHairBase;
    public GameObject bun;
    public GameObject femalePonytail;
    public GameObject pigtails;

    public List<GameObject> weapons;

    public GameObject hairMaterial;

    public GameObject maleSkinClothesMaterial;
    public GameObject femaleSkinClothesMaterial;
    public GameObject skirtMaterial;
    public List<Material> skinClothes;

    public List<Material> maleHairMaterials;
    public List<Material> femaleHairMaterials;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
