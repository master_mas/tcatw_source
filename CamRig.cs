﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamRig : MonoBehaviour {
    public static CamRig instance;
    public Camera mainCamera;
    Vector3 camBasePos;
    // Use this for initialization
    void Awake ()
    {
        instance = this;
    }

    void Start () 
    {
        camBasePos = mainCamera.transform.localPosition;
    }

	// Update is called once per frame
	void LateUpdate ()
    {
        transform.position = Vector3.Lerp(transform.position, PlayerController.instance.transform.position, Time.deltaTime * 7.5f);
	}
    
    public static void CamShake (float intensity)
    {
        if (instance.shakeCorout != null)
        {
            instance.shakeCorout = null;
        }
        instance.shakeCorout = instance.StartCoroutine(ShakeCamera(intensity));
    }

    Coroutine shakeCorout;
    static IEnumerator ShakeCamera (float intensity)
    {
        float shakeTimer = intensity;
        while (shakeTimer > 0)
        {
            instance.mainCamera.transform.localPosition = instance.camBasePos + ((Random.insideUnitSphere* Time.deltaTime * 60) * shakeTimer);
            shakeTimer = Mathf.MoveTowards(shakeTimer, 0, Time.deltaTime);
            yield return null;
        }
    }
}
