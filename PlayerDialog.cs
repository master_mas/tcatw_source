﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
public class PlayerDialog : MonoBehaviour
{
    public static PlayerDialog instance;

    private Text optionThird1;
    private Text optionThird2;
    private Text optionThird3;
    private Text optionHalf1;
    private Text optionHalf2;

    private Text stageMessage;

    public GameObject optionGameObjectThird1;
    public GameObject optionGameObjectThird2;
    public GameObject optionGameObjectThird3;
    public GameObject optionGameObjectHalf1;
    public GameObject optionGameObjectHalf2;

    public GameObject stageMessageGameObject;

    private bool enabledDialog = false;
    private Action<int> currentCallback;
    private int total = 0;

    void Awake()
    {
        if (instance != null)
        {
            enabled = false;
            return;
        }
        instance = this;
    }

    private void Start()
    {
        optionThird1 = optionGameObjectThird1.GetComponentInChildren<Text>();
        optionThird2 = optionGameObjectThird2.GetComponentInChildren<Text>();
        optionThird3 = optionGameObjectThird3.GetComponentInChildren<Text>();

        optionHalf1 = optionGameObjectHalf1.GetComponentInChildren<Text>();
        optionHalf2 = optionGameObjectHalf2.GetComponentInChildren<Text>();

        stageMessage = stageMessageGameObject.GetComponentInChildren<Text>();

        reset();
        clearStageMessage();
    }

    void Update ()
    {
        if (enabledDialog)
        {
            if ((total == 2 || total == 3) && Input.GetAxis("DPadHorizontal") == -1)
            {
                reset();
                currentCallback(1);
            }

            if ((total == 1 || total == 3) && Input.GetAxis("DPadVertical") == 1)
            {
                reset();
                currentCallback(2);
            }

            if ((total == 2 || total == 3) && Input.GetAxis("DPadHorizontal") == 1)
            {
                reset();
                currentCallback(3);
            }
        }
	}

    public void reset()
    {
        // Added this to allow a background on the object that only shows when dialog is available. -James
        optionGameObjectThird1.gameObject.SetActive(false);
        optionGameObjectThird2.gameObject.SetActive(false);
        optionGameObjectThird3.gameObject.SetActive(false);

        optionGameObjectHalf1.gameObject.SetActive(false);
        optionGameObjectHalf2.gameObject.SetActive(false);

        optionThird1.text = "";
        optionThird2.text = "";
        optionThird3.text = "";
        
        optionHalf1.text = "";
        optionHalf2.text = "";

        enabledDialog = false;
    }

    public void playDialog(string[] dialogOptions, Action<int> callback)
    {

        if (enabledDialog)
        {
            return;
        }

        enabledDialog = true;
        total = dialogOptions.Length;
        currentCallback = callback;
        switch (dialogOptions.Length)
        {
            case 0:
                return;
            case 1:
                optionThird2.text = dialogOptions[0];
                optionGameObjectThird2.gameObject.SetActive(true);
                break;
            case 2:
                optionHalf1.text = dialogOptions[0];
                optionHalf2.text = dialogOptions[1];

                optionGameObjectHalf1.gameObject.SetActive(true);
                optionGameObjectHalf2.gameObject.SetActive(true);
                break;
            case 3:
                optionThird1.text = dialogOptions[0];
                optionThird2.text = dialogOptions[1];
                optionThird3.text = dialogOptions[2];

                optionGameObjectThird1.gameObject.SetActive(true);
                optionGameObjectThird2.gameObject.SetActive(true);
                optionGameObjectThird3.gameObject.SetActive(true);
                break;
        }
    }

    public void displayStageMessage(string message, float waitTime, Action callback)
    {
        stageMessageGameObject.SetActive(true);
        stageMessage.text = message;

        Debug.Log(stageMessageGameObject.activeSelf);

        StartCoroutine(stageMessageTimer(waitTime, callback));
    }

    public void clearStageMessage()
    {
        stageMessage.text = "";
        stageMessageGameObject.SetActive(false);


    }

    private IEnumerator stageMessageTimer(float waitTime, Action callback)
    {

        while (waitTime > 0)
        {
            yield return new WaitForEndOfFrame();
            waitTime -= Time.deltaTime;
        }
        clearStageMessage();
        callback.Invoke();
    }
}
