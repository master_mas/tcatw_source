﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NPCGestures))]
[System.Serializable]
public class ComplexEnemy : EntityBase {
    public enum State
    {
        Idle,
        Follow,
        Combat,
        Dead,
        WaitForInstruction
    }
    public enum AggroType
    {
        Neutral,
        OnSightHuman,
        OnSightWolf,
        EnemyList,
        Provoked,
        Never
    }
    public enum EnemyType
    {
        Human,
        Wolf,
        WoodCutter,
        WhiteWolf
    }
    
    enum AttackType
    {
        Horizontal,
        Forward,
        Jump
    }

    public enum CommandType
    {
        None,
        Attack,
        Wait,
        StayAway
    }

    // GameObjects
    public GameObject human;
    public GameObject male;
    public GameObject female;
    public GameObject wolf;
    public GameObject whiteWolf;
    public GameObject woodCutter;

    // Enums
    public State currentState = State.Idle;
    public AggroType aggroState = AggroType.Neutral;
    public EnemyType enemyType = EnemyType.Wolf;
    public CommandType command = CommandType.None;
    private AggroType prevAggroState = AggroType.Neutral;

    // Bools
    public bool canLoseTarget = false;
    public bool hasBeenAttacked = false;
    public bool primaryTargetIsPlayer = false;
    private bool playerIsAnAlly = false;
    private bool stopMoralityChangeOnHit = false;
    private bool swapAllies = false;
    private bool targetingPlayer = false;
    // Wolf Pack Variables
    public bool manualSetup;
    public bool packLeader;
    public bool packSetup = false;
    public bool hasPack = false;

    // Ints
    public int humanDistanceAngle = 60;
    private int targetRef = -1;

    // Floats
    public float rotationSmooth = 5.0f;
    public float maxVisibleRange = 10.0f;
    public float checkTargetTimer = 4.0f;
    public float attackTimer = 3;
    public float kickTimer = 5;
    public float dodgeTimer = 0;
    public float whiteWolfSpeed = 3;
    public float healthFleeAmount;
    public float healthReturnAmount;
    private float runningAttackCooldown = 0;
    private float followDist = 0.5f;
    private float humanAttackTimer = 0;
    private float forwardTargetSpeed;
    // Getting hit delay
    private float hitDamageDelay = 0.2f;
    private float hitDamageTimer = 0.0f;


    public Animator anim;

    public NavMeshAgent nav;
    private NavMeshPath path;

    public Transform runAwayPosition;
    private Transform targetPos;
    private Transform targetObject;
    private Transform oldTarget;
    private Transform targetHolder;
    private Transform myPos;

    private PlayerController playerRef;
    private ComplexEnemy leaderRef;
    public Vector2 attackDelayMinMax = new Vector2(1, 3);

    [SerializeField]
    public List<ComplexEnemy> allies;

    [HideInInspector]
    public SpriteRenderer visualIndicator;

    private EnemyWeapon weapon1;
    private EnemyWeapon weapon2;
    private EnemyWeapon weapon3;
    private EnemyWeapon weapon4;

    private Text speech;
    private Image speechBackground;

    // Animation Names
    private string animJump;
    private string animMove;
    private string animDodge;

    public struct AnimationAttacks
    {
        public string forward;
        public string combo;
        public string horizontal;
        public string kick;
        public string jump;
        public string dodge;
        public string die;
    };

    AnimationAttacks animNames = new AnimationAttacks();

    // this is a big block of transforms that will be set in the editor, they are used to place objects in the scene parented to this enemy
    // that can be used to figure out which attack is best.
    public Transform[] forwardFinders, horizontalFinders, jumpFinders;
    public List<Transform> enemyTargets;
    private List<Transform> distCheckEnemyTargets;

    // Use this for initialization
    void Start ()
    {
        path = new NavMeshPath();
        targetPos = null;
        targetObject = null;
        distCheckEnemyTargets = new List<Transform>();

        playerRef = PlayerManager.GetActivePlayer();

        targetHolder = GameObject.Find("EnemyGroupTargetsHolder").transform;

        anim = GetComponentInChildren<Animator>();
        nav = GetComponentInChildren<NavMeshAgent>();
        EnemyWeapon[] weapons = GetComponentsInChildren<EnemyWeapon>();

        if (weapons.Length == 4)
        {
            weapon1 = weapons[1];
            weapon2 = weapons[3];
            weapon3 = weapons[0];
            weapon4 = weapons[2];
        }
        else
        {
            weapon1 = weapons[0];
            if (weapons.Length > 1)
            {
                weapon2 = weapons[1];
            }
        }

        GameObject.Find(name + "/Finders").transform.parent = anim.transform;
        myPos = anim.transform;

        visualIndicator = GetComponentInChildren<SpriteRenderer>();
        visualIndicator.enabled = false;
        visualIndicator.transform.parent = anim.transform;

        if (packLeader)
        {
            Invoke("SetUpPack", 0.1f);
        }
        else if (enemyType == EnemyType.Wolf || enemyType == EnemyType.Human)
        {
            Invoke("NoPack", 0.3f);
        }

        if (enemyTargets.Count > 0)
        {
            if (swapAllies)
            {
                SwapReference();
            }
            Invoke("UpdateRefPostions", 0.1f);
        }

        PlayerManager.changeEventCallback.Add(ChangeOfPlayerState);

        SetAnimRefs();

        if (enemyType == EnemyType.WhiteWolf)
        {
            nav.updatePosition = true;
            followDist = 2.0f;
        }
        if (enemyType == EnemyType.WoodCutter)
        {
            followDist = 1.0f;
        }

        speech = GetComponentInChildren<Text>();
        speechBackground = GetComponentInChildren<Image>();
        speechBackground.enabled = false;

        if (currentState == State.WaitForInstruction)
        {
            anim.SetFloat("InBattle", 1);
        }
    }

    /// <summary>
    /// Gets updated reference to the player
    /// </summary>
    /// <param name="playerState">Current state of player</param>
    private void ChangeOfPlayerState(PlayerManager.PlayerState playerState)
    {
        playerRef = PlayerManager.GetActivePlayer();
        if (!packLeader)
        {
            if (leaderRef != null)
            {
                targetObject = leaderRef.targetObject;
            }
        }
        else if (targetObject.gameObject.tag == "Player")
        {
            targetObject = playerRef.transform;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        switch (currentState)
        {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Follow:
                FollowUpdate();
                break;
            case State.Combat:
                CombatUpdate();
                break;
            case State.Dead:
                if(enemyType == EnemyType.WhiteWolf && PlayerManager.IsPlayerHuman())
                {
                    FadeToBlack.instance.gameEndWinHuman = true;
                }
                else if (enemyType == EnemyType.WoodCutter && !PlayerManager.IsPlayerHuman())
                {
                    FadeToBlack.instance.gameEndWinWolf = true;
                }
                break;
            case State.WaitForInstruction:
                break;
            default:
                break;
        }

        if (hitDamageTimer > 0)
        {
            hitDamageTimer -= Time.deltaTime;
        }

        if (currentState != State.Dead)
        {
            float tempForwardSpeed = Mathf.MoveTowards(anim.GetFloat("Forward"), forwardTargetSpeed, Time.deltaTime * 5);
            anim.SetFloat("Forward", tempForwardSpeed);
        }
    }

    void IdleUpdate()
    {
        forwardTargetSpeed = 0;
        anim.SetFloat("InBattle", 0);
        CheckForTarget();
    }
    
    /// <summary>
    /// Follows current target, unless too far away or in combat range
    /// </summary>
    void FollowUpdate ()
    {
        if (canLoseTarget)
        {
            if (!CheckIfTargetInRange(targetObject))
            {
                currentState = State.Follow;
                aggroState = prevAggroState;
                canLoseTarget = false;
                targetPos = myPos.parent;
            }
        }

        switch (enemyType)
        {
            case EnemyType.Human:
                HumanCombat();
                break;
            case EnemyType.Wolf:
                if (packLeader && !packSetup)
                {
                    WolfCombat();
                }
                break;
            case EnemyType.WoodCutter:
                BossCombat();
                break;
            case EnemyType.WhiteWolf:
                if (!packSetup)
                {
                    if (CheckIfAlliesDead())
                    {
                        currentState = State.Combat;
                        IndividualTargetSetup(0);
                        packSetup = true;

                        // find woodcutter and add self into enemy list
                        ComplexEnemy[] allEnemies = FindObjectsOfType<ComplexEnemy>();
                        foreach (ComplexEnemy enemy in allEnemies)
                        {
                            if (enemy.enemyType == EnemyType.WoodCutter)
                            {
                                enemy.enemyTargets.Add(myPos);
                            }
                        }
                    }
                }
                else if (packSetup)
                {
                    UpdateTargetHolder(myPos.position);
                }
                break;
            default:
                break;
        }

        if (targetPos != null)
        {
            nav.CalculatePath(targetPos.position, path);
        }

        if (path.corners.Length == 0)
        {
            currentState = State.Idle;
            return;
        }

        anim.SetFloat("InBattle", 1);

        float dist = Vector3.Distance(myPos.position, path.corners[path.corners.Length - 1]);
        if (dist > followDist)
        {
            forwardTargetSpeed = 1;
            if (enemyType == EnemyType.WhiteWolf)
            {
                if (anim.GetCurrentAnimatorStateInfo(1).IsName("Hurt"))
                {
                    forwardTargetSpeed = 0;
                }
                else
                {
                    forwardTargetSpeed = 1;
                    nav.Move(myPos.forward * whiteWolfSpeed * Time.deltaTime);
                }
            }
            for (int i = 0; i < path.corners.Length - 1; i++)
            {
                Debug.DrawLine(path.corners[i], path.corners[i + 1]);
            }
            Quaternion lookDir = Quaternion.LookRotation(path.corners[1] - myPos.position, Vector3.up);
            myPos.rotation = Quaternion.RotateTowards(myPos.rotation, lookDir, Time.deltaTime * rotationSmooth);
        }
        else
        {
            if (enemyType == EnemyType.WhiteWolf && runAwayPosition == targetPos)
            {
                currentState = State.Idle;
                targetObject = playerRef.transform;
                targetPos = null;
                return;
            }

            if (anim.GetFloat("Forward") < 0.05f && anim.GetFloat("Forward") > -0.05f)
            {
                currentState = State.Combat;
            }
            forwardTargetSpeed = 0;

            if (aggroState == prevAggroState)
            {
                targetPos = null;
                currentState = State.Idle;
                packSetup = false;
                return;
            }
        }

        float tempDist = Vector3.Distance(targetObject.position, jumpFinders[0].position);
        if (tempDist < 0.5f && runningAttackCooldown <= 0)
        {
            SetAttack(AttackType.Jump);
            runningAttackCooldown = 5;
        }
        runningAttackCooldown -= Time.deltaTime;

        if (anim.GetCurrentAnimatorStateInfo(0).IsName(animJump))
        {
            rotationSmooth = 65.0f;
        }
        else
        {
            rotationSmooth = 180.0f;
        }
    }

    /// <summary>
    /// Controls combat decisions depending on enemy type
    /// </summary>
    void CombatUpdate()
    {
        if (enemyType == EnemyType.Wolf && packLeader && !packSetup)
        {
            WolfCombat();
        }
        else if (enemyType == EnemyType.Human)
        {
            HumanCombat();
        }
        else if (enemyType == EnemyType.WhiteWolf || enemyType == EnemyType.WoodCutter)
        {
            BossCombat();
        }

        // Make AI look at target
        Quaternion lookDir = Quaternion.LookRotation(targetObject.position - myPos.position, Vector3.up);
        if (anim.GetCurrentAnimatorStateInfo(0).IsName(animMove))
        {
            myPos.rotation = Quaternion.RotateTowards(myPos.rotation, lookDir, Time.deltaTime * 180);
        }
        else
        {
            myPos.rotation = Quaternion.RotateTowards(myPos.rotation, lookDir, Time.deltaTime * 45);
        }

        CombatDecisions();
    }

    /// <summary>
    /// Determine whether to attack, dodge or follow
    /// </summary>
    void CombatDecisions()
    {
        // Distance check to determine to attack, dodge or follow
        if (targetPos == null)
        {
            return;
        }

        float dist = Vector3.Distance(myPos.position, targetPos.position);
        if (dist < 2.5f)
        {
            if (enemyType == EnemyType.Human && command == CommandType.Wait)
            {
                attackTimer -= Time.deltaTime;
            }
            else
            {
                if (kickTimer <= 0)
                {
                    myPos.rotation = Quaternion.LookRotation(targetObject.position - myPos.position, Vector3.up);
                    //anim.SetTrigger("Kick"); // won't have for small enemies
                    kickTimer = 5;
                }
                else
                {
                    kickTimer -= Time.deltaTime;
                }

                if (attackTimer <= 0)
                {
                    ChooseAttack();
                    attackTimer = Random.Range(2.0f, 5.0f);
                    //kickTimer = 5;
                    if (enemyType == EnemyType.Human)
                    {
                        command = CommandType.Wait;
                    }
                }
                else
                {
                    attackTimer -= Time.deltaTime;
                }
            }

            DetermineDodge();
        }
        else if (enemyType == EnemyType.Human && packLeader && allies.Count > 1)
        {
            return;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName(animMove))
        {
            currentState = State.Follow;
        }
    }

    void WolfCombat()
    {
        RotateTransformsDependingOnNumber(allies.Count);
        packSetup = true;
    }

    /// <summary>
    /// Wolf targeting setup
    /// </summary>
    /// <param name="num">Number of current allies</param>
    void RotateTransformsDependingOnNumber(int num)
    {
        if (num == 1)
        {
            SetPlayerAsTarget(allies[0].transform);
            return;
        }

        targetHolder.LookAt(allies[0].transform);
        int rotateAmount = 360 / num;
        for (int i = 0; i < num; i++)
        {
            Transform newTarget = GameObject.Find("Target" + (i + 1)).transform;
            newTarget.position = newTarget.parent.position;
            newTarget.LookAt(allies[0].transform);
            newTarget.Rotate(Vector3.up, rotateAmount * i);
            newTarget.position += newTarget.forward * 1.0f;
            allies[i].targetPos = newTarget;
            allies[i].targetObject = playerRef.transform;
            allies[i].targetRef = i + 1;
        }
    }

    /// <summary>
    /// Sets target to act as player position
    /// </summary>
    /// <param name="lookAtTransform">Transform for target holder to look at</param>
    void SetPlayerAsTarget(Transform lookAtTransform)
    {
        targetHolder.LookAt(myPos);
        targetObject = playerRef.transform;
        Transform newTarget = GameObject.Find("Target1").transform;
        newTarget.position = playerRef.transform.position;
        newTarget.LookAt(lookAtTransform);
        newTarget.position += newTarget.forward * 1.0f;
        targetPos = newTarget;
    }

    void HumanCombat()
    {
        if (packLeader)
        {
            if (!packSetup)
            {
                HumanSetPositions(allies.Count);
                packSetup = true;
            }

            Vector3 averagePos = new Vector3();

            if (allies.Count > 1)
            {
                for (int i = 1; i < allies.Count; i++)
                {
                    averagePos += allies[i].myPos.position;
                }
                averagePos /= allies.Count - 1;
            }
            else
            {
                averagePos = myPos.position;
            }

            averagePos.y = 0;

            UpdateTargetHolder(averagePos);
            CheckWhoCanAttack();
            return;
        }
        else if (aggroState == AggroType.EnemyList)
        {
            command = CommandType.Attack;
        }

        // Check if health is low, and if it is move away from player toward leader to heal
        if (health <= healthFleeAmount)
        {
            ChangeTarget(true);
        }
        else if (health >= healthReturnAmount && command == CommandType.StayAway)
        {
            ChangeTarget(false);
        }
        else if (health < healthReturnAmount && command == CommandType.StayAway)
        {
            float dist = Vector3.Distance(myPos.position, targetPos.position);
            if (dist <= followDist && health > 0)
            {
                Heal(Time.deltaTime);
            }
        }
    }

    /// <summary>
    /// Determines who to attack in groups
    /// </summary>
    void CheckWhoCanAttack()
    {
        if (allies.Count == 1 || humanAttackTimer > 0)
        {
            humanAttackTimer -= Time.deltaTime;
            command = CommandType.Attack;
            return;
        }

        List<int> whoToChoose = new List<int>();

        for (int i = 1; i < allies.Count; i++)
        {
            if (allies[i].command == CommandType.Wait && allies[i].attackTimer <= -1)
            {
                whoToChoose.Add(i);
            }
            if (allies[i].command != CommandType.StayAway)
            {
                allies[i].command = CommandType.Wait;
            }
        }

        if (whoToChoose.Count == 0)
        {
            return;
        }
        int attacker = Random.Range(0, whoToChoose.Count);
        allies[whoToChoose[attacker]].command = CommandType.Attack;
        humanAttackTimer = Random.Range(attackDelayMinMax.x, attackDelayMinMax.y);
    }

    /// <summary>
    /// Human enemy return to heal or re-enter combat
    /// </summary>
    /// <param name="healing">Need to heal</param>
    void ChangeTarget(bool healing)
    {
        if (leaderRef != null)
        {
            if (healing)
            {
                targetPos = leaderRef.transform;
                command = CommandType.StayAway;
                followDist = 2.0f;
                float dist = Vector3.Distance(myPos.position, targetPos.position);
                if (dist <= followDist)
                {
                    Heal(Time.deltaTime);
                }
            }
            else
            {
                targetPos = oldTarget;
                command = CommandType.Wait;
                followDist = 0.5f;
            }
            currentState = State.Follow;
        }
    }

    /// <summary>
    /// Human targeting setup
    /// </summary>
    /// <param name="num">Number of allies</param>
    void HumanSetPositions(int num)
    {
        targetHolder.LookAt(allies[0].transform);
        if (num > 1)
        {
            int rotateAmount = (humanDistanceAngle * (num - 2)) / 2;

            for (int i = 1; i < num; i++)
            {
                Transform newTarget = GameObject.Find("Target" + (i + 1)).transform;
                newTarget.LookAt(allies[0].transform);
                newTarget.Rotate(Vector3.up, rotateAmount);
                newTarget.position += newTarget.forward * 1.0f;
                allies[i].targetPos = newTarget;
                allies[i].targetObject = playerRef.transform;
                allies[i].targetRef = i + 1;
                allies[i].oldTarget = newTarget;
                rotateAmount -= humanDistanceAngle;
            }
        }
    }

    /// <summary>
    /// Single enemy targeting player
    /// </summary>
    /// <param name="num">Ally number</param>
    void IndividualTargetSetup(int num)
    {
        Transform newTarget = GameObject.Find("Target1").transform;

        if (enemyType == EnemyType.WhiteWolf)
        {
            newTarget.position = playerRef.transform.position;
            newTarget.LookAt(targetHolder);
            newTarget.position += newTarget.forward * 1.5f;
            targetObject = playerRef.transform;
            targetPos = newTarget;
            return;
        }

        // Reset position incase it hasn't been reset
        UpdateTargetHolder(allies[num].transform.position);
        newTarget.position = playerRef.transform.position;
        newTarget.LookAt(allies[num].transform);
        newTarget.position += newTarget.forward * 1.0f;
        targetObject = playerRef.transform;
        targetPos = newTarget;
        if (enemyType == EnemyType.Human)
        {
            command = CommandType.Attack;
            if (allies.Count == 2)
            {
                allies[1].targetPos = newTarget;
            }
        }
    }

    void BossCombat()
    {
        targetHolder.LookAt(myPos);
    }

    bool CheckIfAlliesDead()
    {
        for (int i = 0; i < allies.Count; i++)
        {
            if (allies[i].currentState != State.Dead)
            {
                return false;
            }
        }

        return true;
    }

    void DetermineDodge()
    {
        if (targetingPlayer)
        {
            if (!PlayerController.instance.anim.GetCurrentAnimatorStateInfo(0).IsName("Movement") &&
                        !PlayerController.instance.anim.GetCurrentAnimatorStateInfo(0).IsName(animDodge))
            {
                if (dodgeTimer < 0 &&
                    Vector3.Dot(PlayerController.instance.transform.forward, myPos.forward) < -0.75f &&
                    anim.GetCurrentAnimatorStateInfo(0).IsName(animMove))
                {
                    anim.Play(animDodge);
                    dodgeTimer = 2;
                }
            }
            dodgeTimer -= Time.deltaTime;
        }
        else
        {
            Animator targetAnim = targetObject.GetComponentInChildren<Animator>();
            if (!targetAnim.GetCurrentAnimatorStateInfo(0).IsName(animMove) &&
            !targetAnim.GetCurrentAnimatorStateInfo(0).IsName(animDodge))
            {
                if (dodgeTimer < 0 &&
                    Vector3.Dot(targetObject.forward, myPos.forward) < -0.75f &&
                    anim.GetCurrentAnimatorStateInfo(0).IsName(animMove))
                {
                    anim.Play(animDodge);
                    dodgeTimer = 2;
                }
            }
            dodgeTimer -= Time.deltaTime;
        }
    }

    void ChooseAttack()
    {
        AttackType attack = AttackType.Forward;
        float closest = 99999;
        foreach (Transform item in forwardFinders)
        {
            float tempDist = Vector3.Distance(targetObject.position, item.position);
            if (closest > tempDist)
            {
                closest = tempDist;
                attack = AttackType.Forward;
            }
        }

        foreach (Transform item in horizontalFinders)
        {
            float tempDist = Vector3.Distance(targetObject.position, item.position);
            if (closest > tempDist)
            {
                closest = tempDist;
                attack = AttackType.Horizontal;
                weapon1.damage = heavyDamage;
                if (weapon2 != null)
                {
                    weapon2.damage = heavyDamage;
                }
                if (weapon3 != null)
                {
                    weapon3.damage = heavyDamage;
                }
                if (weapon4 != null)
                {
                    weapon4.damage = heavyDamage;
                }
            }
        }
        if (enemyType != EnemyType.WhiteWolf)
        {
            foreach (Transform item in jumpFinders)
            {
                float tempDist = Vector3.Distance(targetObject.position, item.position);
                if (closest > tempDist)
                {
                    closest = tempDist;
                    attack = AttackType.Jump;
                }
            }
        }

        SetAttack(attack);
    }

    void SetAttack(AttackType attack)
    {

        switch (enemyType)
        {
            case EnemyType.Human:
                break;
            case EnemyType.Wolf:
                GlobalSoundPlayer.Play("WolfNpcGrowl", transform.position);
                break;
            case EnemyType.WoodCutter:
                GlobalSoundPlayer.Play("WoodcutterAttack", transform.position);
                break;
            case EnemyType.WhiteWolf:
                GlobalSoundPlayer.Play("WhiteWolfAttack", transform.position);
                break;
            default:
                break;
        }

        switch (attack)
        {
            case AttackType.Horizontal:
                if (Random.value < 0.5f)
                {
                    anim.SetTrigger(animNames.horizontal);
                    weapon1.damage = heavyDamage;
                    if (weapon2 != null)
                    {
                        weapon2.damage = heavyDamage;
                    }
                    if (weapon3 != null)
                    {
                        weapon3.damage = heavyDamage;
                    }
                    if (weapon4 != null)
                    {
                        weapon4.damage = heavyDamage;
                    }
                }
                else
                {
                    anim.SetTrigger(animNames.combo);
                    weapon1.damage = lightDamage;
                    if (weapon2 != null)
                    {
                        weapon2.damage = lightDamage;
                    }
                    if (weapon3 != null)
                    {
                        weapon3.damage = lightDamage;
                    }
                    if (weapon4 != null)
                    {
                        weapon4.damage = lightDamage;
                    }
                }
                break;
            case AttackType.Forward:
                if (Random.value > 0.3f)
                {
                    anim.SetTrigger(animNames.forward);
                }
                else
                {
                    if (enemyType == EnemyType.WhiteWolf)
                    {
                        anim.SetTrigger(animNames.horizontal);
                    }
                    else
                    {
                        anim.SetTrigger(animNames.combo);
                    }
                }
                weapon1.damage = lightDamage;
                if (weapon2 != null)
                {
                    weapon2.damage = lightDamage;
                }
                if (weapon3 != null)
                {
                    weapon3.damage = lightDamage;
                }
                if (weapon4 != null)
                {
                    weapon4.damage = lightDamage;
                }
                break;
            case AttackType.Jump:
                anim.SetTrigger(animNames.jump);
                weapon1.damage = heavyDamage;
                if (weapon2 != null)
                {
                    weapon2.damage = heavyDamage;
                }
                if (weapon3 != null)
                {
                    weapon3.damage = heavyDamage;
                }
                if (weapon4 != null)
                {
                    weapon4.damage = heavyDamage;
                }
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Checks id there is a target
    /// </summary>
    void CheckForTarget()
    {
        targetingPlayer = false;
        switch (aggroState)
        {
            case AggroType.Neutral: // Potential to change state, but don't know if it needed
                break;
            case AggroType.OnSightHuman: // within range
                targetingPlayer = true;
                AttackOnSight();
                break;
            case AggroType.OnSightWolf: // within range
                targetingPlayer = true;
                AttackOnSight();
                break;
            case AggroType.EnemyList: // within range
                GetClosestEnemyToTarget();
                if (targetObject != null)
                {
                    SetTargetAndAttack(targetObject, false);
                    StopAllCoroutines();
                    StartCoroutine("TargetRangeChecker");
                }
                else if (enemyType == EnemyType.WhiteWolf && !packSetup && CheckIfAlliesDead())
                {
                    currentState = State.Follow;
                    IndividualTargetSetup(0);
                    packSetup = true;

                    // find woodcutter and add self into enemy list
                    ComplexEnemy[] allEnemies = FindObjectsOfType<ComplexEnemy>();
                    foreach (ComplexEnemy enemy in allEnemies)
                    {
                        if (enemy.enemyType == EnemyType.WoodCutter)
                        {
                            enemy.enemyTargets.Add(this.transform);
                            enemy.distCheckEnemyTargets.Add(myPos);
                        }
                    }
                    enemyTargets.Add(playerRef.transform);
                    distCheckEnemyTargets.Add(playerRef.transform);
                }
                else
                {
                    anim.ResetTrigger(animNames.forward);
                    anim.ResetTrigger(animNames.horizontal);
                    anim.ResetTrigger(animNames.combo);
                    anim.ResetTrigger(animNames.jump);
                }
                break;
            case AggroType.Provoked: // when attacked
                if (hasBeenAttacked)
                {
                    if (packLeader)
                    {
                        PartyAttacked();
                    }
                    else if (leaderRef != null)
                    {
                        leaderRef.PartyAttacked();
                    }
                    hasBeenAttacked = false;

                    UpdateMorality();
                }
                break;
            case AggroType.Never: // Just takes it
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Updates enitre enemy party to hostile
    /// </summary>
    void PartyAttacked()
    {
        bool playerState = PlayerManager.IsPlayerHuman();
        for (int i = 0; i < allies.Count; i++)
        {
            if(!playerState)
            {
                allies[i].aggroState = AggroType.OnSightWolf;
            }
            else
            {
                allies[i].aggroState = AggroType.OnSightHuman;
            }

            allies[i].anim.SetFloat("InBattle", 1);

            if (CheckIfSameType())
            {
                allies[i].canLoseTarget = true;
            }
            allies[i].targetingPlayer = true;
            allies[i].SetTargetAndAttack(playerRef.transform, true);
        }
    }

    /// <summary>
    /// Sets target if can see player
    /// </summary>
    void AttackOnSight()
    {
        if ((enemyType == EnemyType.Wolf || enemyType == EnemyType.WhiteWolf) && PlayerManager.IsPlayerHuman())
        {
            // player is human
            SetTargetAndAttack(playerRef.transform, true);
        }
        else if ((enemyType == EnemyType.Human || enemyType == EnemyType.WoodCutter) && !PlayerManager.IsPlayerHuman())
        {
            // player is wolf
            SetTargetAndAttack(playerRef.transform, true);
        }
    }

    /// <summary>
    /// Sets state into Combat
    /// </summary>
    /// <param name="newTarget">Found target</param>
    /// <param name="targetPlayer">Player is main target</param>
    void SetTargetAndAttack(Transform newTarget, bool targetPlayer)
    {
        if (newTarget != null && CheckIfTargetInRange(newTarget))
        {
            targetObject = newTarget;
            if (!targetPlayer)
            {
                targetPos = newTarget;
            }
            else
            {
                SequenceListener.NPCTargetsPlayer(gameObject);
                if (enemyType == EnemyType.WoodCutter)
                {
                    SetPlayerAsTarget(myPos);
                }
            }
            currentState = State.Combat;
        }
    }

    bool CheckIfTargetInRange(Transform newTarget)
    {
        if (!canLoseTarget || Vector3.Distance(myPos.position, newTarget.position) < maxVisibleRange)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Keeps checking that target is in range
    /// </summary>
    /// <returns></returns>
    IEnumerator TargetRangeChecker()
    {
        while (true)
        {
            yield return new WaitForSeconds(checkTargetTimer);
            GetClosestEnemyToTarget();
        }
    }

    /// <summary>
    /// Finds closest enemy in target list
    /// </summary>
    void GetClosestEnemyToTarget()
    {
        float minDist = maxVisibleRange;
        if (!canLoseTarget)
        {
            minDist = 10000;
        }
        int count = -1;
        for (int i = 0; i < distCheckEnemyTargets.Count; i++)
        {
            float newDist = Vector3.Distance(myPos.position, distCheckEnemyTargets[i].position);
            if (newDist < minDist)
            {
                ComplexEnemy enemy = enemyTargets[i].GetComponent<ComplexEnemy>();
                PlayerController player = enemyTargets[i].GetComponent<PlayerController>();
                if (enemy != null && enemy.currentState != State.Dead)
                {
                    minDist = newDist;
                    count = i;
                }
                else if (player != null && player.currentHealth > 0)
                {
                    minDist = newDist;
                    count = i;
                }
                anim.SetFloat("InBattle", 1);
            }
        }

        if (count == -1)
        {
            currentState = State.Idle;
            targetObject = null;
            StopAllCoroutines();
            return;
        }
        if (primaryTargetIsPlayer && Vector3.Distance(myPos.position, playerRef.transform.position) < (maxVisibleRange - minDist))
        {
            targetPos = playerRef.transform;
            targetObject = playerRef.transform;
            targetingPlayer = true;
            followDist = 0.5f;
        }
        else
        {
            if (distCheckEnemyTargets[count] == playerRef.transform)
            {
                SetPlayerAsTarget(myPos);
            }
            else
            {
                targetObject = distCheckEnemyTargets[count];
                targetPos = distCheckEnemyTargets[count];
                targetingPlayer = false;
                followDist = 2.0f;
            }
        }
    }

    /// <summary>
    /// Deals damage and checks for death
    /// </summary>
    /// <param name="amount">Damage amount</param>
    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        if (health > 0)
        {
            switch (enemyType)
            {
                case EnemyType.Human:
                    GlobalSoundPlayer.Play("HumanNpcHurt", transform.position);
                    break;
                case EnemyType.Wolf:
                    GlobalSoundPlayer.Play("WolfNpcHurt", transform.position);
                    break;
                case EnemyType.WoodCutter:
                    GlobalSoundPlayer.Play("WoodcutterHurt", transform.position);
                    break;
                case EnemyType.WhiteWolf:
                    GlobalSoundPlayer.Play("WhitewolfHurt", transform.position);
                    break;
                default:
                    break;
            }

            bool hurt = true;

            if ((enemyType == EnemyType.WhiteWolf || enemyType == EnemyType.WoodCutter) && !anim.GetCurrentAnimatorStateInfo(0).IsName(animMove))
            {
                hurt = false;
            }

            if (hurt)
            {
                anim.CrossFade("Hurt", 0.2f, 1);
            }
        }
        if (health == 0)
        {
            switch (enemyType)
            {
                case EnemyType.Human:
                    GlobalSoundPlayer.Play("HumanNpcHurt", transform.position);
                    break;
                case EnemyType.Wolf:
                    GlobalSoundPlayer.Play("WolfNpcDie", transform.position);
                    break;
                case EnemyType.WoodCutter:
                    GlobalSoundPlayer.Play("WoodcutterHurt", transform.position);
                    break;
                case EnemyType.WhiteWolf:
                    GlobalSoundPlayer.Play("WhitewolfHowl", transform.position);
                    break;
                default:
                    break;
            }

            if (packLeader)
            {
                TransferPackDataToNewLeader();
                if (allies.Count == 0)
                {
                    Transform newTarget = GameObject.Find("Target1").transform;
                    newTarget.localPosition = Vector3.zero;
                }
            }
            else if (leaderRef != null)
            {
                leaderRef.MemberDied(this);
            }

            weapon1.on = false;
            if (weapon2 != null)
            {
                weapon2.on = false;
            }
            if (weapon3 != null)
            {
                weapon3.on = false;
            }
            if (weapon4 != null)
            {
                weapon4.on = false;
            }

            if (CheckIfSameType())
            {
                UpdateMorality();
            }

            GetComponentInChildren<BoxCollider>().enabled = false;
            nav.enabled = false;
            currentState = State.Dead;
            anim.CrossFade(animNames.die, .1f, 1);
            SequenceListener.PlayerKilledNPC(gameObject);
        }
        else if (!hasBeenAttacked && CheckIfSameType() && leaderRef != null)
        {
            prevAggroState = aggroState;
            for (int i = 0; i < leaderRef.allies.Count; i++)
            {
                leaderRef.allies[i].prevAggroState = leaderRef.allies[i].aggroState;
            }
            aggroState = AggroType.Provoked;
            hasBeenAttacked = true;
        }
    }

    /// <summary>
    /// Checks if player is of same type as self
    /// </summary>
    /// <returns></returns>
    bool CheckIfSameType()
    {
        if ((enemyType == EnemyType.Wolf && !PlayerManager.IsPlayerHuman()) ||
            (enemyType == EnemyType.Human && PlayerManager.IsPlayerHuman()))
        {
            return true;
        }

        return false;
    }

    void UpdateMorality()
    {
        if (enemyType == EnemyType.Human)
        {
            MoralityController.Self.Weight -= 1;
        }
        else if (enemyType == EnemyType.Wolf)
        {
            MoralityController.Self.Weight += 1;
        }
    }

    /// <summary>
    /// Defualt fight enemy list
    /// </summary>
    public void SetupAIvsAI()
    {
        currentState = State.Idle;
    }

    /// <summary>
    /// Adds player to enemy list to fight
    /// </summary>
    public void SetupPlayerEnemy()
    {
        currentState = State.Idle;
        enemyTargets.Add(playerRef.transform);
        distCheckEnemyTargets.Add(playerRef.transform);

        if (packLeader && enemyType == EnemyType.Wolf)
        {
            WolfCombat();
            packSetup = true;
            for (int i = 0; i < allies.Count; i++)
            {
                allies[i].currentState = State.Idle;
            }
        }
        else if (enemyType == EnemyType.WhiteWolf)
        {
            enemyTargets.Clear();
            distCheckEnemyTargets.Clear();

            // run away to set place
            if (runAwayPosition != null)
            {
                RunAway(runAwayPosition);
            }
            currentState = State.Follow;
        }
        else if (enemyType == EnemyType.WoodCutter)
        {
            enemyTargets.Clear();
            distCheckEnemyTargets.Clear();

            targetHolder.LookAt(myPos);
            targetObject = playerRef.transform;
            Transform newTarget = GameObject.Find("Target1").transform;
            newTarget.position = playerRef.transform.position;
            newTarget.LookAt(myPos);
            newTarget.position += newTarget.forward * 1.0f;
            targetPos = newTarget;
            targetObject = playerRef.transform;

            enemyTargets.Add(playerRef.transform);
            distCheckEnemyTargets.Add(playerRef.transform);
        }
    }

    /// <summary>
    /// Player becomes ally
    /// </summary>
    public void SetupPlayerAlly()
    {
        currentState = State.Idle;
        playerIsAnAlly = true;
        weapon1.playerIsAlly = true;
        if (weapon2 != null)
        {
            weapon2.playerIsAlly = true;
        }
        if (weapon3 != null)
        {
            weapon3.playerIsAlly = true;
        }
        if (weapon4 != null)
        {
            weapon4.playerIsAlly = true;
        }
    }

    /// <summary>
    /// Check for ally hits to prevent them or deal damage if not
    /// </summary>
    /// <param name="other">Other complex enemy</param>
    public void HitByAI(GameObject other)
    {
        if (allies.Contains(other.GetComponent<ComplexEnemy>()))
        {
            return;
        }
        if (other != null)
        {
            ComplexEnemy otherenemy = other.GetComponentInParent<ComplexEnemy>();
            if (otherenemy.enemyType != enemyType)
            {
                TakeDamage(1);
            }
        }
        else
        {
            Debug.Log("other is null");
        }
    }

    /// <summary>
    /// Transfers ally list to a new leader
    /// </summary>
    void TransferPackDataToNewLeader()
    {
        allies.RemoveAt(0);
        if (allies.Count > 0)
        {
            allies[0].ReceivePackData(allies);
        }
    }

    /// <summary>
    /// Sets the new pack leader
    /// </summary>
    /// <param name="pack">List of the pack</param>
    void ReceivePackData(List<ComplexEnemy> pack)
    {
        packLeader = true;
        allies = pack;
    }

    /// <summary>
    /// Throws debug line if no pack has been arranged
    /// </summary>
    void NoPack()
    {
        if (!hasPack && aggroState != AggroType.EnemyList)
        {
            if (enemyType == EnemyType.Wolf)
            {
                Debug.Log(name + " hasn't been assigned to a pack");
            }
            else if (enemyType == EnemyType.Human)
            {
                Debug.Log(name + " hasn't been assigned to a group");
            }

            targetPos = GameObject.Find("Target1").transform;
        }

        if (enemyType == EnemyType.Human && allies.Count == 0 && !hasPack)
        {
            allies.Add(this);
            packLeader = true;
            SetUpPack();
            IndividualTargetSetup(0);
        }
    }

    /// <summary>
    /// Sets up allies in pack to belong to a pack
    /// </summary>
    void SetUpPack()
    {
        for (int i = 0; i < allies.Count; i++)
        {
            if (swapAllies)
            {
                allies.Add(allies[0].gameObject.GetComponentsInChildren<ComplexEnemy>()[1]);
                Destroy(allies[0].gameObject.GetComponent<ComplexEnemy>());
                allies.RemoveAt(0);
                allies[allies.Count - 1].hasPack = true;
                allies[allies.Count - 1].SetPackLeader(this);
            }
            else
            {
                allies[i].hasPack = true;
                allies[i].SetPackLeader(this);
            }
        }
        swapAllies = false;
        targetObject = playerRef.transform;
    }

    void SwapReference()
    {
        for (int i = 0; i < enemyTargets.Count; i++)
        {
            enemyTargets[i] = enemyTargets[i].gameObject.GetComponentsInChildren<ComplexEnemy>()[1].transform;
        }
    }

    void SetPackLeader(ComplexEnemy leader)
    {
        leaderRef = leader;
    }

    /// <summary>
    /// Updates targets around player
    /// </summary>
    /// <param name="member">Memeber that dies</param>
    void MemberDied(ComplexEnemy member)
    {
        Transform newTarget = GameObject.Find("Target" + member.targetRef).transform;
        newTarget.localPosition = Vector3.zero;

        for (int i = 0; i < allies.Count; i++)
        {
            if (allies[i] == member)
            {
                allies.RemoveAt(i);
                break;
            }
        }

        if (allies.Count <= 2)
        {
            IndividualTargetSetup(allies.Count - 1);
        }
    }

    void UpdateTargetHolder(Vector3 target)
    {
        targetHolder.LookAt(target);
    }

    /// <summary>
    /// Sets up animation names depending on enemy type
    /// </summary>
    void SetAnimRefs()
    {
        switch (enemyType)
        {
            case EnemyType.Human:
                animJump = "Jump";
                animMove = "Walking";
                animDodge = "Dodge";

                animNames.combo = "Combo";
                animNames.dodge = "Dodge";
                animNames.forward = "StandardAttack";
                animNames.horizontal = "Horizontal";
                animNames.jump = "Jump";
                animNames.kick = "Kick";
                animNames.die = "Death";
                break;
            case EnemyType.Wolf:
                animJump = "Slam";
                animMove = "Walking";
                animDodge = "Back Dodge";

                animNames.combo = "Combo";
                animNames.dodge = "Dodge";
                animNames.forward = "StandardAttack";
                animNames.horizontal = "Horizontal";
                animNames.jump = "Jump";
                animNames.kick = "Heavy";
                animNames.die = "Death";
                break;
            case EnemyType.WoodCutter:
                animJump = "Jump";
                animMove = "Walking";
                animDodge = "Dodge";

                animNames.combo = "Combo";
                animNames.dodge = "Dodge";
                animNames.forward = "StandardAttack";
                animNames.horizontal = "Horizontal";
                animNames.jump = "Jump";
                animNames.kick = "Kick";
                animNames.die = "Death";
                break;
            case EnemyType.WhiteWolf:
                animJump = "Jump";
                animMove = "Walk";
                animDodge = "Dodge";

                animNames.combo = "attackLightcombo";
                animNames.dodge = "";
                animNames.forward = "attackLightcombo";
                animNames.horizontal = "attackHeavy";
                animNames.jump = "";
                animNames.kick = "";
                animNames.die = "";
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Determines getting hurt or not
    /// </summary>
    /// <param name="damage"></param>
    /// <returns></returns>
    public bool Hurt(float damage)
    {
        if (aggroState == AggroType.Neutral || aggroState == AggroType.Never)
        {
            UpdateMorality();
        }

        if (aggroState == AggroType.Provoked)
        {
            hasBeenAttacked = true;
        }

        if (!playerIsAnAlly && hitDamageTimer <= 0)
        {
            hitDamageTimer = hitDamageDelay;
            TakeDamage(damage);
            SequenceListener.PlayerHitEnemy(gameObject);
        }
        else
        {
            return false;
        }

        return true;
    }

    // Controls for turning on and off weapon colliders
    public void AttackStartLeft()
    {
        weapon1.on = true;

        if(weapon3 != null)
        {
            weapon3.on = true;
        }
    }

    public void AttackEndLeft()
    {
        weapon1.on = false;

        if (weapon3 != null)
        {
            weapon3.on = false;
        }
    }

    public void AttackStartRight()
    {
        weapon2.on = true;

        if (weapon4 != null)
        {
            weapon4.on = true;
        }
    }

    public void AttackEndRight()
    {
        weapon2.on = false;

        if (weapon4 != null)
        {
            weapon4.on = false;
        }
    }

    public void AttackOn()
    {
        AttackStartLeft();
    }

    public void AttackOff()
    {
        AttackEndLeft();
    }

    /// <summary>
    /// Adds enemy targets positions into list
    /// </summary>
    void UpdateRefPostions()
    {
        for (int i = 0; i < enemyTargets.Count; i++)
        {
            distCheckEnemyTargets.Add(enemyTargets[i].gameObject.GetComponent<ComplexEnemy>().UpdateRefPos());
        }
    }

    /// <summary>
    /// Returns position of actual enemy, not holder gameobject
    /// </summary>
    public Transform UpdateRefPos()
    {
        return myPos;
    }

    public IEnumerator PlayDialogue(string text, float secondsWait, Action callback)
    {
        if (currentState == State.Dead)
        {
            callback.Invoke();
        }
        else
        {
            speechBackground.enabled = true;
            speech.text = text;

            while (secondsWait > 0)
            {
                secondsWait -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            // REMOVE TEXT
            speech.text = "";
            speechBackground.enabled = false;
            callback.Invoke();
        }
    }

    public void StopDialogue()
    {
        speech.text = "";
        speechBackground.enabled = false;
    }

    /// <summary>
    /// Sets position to move to
    /// </summary>
    /// <param name="runTo">Positons to move to</param>
    public void RunAway(Transform runTo)
    {
        targetPos = runTo;
        targetObject = runTo;
    }
}
