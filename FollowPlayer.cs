﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    Transform myTransform;
    Transform parent;

	// Use this for initialization
	void Start () {
        parent = transform.parent;
        transform.parent = null;
        PlayerManager.changeEventCallback.Add(ChangeOfPlayerState);
    }
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = parent.position;
	}

    void ChangeOfPlayerState(PlayerManager.PlayerState state)
    {
        parent = PlayerManager.GetActivePlayer().transform;
    }
}
