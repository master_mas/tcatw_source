﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeByDistancePlayer : MonoBehaviour {
    AudioSource source;
    Vector3 posLastFrame;
    public bool playing = true;
    [SerializeField]
    float volumeByDistanceMultiplier=1;
    [SerializeField]
    float pitchByDistanceMultiplier=1;


    [SerializeField]
    float speedOfVolChange=1, speedOfPitchChange=1;

    float currentPitch, currentVol;

    // Use this for initialization
    void Start ()
    {
        source = GetComponent<AudioSource>();
        posLastFrame = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (playing)
        {
            float distance = Vector3.Distance(transform.position, posLastFrame);
            distance = distance * distance;

            float targetVol = distance * volumeByDistanceMultiplier;
            float targetPitch = distance * pitchByDistanceMultiplier;
            targetPitch += 1f;

            currentPitch = Mathf.Lerp(currentPitch, targetPitch, Time.deltaTime * speedOfVolChange);
            currentVol = Mathf.Lerp(currentVol, targetVol, Time.deltaTime * speedOfPitchChange);


        }
        else
        {
            currentVol = Mathf.MoveTowards(source.volume, 0, Time.deltaTime * 4);
        }

        source.pitch = currentPitch;
        source.volume = currentVol;
        posLastFrame = transform.position;
    }

    public void Play ()
    {
        playing = true;
        source.volume = 0;
        source.pitch = 0.5f;
    }

    public void Stop()
    {
        playing = false;
    }
}
