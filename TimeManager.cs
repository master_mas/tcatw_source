﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// this class manages all interaction with the time scale
/// </summary>
public class TimeManager : MonoBehaviour {

    public static TimeManager instance;

    private void Awake()
    {
        instance = this;
    }

    public static void PauseTime()
    {
        if (instance == null)
        {
            GameObject temp = new GameObject();
            temp.AddComponent<TimeManager>();
        }
        instance.StartCoroutine(instance.Pause());
    }

    IEnumerator Pause()
    {
        float timer = 0;
        Time.timeScale = 0.01f;

        while (timer < 0.15)
        {
            timer += Time.unscaledDeltaTime;
            yield return null;
        }
        Time.timeScale = 1f;
    }
}
