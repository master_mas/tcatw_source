﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    [SerializeField]
    GameObject hitEffectPrefab;
    public bool on;
    [HideInInspector]
    public bool playerIsAlly = false;

    [HideInInspector]
    public float damage = 1;

    void OnTriggerEnter(Collider collision)
    {
        if (on)
        {
            if (collision.gameObject.CompareTag("Player") && !playerIsAlly)
            {
                if (PlayerController.instance.Hurt(damage, false))
                {
                    SequenceListener.NPCHitPlayer(gameObject.transform.root.gameObject);
                    //GameObject temp = Instantiate(hitEffectPrefab, transform.position, Random.rotation);
                    //Destroy(temp, 3);
                }
            }
            else if (collision.gameObject.CompareTag("Enemy"))
            {
                if (collision.gameObject.name != gameObject.transform.name)
                {
                    collision.gameObject.GetComponentInParent<ComplexEnemy>().HitByAI(gameObject);
                    SequenceListener.NPCHitsNPC(collision.gameObject, gameObject);
                }
            }
        }
    }
}
