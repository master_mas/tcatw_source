﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternBrightnessByWind : MonoBehaviour {
    public Light light;
    float lightPow, emisPow;
    public Color emissionColor,emissionTempCol;
    public MeshRenderer meshRend;
    Material emissionMat;
	// Use this for initialization
	void Start () {
        emissionMat = meshRend.materials[1];
        emissionColor = emissionMat.GetColor("_EmissionColor");
        lightPow = light.intensity;

    }
	
	// Update is called once per frame
	void Update () {
        if (WindManager.instance != null)
        {
            light.intensity = Mathf.Lerp(lightPow, 0.2f, WindManager.GetCurrentWind().magnitude);
            emissionTempCol = emissionColor * Mathf.Lerp(1.5f, 0.25f, WindManager.GetCurrentWind().magnitude);
            emissionMat.SetColor("_EmissionColor", emissionTempCol);
        }
	}
}
