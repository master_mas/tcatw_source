﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

    Transform cam;
    Transform targetPos;
    Transform myPos;

    Vector3 offset;

    float distance = 14;

    // Use this for initialization
    void Start () {
        cam = Camera.main.transform;
        myPos = transform;
        targetPos = transform.parent.GetComponentInChildren<Animator>().transform;
        offset = myPos.localPosition;

        NPC npcChecker = transform.parent.GetComponent<NPC>();
        ComplexEnemy enemyChecker = transform.parent.GetComponent<ComplexEnemy>();

        if (npcChecker != null &&  npcChecker.enemyType == NPC.Type.WhiteWolf ||
            enemyChecker != null && enemyChecker.enemyType == ComplexEnemy.EnemyType.WhiteWolf)
        {
            offset += new Vector3(0, 1.0f, 0);
        }
        else if (npcChecker != null && npcChecker.enemyType == NPC.Type.WoodCutter ||
            enemyChecker != null && enemyChecker.enemyType == ComplexEnemy.EnemyType.WoodCutter)
        {
            offset += new Vector3(0, 1.0f, 0);
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (Mathf.Abs(PlayerManager.GetActivePlayer().transform.position.x - transform.position.x) > distance || Mathf.Abs(PlayerManager.GetActivePlayer().transform.position.z - transform.position.z) > distance)
        {
        }
        else { 
            transform.LookAt(cam);
            myPos.position = targetPos.position + offset;
        }
	}
}
