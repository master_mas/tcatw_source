﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLowRockCounter : MonoBehaviour {

    public static GLowRockCounter instance;
    public GameObject box;
    public int destroyedCount = 0;
    private float spwanDelay = 0.5f;
    private float maxSpawnCount = 20;
    private int spawnCounter = 0;
	// Use this for initialization
	void Start () {
        instance = this;
	}

    void Update()
    {
        if (destroyedCount == 29 && spwanDelay <= 0 && spawnCounter < maxSpawnCount)
        {
            Instantiate(box, PlayerManager.GetActivePlayer().transform.position + new Vector3(Random.Range(-1, 1), 30, Random.Range(-1, 1)), Quaternion.identity);
            spawnCounter++;
            spwanDelay = 0.5f;
        }
        else if (spwanDelay > 0)
        {
            spwanDelay -= Time.deltaTime;
        }
    }

    public void IncreaseCount()
    {
        destroyedCount++;
    }
}
