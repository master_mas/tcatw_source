﻿using UnityEngine;

public class RandomImageRotation : MonoBehaviour
{
    public float rotatationDuration = 2f;

    public float rotationAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    private int angle = 0;
    private int Angle
    {
        get { return angle; }
        set
        {
            angle = value;
            if (angle > 360)
            {
                angle = angle - 360;
            }
        }
    }

    void Update()
    {

        if (rotatationDuration > 0)
        {
            Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 2.5f);
            Angle += 10;

            rotatationDuration -= Time.deltaTime * decreaseFactor;
        }
        else
            rotatationDuration = 0f;
    }
}
