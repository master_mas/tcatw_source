﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Creator : MonoBehaviour
{
    public enum Discipline
    {
        Programmer,
        Designer,
        Artist,
        Environment_Artist,
        Character_Artist,
        Composer,
        
    }

    public string name;
    public Discipline discipline;
    public int order;

    private List<CharacterStorageContainer> characterStorageContainers = new List<CharacterStorageContainer>();
    private List<CharacterStorageContainer> characterStorageContainers2 = new List<CharacterStorageContainer>();

    private int fadeCount = 60;
    private float currentFadeCount = 0;

    private bool disable = false;
    private bool fadeIn = true;

    private float waitTime = 3;
    private float currentWaitTime = 0;

    private float fadeTime = 1;
    private float currentFadeTime = 0;

    public List<Image> image = new List<Image>();
    private Text text = null;
    private Color color;

    private void Awake()
    {
        
    }

    public void Start()
    {
        CreditsFlow.instance.register(this);

        if(image.Count == 0)
            image = GetComponentsInChildren<Image>().ToList();

        color = Color.white;
        color.a = 0;
        image.ForEach(x => x.color = color);

        text = GetComponentInChildren<Text>();

        foreach (var c in name)
            characterStorageContainers.Add(new CharacterStorageContainer(new[] { Color.black, Color.white }, fadeCount, c));

        foreach (var c in discipline.ToString())
        {
            char character = c;
            if (character == '_')
                character = ' ';

            characterStorageContainers2.Add(new CharacterStorageContainer(new[] { Color.black, Color.grey }, fadeCount, character));
        }

        if(text != null)
            text.text = convertListToString(characterStorageContainers) + "\n" + convertListToString(characterStorageContainers2);

        gameObject.SetActive(false);
    }

    public void Update()
    {
        if (disable)
            return;

        characterStorageContainers.ForEach(x => x.update(Time.deltaTime));
        characterStorageContainers2.ForEach(x => x.update(Time.deltaTime));
        if(text != null)
            text.text = convertListToString(characterStorageContainers) + "\n" + convertListToString(characterStorageContainers2);

        if (FadeIn)
        {
            if (currentFadeCount <= fadeCount)
            {
                color.a = 1f * (currentFadeCount / fadeCount);
                image.ForEach(x => x.color = color);
                currentFadeCount++;
            }
        }
        else
        {
            if (currentFadeCount >= 0)
            {
                color.a = 1f * (currentFadeCount / fadeCount);
                image.ForEach(x => x.color = color);
                currentFadeCount--;
            }
        }
    }

    public bool FadeIn
    {
        get { return fadeIn; }
        set
        {
            fadeIn = value;
            characterStorageContainers.ForEach(x => x.Reverse = !value);
            characterStorageContainers2.ForEach(x => x.Reverse = !value);
            gameObject.SetActive(true);

            if(fadeIn)
                currentFadeCount = 0;
            else
                currentFadeCount = fadeCount;
        }
    }

    public bool Disable
    {
        get { return disable; }
        set
        {
            disable = value;
            characterStorageContainers.ForEach(x => x.disable = disable);
            characterStorageContainers2.ForEach(x => x.disable = disable);
            gameObject.SetActive(!disable);
        }
    }

    public static string convertListToString(List<CharacterStorageContainer> list)
    {
        var data = "";
        list.ForEach(x =>
        {
            if (x != null)
                data += x;
        });
        return data;
    }

    [Serializable]
    public class CharacterStorageContainer
    {
        private readonly char character;
        private readonly Color[] colors;
        private readonly int fadeCount;
        private bool _reverse;
        private string color;

        private float currentFadeCount;

        public bool disable = true;
        private float movingTo = 1;
        private bool newLine;

        public CharacterStorageContainer(Color[] colors, int fadeCount, char character, bool newline = false)
        {
            this.colors = colors;
            this.fadeCount = fadeCount;
            this.character = character;

            processNewColor(colors[0]);

            newLine = newline;
        }

        public bool Reverse
        {
            private get { return _reverse; }
            set
            {
                _reverse = value;

                if (_reverse)
                {
                    disable = false;
                    currentFadeCount = 0;
                    movingTo = colors.Length;
                }
            }
        }

        public void update(float delta)
        {
            if (disable)
                return;

            if (Reverse)
            {
                if (currentFadeCount <= 0)
                    if (movingTo <= 1)
                    {
                        processNewColor(colors[0]);
                        disable = true;
                    }
                    else
                    {
                        movingTo--;
                        currentFadeCount = fadeCount;
                    }
            }
            else
            {
                if (currentFadeCount > fadeCount)
                    if (movingTo >= colors.Length - 1)
                    {
                        processNewColor(colors[colors.Length - 1]);
                        disable = true;
                    }
                    else
                    {
                        movingTo++;
                        currentFadeCount = 0;
                    }
            }

            if (disable)
                return;

            var gradient = new Gradient();
            var gck = new GradientColorKey[2];
            var gak = new GradientAlphaKey[2];

            if (Reverse)
            {
                gck[0].color = colors[(int)(movingTo - 1)];
                gck[0].time = 0;
                try
                {
                    gck[1].color = colors[(int)movingTo];
                }
                catch (IndexOutOfRangeException)
                {
                    Debug.Log(movingTo - 1);
                }
                gck[1].time = 1;

                gak[0].alpha = /*(currentFadeCount / fadeCount / 10) + (movingTo) / colors.Length*/ 1;
                gak[0].time = 0;
                gak[1].alpha = /*(currentFadeCount / fadeCount / 10) + (movingTo - 1) / colors.Length*/ 0;
                gak[1].time = 1;
            }
            else
            {
                gck[0].color = colors[(int)(movingTo - 1)];
                gck[0].time = 0;
                gck[1].color = colors[(int)movingTo];
                gck[1].time = 1;

                gak[0].alpha = currentFadeCount / fadeCount / 10 + (movingTo - 1) / colors.Length;
                gak[0].time = 0;
                gak[1].alpha = currentFadeCount / fadeCount / 10 + movingTo / colors.Length;
                gak[1].time = 1;
            }

            gradient.SetKeys(gck, gak);

            processNewColor(gradient.Evaluate(currentFadeCount / fadeCount));

            if (Reverse)
                currentFadeCount--;
            else
                currentFadeCount++;
        }

        private void processNewColor(Color color)
        {
            this.color = ColorUtility.ToHtmlStringRGB(color);
        }

        public override string ToString()
        {
            return (newLine ? "\n" : "") + "<color=#" + color + ">" + character + "</color>";
        }
    }
}