﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HubrisBarUI : MonoBehaviour {
    public AudioSource hubrisGain, humanityGain;
    public GameObject rose, min, max,wolfIcon, humanIcon;
    Image img;
    Material mat;
    float _bloodValue = 0.5f;
    float bloodValue
    {
        get { return _bloodValue; }
        set
        {
            if (value > 1) _bloodValue = 1;
            else if (value < 0) _bloodValue = 0;
            else
                _bloodValue = value;
        }
    }

    float _bloodTarget = 0.5f;
    float bloodTarget
    {
        get { return _bloodTarget; }
        set
        {
            if (value > 1) _bloodTarget = 1;
            else if (value < 0) _bloodTarget = 0;
            else
                _bloodTarget = value;
        }
    }

    float _roseValue = 0.5f;
    float roseValue
    {
        get { return _roseValue; }
        set
        {
            if (value > 1) _roseValue = 1;
            else if (value < 0) _roseValue = 0;
            else
                _roseValue = value;
        }
    }

    float _roseTarget = 0.5f;
    float roseTarget
    {
        get { return _roseTarget; }
        set
        {
            if (value > 1) _roseTarget = 1;
            else if (value < 0) _roseTarget = 0;
            else
                _roseTarget = value;
        }
    }

    public AnimationCurve curve;

    //outlines
    public Image wolfBloom, humanBloom;
    //public Outline wolf1, wolf2, human1, human2;

	void Start ()
    {
        img = GetComponent<Image>();
        mat = img.material;
        mat.SetFloat("_RampPos", bloodValue);
	}
	
	// Update is called once per frame
	void Update ()
    {
        rose.transform.position = Vector3.Lerp(min.transform.position, max.transform.position, roseValue);

        mat.SetFloat("_RampPos", Mathf.Lerp(0.01f,1f, bloodValue));

        roseValue = Mathf.Lerp(roseValue, roseTarget, Time.deltaTime*2);
        bloodValue = Mathf.Lerp(bloodValue, bloodTarget, Time.deltaTime*3);

        float scale = Mathf.Clamp(roseValue - roseTarget,-1,1);

        wolfIcon.transform.localScale = Vector3.one * (1 + (curve.Evaluate(wolfIconPulse) * 0.2f));

        humanIcon.transform.localScale = Vector3.one * (1 + (curve.Evaluate(humanIconPulse) * 0.2f));
        
        
        if (MoralityController.Self.moralityChangeOverTime)
        {
            if (MoralityController.Self.PlayerOnPath)
            {
                wolfBloom.color = new Color(wolfBloom.color.r, wolfBloom.color.g, wolfBloom.color.b, 0);
                humanBloom.color = new Color(humanBloom.color.r, humanBloom.color.g, humanBloom.color.b, Mathf.Abs( Mathf.Sin(Time.time*2)));
            }
            else
            {
                humanBloom.color = new Color(humanBloom.color.r, humanBloom.color.g, humanBloom.color.b, 0);
                wolfBloom.color = new Color(wolfBloom.color.r, wolfBloom.color.g, wolfBloom.color.b, Mathf.Abs(Mathf.Sin(Time.time*2)));
            }
        }else
        {
            humanBloom.color = new Color(humanBloom.color.r, humanBloom.color.g, humanBloom.color.b, 0);
            wolfBloom.color = new Color(wolfBloom.color.r, wolfBloom.color.g, wolfBloom.color.b, 0);
        }

        wolfIconPulse -= Time.deltaTime * 2;
        humanIconPulse -= Time.deltaTime * 2;

        ModUI(Mathf.Lerp(0,1, (-(MoralityController.Self.Weight)/30) + 0.5f));
    }

    float wolfIconPulse = 0, humanIconPulse = 0;
    void ModUI (float pos)
    {
        if (Mathf.Abs(bloodTarget - pos) > 0.001f)
        {
            if (bloodTarget < pos)
            {
                wolfIconPulse = 1;
            }
            else if (bloodTarget > pos)
            {
                humanIconPulse = 1;
            }
        }

        if (Mathf.Abs(bloodTarget - pos) > 0.1f)
        {
            if (bloodTarget < pos)
            {
                hubrisGain.Play();
            }
            else if (bloodTarget > pos)
            {
                humanityGain.Play();
            }
        }

        bloodTarget = pos;
        roseTarget = pos;
    }
}
