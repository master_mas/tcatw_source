﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NPCGestures))]
public class NPC : EntityBase
{

    public enum Type
    {
        Human,
        Wolf,
        WoodCutter,
        WhiteWolf
    }

    public enum State
    {
        Idle,
        Path,
        Dead
    }

    public enum PathChoice
    {
        Loop,
        BackAndForth,
        PointToPoint
    }

    // Enums
    public State currentState = State.Idle;
    public Type enemyType = Type.Human;
    public PathChoice pathType = PathChoice.Loop;

    public List<Transform> waypointPath;

    // Ints
    private int currentPathPoint = 0;
    private int currentPathDirection;

    // Floats
    public float rotationSmooth = 360;
    private float forwardTargetSpeed;
    private float whiteWolfSpeed = 3;

    // Bools
    private bool runAway = false;
    public bool canBeHurt = false;

    private Animator anim;
    private NavMeshAgent nav;
    private NavMeshPath navPath;

    private Text speech;
    private Image speechBackground;

    private Vector3 runAwayDestionation;

    private Transform myPos;

    // GameObjects
    public GameObject wolf;
    public GameObject human;
    public GameObject whiteWolf;
    public GameObject woodCutter;

    // Use this for initialization
    void Start()
    {
        navPath = new NavMeshPath();
        anim = GetComponentInChildren<Animator>();
        nav = GetComponentInChildren<NavMeshAgent>();
        speech = GetComponentInChildren<Text>();
        speechBackground = GetComponentInChildren<Image>();
        speechBackground.enabled = false;
        myPos = anim.transform;
        if (currentState == State.Path && waypointPath.Count <= 1)
        {
            Debug.Log("Path state selected, but has no path or only one waypoint. State changed to Idle.");
            currentState = State.Idle;
        }
        else
        {
            currentPathPoint = 1;
            currentPathDirection = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Path:
                PathUpdate();
                break;
            case State.Dead:
                break;
            default:
                break;
        }

        float tempForwardSpeed = Mathf.MoveTowards(anim.GetFloat("Forward"), forwardTargetSpeed, Time.deltaTime * 5);
        anim.SetFloat("Forward", tempForwardSpeed);
    }

    void IdleUpdate()
    {
        forwardTargetSpeed = 0;
        anim.SetFloat("InBattle", 0);
    }

    void PathUpdate()
    {
        if (runAway)
        {
            nav.CalculatePath(runAwayDestionation, navPath);
            anim.SetFloat("InBattle", 1);
        }
        else
        {
            nav.CalculatePath(waypointPath[currentPathPoint].position, navPath);
        }

        if (navPath.corners.Length == 0)
        {
            return;
        }

        float dist = Vector3.Distance(myPos.position, navPath.corners[navPath.corners.Length - 1]);
        if (dist > 0.5f)
        {
            forwardTargetSpeed = 1;
            if (enemyType == Type.WhiteWolf)
            {
                nav.Move(myPos.forward * whiteWolfSpeed * Time.deltaTime);
            }
            for (int i = 0; i < navPath.corners.Length - 1; i++)
            {
                Debug.DrawLine(navPath.corners[i], navPath.corners[i + 1]);
            }
            Quaternion lookDir = Quaternion.LookRotation(navPath.corners[1] - myPos.position, Vector3.up);
            myPos.rotation = Quaternion.RotateTowards(myPos.rotation, lookDir, Time.deltaTime * rotationSmooth);
        }
        else
        {
            TargetNextPoint();
        }
    }

    /// <summary>
    /// Gets next point on path
    /// </summary>
    void TargetNextPoint()
    {
        switch (pathType)
        {
            case PathChoice.Loop:
                if (currentPathPoint == waypointPath.Count - 1)
                {
                    currentPathPoint = 0;
                    Debug.Log(name + " has reached the end of the path. Back to start.");
                    return;
                }

                break;
            case PathChoice.BackAndForth:

                if (currentPathPoint == 0 || currentPathPoint == (waypointPath.Count - 1))
                {
                    currentPathDirection *= -1;
                    Debug.Log(name + " has reached the end of the path, turning around.");
                }

                break;
            case PathChoice.PointToPoint:
                if (runAway)
                {
                    Destroy(gameObject);
                }
                else if (currentPathPoint == waypointPath.Count - 1)
                {
                    Debug.Log(name + " has reached the end of the path.");
                    currentState = State.Idle;
                }
                break;
            default:
                break;
        }
        if (currentPathPoint < waypointPath.Count)
        {
            currentPathPoint += currentPathDirection;
        }
    }

    /// <summary>
    /// Draws debuy path line
    /// </summary>
    private void OnDrawGizmos()
    {
        if (waypointPath != null && waypointPath.Count > 1)
        {
            Gizmos.color = Color.white;
            for (int i = 0; i < waypointPath.Count - 1; i++)
            {
                if (waypointPath[i] == null)
                {
                    waypointPath.RemoveAt(i);
                    RenameWaypoints(i);
                    return;
                }
                else if (waypointPath[i + 1] == null)
                {
                    waypointPath.RemoveAt(i + 1);
                    RenameWaypoints(i + 1);
                    return;
                }
                Gizmos.DrawLine(waypointPath[i].position, waypointPath[i + 1].position);
            }
        }
    }

    /// <summary>
    /// Names waypoints after given starting point
    /// </summary>
    /// <param name="startPoint">point to start at</param>
    private void RenameWaypoints(int startPoint)
    {
        for (int i = startPoint; i < waypointPath.Count - 1; i++)
        {
            waypointPath[i].name = name + "_waypoint_" + i;
        }
    }

    /// <summary>
    /// Swaps current state
    /// </summary>
    public void SwapIdleOrPathState()
    {
        if (currentState == State.Idle)
        {
            currentState = State.Path;
        }
        else if (currentState == State.Path)
        {
            currentState = State.Idle;
        }
    }

    public IEnumerator PlayDialogue(string text, float secondsWait, Action callback)
    {
        if (currentState == State.Dead)
        {
            callback.Invoke();
        }
        else
        {
            speechBackground.enabled = true;
            speech.text = text;

            while (secondsWait > 0)
            {
                secondsWait -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            // REMOVE TEXT

            if (speech.text.Equals(text))
            {
                speech.text = "";
                speechBackground.enabled = false;
            }

            callback.Invoke();
        }
    }

    public void StopDialogue()
    {
        speech.text = "";
        speechBackground.enabled = false;
    }

    /// <summary>
    /// Handles damage taken
    /// </summary>
    /// <param name="amount">Damage amount</param>
    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        if (health == 0)
        {
            GetComponentInChildren<BoxCollider>().enabled = false;
            nav.enabled = false;
            currentState = State.Dead;
            anim.CrossFade("Death", .1f, 1);
            SequenceListener.PlayerKilledNPC(gameObject);
        }
        else
        {
            SequenceListener.PlayerHitNPC(this);

            switch (enemyType)
            {
                case Type.Human:
                    GlobalSoundPlayer.Play("HumanNpcHurt", transform.position);
                    break;
                case Type.Wolf:
                    GlobalSoundPlayer.Play("WolfNpcHurt", transform.position);
                    break;
                case Type.WoodCutter:
                    GlobalSoundPlayer.Play("WoodcutterHurt", transform.position);
                    break;
                case Type.WhiteWolf:
                    GlobalSoundPlayer.Play("WhitewolfHurt", transform.position);
                    break;
                default:
                    break;
            }
        }
    }

    /// <summary>
    /// Determines if can be hurt
    /// </summary>
    /// <param name="amount">Damage amount</param>
    /// <returns></returns>
    public bool Hurt(float amount)
    {
        if (canBeHurt)
        {
            TakeDamage(amount);
            SequenceListener.PlayerHitNPC(this);
            return true;
        }
        else
        {
            SequenceListener.PlayerHitNPC(this);
        }
        return false;
    }

    /// <summary>
    /// Moves npc to specific spot
    /// </summary>
    /// <param name="moveTo">Spot to move to</param>
    public void RunAway(Transform moveTo)
    {
        currentState = State.Path;
        pathType = PathChoice.PointToPoint;
        runAway = true;

        runAwayDestionation = moveTo.position;
        gameObject.GetComponentInChildren<BoxCollider>().enabled = false;
    }
}
