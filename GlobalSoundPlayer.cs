﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSoundPlayer : MonoBehaviour {

    // A simpleton that spawns a prefab
    static GlobalSoundPlayer _instance;
    public static GlobalSoundPlayer instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gO = Instantiate((GameObject)Resources.Load("GlobalSound"));
                _instance = gO.GetComponent<GlobalSoundPlayer>();
                _instance.Init();
                return _instance;
            }
            else
            {
                return _instance;
            }
        }
        set
        {
            if (instance != null)
            {
                Debug.LogWarning("You are spawning a global sound manager when one already exists");
            }
            _instance = value;
        }
    }

    Dictionary<string, SoundPlayer> sounds = new Dictionary<string, SoundPlayer>();

    private void Init()
    {
        foreach (Transform item in transform)
        {
            SoundPlayer tempSound = item.GetComponent<SoundPlayer>();
            string gOName = item.name;

            sounds.Add(gOName, tempSound);
        }
    }

    public static void Play(string soundName, Vector3 position)
    {
        if (instance.sounds.ContainsKey(soundName))
        {
            GameObject gO = instance.sounds[soundName].gameObject;
            GameObject tempgO = Instantiate(gO, position, Quaternion.identity);
            tempgO.GetComponent<SoundPlayer>().playOnStart = true;
            Destroy(tempgO, 5);
        }
        else
        {
            Debug.Log("Sound " + soundName + " not found", instance.gameObject);
        }
    }
}
