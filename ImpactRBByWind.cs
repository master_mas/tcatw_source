﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactRBByWind : MonoBehaviour {
    Rigidbody rb;
    public float windImpactMultiplier = 1;
    float randValue;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        randValue = Random.Range(0.9f, 1.1f);
    }
	
	// Update is called once per frame
	void Update () {
        if (WindManager.instance!= null)
            rb.AddForce(WindManager.GetCurrentWind()* randValue * windImpactMultiplier);
    }
}
