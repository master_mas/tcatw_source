﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternSway : MonoBehaviour {
    public SoundPlayer sound;
    Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        timer = Random.Range(0, 5)+5;
        rb.AddForce(new Vector3(Random.value * 100, 0, Random.value * 100));
    }

    float timer;
	void Update () {
        timer -= Time.deltaTime;

        if (rb.IsSleeping())
        {
            timer = 0;
        }
        if (timer <= 0)
        {
            timer = Random.Range(0, 5) + 5;
            sound.Play();
            rb.AddForce(new Vector3(10 + Random.value * 10, 0, 10 + Random.value * 10));
        }

    }
}
