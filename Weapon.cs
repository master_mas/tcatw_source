﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    // automatically turns on and off trails
    bool _on;
    public bool on
    {
        get { return _on; }
        set
        {
            if (value != _on)
            {
                foreach (MeleeWeaponTrail item in trails)
                {
                    if (item != null)
                    item.Emit = value;
                }
            }
            _on = value;
        }
    }

    public bool heavy;

    public GameObject hitEffectPrefab, heavyHitPrefab;

    public ParticleSystem sparkEmitter;
    public MeleeWeaponTrail[] trails;

    float lightAttackDamage = 1, heavyAttackDamage = 2;

    void OnTriggerEnter(Collider collision)
    {
        if (on && collision.gameObject.tag == "Enemy")
        {
            GameObject temp = null;
            NPC npc = collision.gameObject.GetComponentInParent<NPC>();
            ComplexEnemy complexEnemy = collision.gameObject.GetComponentInParent<ComplexEnemy>();
            bool hasHurtOther = true;

            if (npc != null)
            {
                hasHurtOther = npc.Hurt(heavy ? heavyAttackDamage : lightAttackDamage);
            }
            else if (complexEnemy != null)
            {
                // if heavy, send heavy damage else send light
                hasHurtOther = complexEnemy.Hurt(heavy ? heavyAttackDamage : lightAttackDamage);
            }
            else
            {
                collision.gameObject.SendMessage("Hurt", heavy ? heavyAttackDamage : lightAttackDamage, SendMessageOptions.DontRequireReceiver);
            }

            if (hasHurtOther)
            {
                sparkEmitter.Play();
                if (!heavy)
                {
                    CamRig.CamShake(0.1f);
                    temp = Instantiate(hitEffectPrefab, transform.position, Random.rotation);
                    //temp = Instantiate(hitEffectPrefab, collision.contacts[0].point, Quaternion.identity);
                }
                else
                {
                    TimeManager.PauseTime();
                    CamRig.CamShake(0.3f);
                    temp = Instantiate(heavyHitPrefab, transform.position, Random.rotation);
                }
                Destroy(temp, 3);
            }
        }
        else if (on && collision.gameObject.tag == "destructible")
        {
            collision.gameObject.SendMessage("Hurt", SendMessageOptions.DontRequireReceiver);
        }
    }

    public void SetUpDamage (float _lightAttackDamage, float _heavyAttackDamage)
    {
        lightAttackDamage = _lightAttackDamage;
        heavyAttackDamage = _heavyAttackDamage;
    }
}
