﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class MusicMixerEditor : MonoBehaviour {

    public AnimationCurve volumeChangeCurve;

    public enum ModSoundOrMusic
    {
        sound,
        music
    }

    public ModSoundOrMusic modWhichTrack = ModSoundOrMusic.music;
    public AudioMixer mixerToEdit;
    Slider mainSlider;
	// Use this for initialization
	void Start () {
        mainSlider = GetComponent<Slider>();
	}
	
	public void OnValueChange ()
    {
        mixerToEdit.SetFloat(modWhichTrack == ModSoundOrMusic.music ? "MusicVol" : "SoundVol", Mathf.Lerp(-60, 0, volumeChangeCurve.Evaluate(mainSlider.value)));
    }
}
