﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Provide styling for the Node editor based on the Pro/Free versions of Unity Editor
 * Author: Sam Murphy
 */

public static class Styles
{
    public static readonly GUIStyle nodeActive;
    public static readonly GUIStyle nodeActiveSelected;
    public static readonly GUIStyle node;
    public static readonly GUIStyle nodeSelected;
    public static readonly GUIStyle inPoint;
    public static readonly GUIStyle outPoint;
    public static readonly GUIStyle cancelPoint;

    public static readonly float DEFAULT_MAIN_SPACE_SPACING = 138f;
    public static readonly float DEFAULT_COMPLEX_NODE_COMPONENT_SPACING = 10f;

    static Styles()
    {
        //Static constructor check to see if the version of Unity is licensed, and use the appropriate skin as required.
        if (EditorGUIUtility.isProSkin)
        {
            nodeActive = new GUIStyle
            {
                normal = {background = EditorGUIUtility.Load("builtin skins/darkskin/images/node6.png") as Texture2D},
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            nodeActiveSelected = new GUIStyle
            {
                normal =
                {
                    background = EditorGUIUtility.Load("builtin skins/darkskin/images/node6 on.png") as Texture2D
                },
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            node = new GUIStyle
            {
                normal = {background = EditorGUIUtility.Load("builtin skins/darkskin/images/node0.png") as Texture2D},
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            nodeSelected = new GUIStyle
            {
                normal =
                {
                    background = EditorGUIUtility.Load("builtin skins/darkskin/images/node0 on.png") as Texture2D
                },
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            cancelPoint = "flow node 6";
            cancelPoint.border = new RectOffset(4, 4, 12, 12);
        }
        else
        {
            nodeActive = new GUIStyle
            {
                normal = {background = EditorGUIUtility.Load("builtin skins/lightskin/images/node6.png") as Texture2D},
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            nodeActiveSelected = new GUIStyle
            {
                normal =
                {
                    background = EditorGUIUtility.Load("builtin skins/lightskin/images/node6 on.png") as Texture2D
                },
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            node = new GUIStyle
            {
                normal = {background = EditorGUIUtility.Load("builtin skins/lightskin/images/node1.png") as Texture2D},
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            nodeSelected = new GUIStyle
            {
                normal =
                {
                    background = EditorGUIUtility.Load("builtin skins/lightskin/images/node1 on.png") as Texture2D
                },
                border = new RectOffset(12, 12, 12, 12),
                padding = new RectOffset(16, 16, 8, 8)
            };

            cancelPoint = "flow node 3";
            cancelPoint.border = new RectOffset(4, 4, 12, 12);
        }

        inPoint = new GUIStyle
        {
            normal = {background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D},
            active = {background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D},
            border = new RectOffset(4, 4, 12, 12)
        };

        outPoint = new GUIStyle
        {
            normal = {background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn right.png") as Texture2D},
            active =
            {
                background = EditorGUIUtility.Load("builtin skins/lightskin/images/btn right on.png") as Texture2D
            },
            border = new RectOffset(4, 4, 12, 12)
        };
    }
}
#endif