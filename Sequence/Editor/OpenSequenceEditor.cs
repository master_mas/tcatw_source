﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SequenceContainer))]
public class OpenSequenceEditor : Editor
{
    public override void OnInspectorGUI()
    {
        GUILayout.Space(15);
        if (GUILayout.Button("Sequence Editor"))
        {
            ActiveSequenceContainer.editorUsingContainer = (SequenceContainer) target;
            EditorWindow.GetWindow<SequenceEditor>();
        }
        GUILayout.Space(15);
        if (!EditorApplication.isPlaying)
            GUI.enabled = false;
        if (GUILayout.Button("Arm Sequence"))
        {
            ActiveSequenceContainer.stopSequence((SequenceContainer) target);
            ((SequenceContainer)target).armSequence();
        }
        if (!EditorApplication.isPlaying)
            GUI.enabled = true;
        GUILayout.Space(15);
        if (GUILayout.Button("Stop Sequence"))
        {
            ActiveSequenceContainer.stopSequence((SequenceContainer) target);
        }
        GUILayout.Space(15);
    }
}
#endif