﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Scripts.Sequence.EditorNodes;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[InitializeOnLoad]
public class SequenceEditor : EditorWindow
{
    private static Action onLoseFocusAction;
    private static Action onFocusAction;

    private SequenceContainer _sequenceContainer;

    private List<EditorNode> nodes = new List<EditorNode>();
    private List<SequenceConnection> connections = new List<SequenceConnection>();

    private SequenceConnectionPoint selectedInPoint;
    private SequenceConnectionPoint selectedOutPoint;

    //Grid
    private Vector2 offset;
    private Vector2 drag;

    //Using Game
    private bool usingGame = false;

    static SequenceEditor()
    {
        EditorApplication.playmodeStateChanged += gameplayStateChange;
    }

    private static void OpenWindow()
    {
        SequenceEditor window = GetWindow<SequenceEditor>();
        window.titleContent = new GUIContent("Sequence Editor");
        window.minSize = new Vector2(1280, 720);
    }

    private static void gameplayStateChange()
    {
        ActiveSequenceContainer.gameUsingContainer = new List<SequenceContainer>();
        ActiveSequenceContainer.gameUsingNodes.Clear();
    }

    private void OnGUI()
    {
        DrawGrid(20, 0.2f, Color.gray);
        DrawGrid(100, 0.4f, Color.gray);

        DrawConnections();
        DrawNodes();

        DrawConnectionLine(Event.current);

        ProcessNodeEvents(Event.current);
        ProcessEvents(Event.current);

        if (GUI.changed) Repaint();
    }

    public void Update()
    {
        if (ActiveSequenceContainer.editorUsingContainer != null && ActiveSequenceContainer.gameUsingContainer.Contains(ActiveSequenceContainer.editorUsingContainer) && !usingGame)
        {
            OnLostFocus();
            changeOverToGameNodes();
        }

        bool requestGUIUpdate = false;
        foreach (EditorNode node in nodes)
        {
            if(node.updateNode()) { requestGUIUpdate = true; }
        }

        if(requestGUIUpdate)
            Repaint();
    }

    public void OnFocus()
    {
        _sequenceContainer = ActiveSequenceContainer.editorUsingContainer;

        nodes.Clear();
        connections.Clear();
        ActiveSequenceContainer.editorUsingNodes.Clear();

        if (_sequenceContainer == null)
        {
            return;
        }

        CallbackActions.OnClickOutPoint = OnClickOutPoint;
        CallbackActions.OnClickInPoint = OnClickInPoint;
        CallbackActions.OnClickRemoveConnection = OnClickRemoveConnection;
        CallbackActions.OnClickRemoveNode = OnClickRemoveNode;
        CallbackActions.OnClickRemoveSubConnection = OnClickRemoveSubConnection;

        selectedInPoint = null;
        selectedOutPoint = null;

        string dir = Application.dataPath + "/Dialog/" + SceneManager.GetActiveScene().name;

        List<SerialNode> data = IONodes.load(dir + "/" + IONodes.directoryStructureBuilder(_sequenceContainer.transform) + ".dialog");

        foreach (SerialNode node in data)
        {
            EditorNode eNode = OnClickAddNode(new Vector2(node.positionX, node.positionY), (NodeType)Enum.Parse(typeof(NodeType), node.type), node.id);
            eNode.Weighting = node.weight;
            eNode.UseWeighting = node.useWeighting;
            eNode.MaxWeighting = node.maxWeight;
//            eNode.Cancellable = node.cancellable;
        }

        int refID = 0;
        foreach (SerialNode node in data)
        {
            switch ((NodeType)Enum.Parse(typeof(NodeType), node.type))
            {
                case NodeType.ACTION:
                    SerialNodeAction serialNodeAction = (SerialNodeAction)node;

                    EditorNodeAction nodeAction = (EditorNodeAction)nodes[refID];

                    foreach (SerialNodeActionComponent serialNodeActionComponent in serialNodeAction.components)
                    {
                        NodeActionComponent component = null;
                        EditorNodeActionComponent editorNodeActionComponent = null;
                        switch ((NodeActionComponentType)Enum.Parse(typeof(NodeActionComponentType), serialNodeActionComponent.type))
                        {
                            case NodeActionComponentType.CUSTOM:
                                editorNodeActionComponent = new EditorNodeActionComponentCustom(
                                    nodeAction,
                                    component = new NodeActionComponentCustom(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        WatchObject = getComponentWithInterface<NodeActionComponentCustomExecutor>(getObjectFromScene(((SerialNodeActionComponentCustom)serialNodeActionComponent).objectLocalIdInFile))
                                    });
                                break;
                            case NodeActionComponentType.PARTICLE_SYSTEM:
                                editorNodeActionComponent = new EditorNodeActionComponentParticleSystem(
                                    nodeAction,
                                    component = new NodeActionComponentParticleSystem(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        ParticleSystem = getObjectComponentFromScene<ParticleSystem>(((SerialNodeActionComponentParticleSystem)serialNodeActionComponent).objectLocalIdInFile)
                                    });
                                break;
                            case NodeActionComponentType.SOUND:
                                editorNodeActionComponent = new EditorNodeActionComponentSound(
                                    nodeAction,
                                    component = new NodeActionComponentSound(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        SoundPlayer = getObjectComponentFromScene<SoundPlayer>(((SerialNodeActionComponentSound)serialNodeActionComponent).objectLocalIdInFile)
                                    });
                                break;
                            case NodeActionComponentType.TELEPORT:
                                editorNodeActionComponent = new EditorNodeActionComponentTeleport(
                                    nodeAction,
                                    component = new NodeActionComponentTeleport(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        GameObject = getObjectComponentFromScene<Transform>(((SerialNodeActionComponentTeleport)serialNodeActionComponent).gameObject),
                                        MoveToLocation = getObjectComponentFromScene<Transform>(((SerialNodeActionComponentTeleport)serialNodeActionComponent).moveToLocation)
                                    });
                                break;
                            case NodeActionComponentType.NPC_RUN_AWAY:
                                editorNodeActionComponent = new EditorNodeActionComponentNPCRunAway(
                                    nodeAction,
                                    component = new NodeActionComponentNPCRunAway(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        Npc = getObjectComponentFromScene<NPC>(((SerialNodeActionComponentNPCRunAway)serialNodeActionComponent).npc),
                                        Location = getObjectComponentFromScene<Transform>(((SerialNodeActionComponentNPCRunAway)serialNodeActionComponent).moveToLocation)
                                    });
                                break;
                            case NodeActionComponentType.REPLACE:
                                editorNodeActionComponent = new EditorNodeActionComponentReplace(
                                    nodeAction,
                                    component = new NodeActionComponentReplace(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        ToBeReplace = getObjectFromScene(((SerialNodeActionComponentReplace)serialNodeActionComponent).replaceThis),
                                        Replacer = getObjectFromScene(((SerialNodeActionComponentReplace)serialNodeActionComponent).replaceWith)
                                    });
                                break;
                            case NodeActionComponentType.COMPLEX_ENEMY_FIGHT:
                                editorNodeActionComponent = new EditorNodeActionComponentComplexEnemyFight(
                                    nodeAction,
                                    component = new NodeActionComponentComplexEnemyFight(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        ComplexEnemies = ((SerialNodeActionComponentComplexEnemyFight)serialNodeActionComponent).objectNames.Select(
                                            obj => obj.Equals("")
                                                    ? null
                                                    : getObjectComponentFromScene<ComplexEnemy>(obj)
                                            ).ToList()
                                    });
                                break;
                            case NodeActionComponentType.WOLVES_GOES_HOSTILE:
                                editorNodeActionComponent = new EditorNodeActionComponentComplexEnemyWolvesGoHostile(
                                    nodeAction,
                                    component = new NodeActionComponentComplexEnemyWolvesGoHostile(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        Humans = ((SerialNodeActionComponentComplexEnemyWolvesGoHostile)serialNodeActionComponent).humanObjectNames.Select(
                                            obj => obj.Equals("")
                                                ? null
                                                : getObjectComponentFromScene<ComplexEnemy>(obj)
                                        ).ToList(),
                                        Wolves = ((SerialNodeActionComponentComplexEnemyWolvesGoHostile)serialNodeActionComponent).wolfObjectNames.Select(
                                            obj => obj.Equals("")
                                                ? null
                                                : getObjectComponentFromScene<ComplexEnemy>(obj)
                                        ).ToList()
                                    });
                                break;
                            case NodeActionComponentType.HUMAN_GOES_HOSTILE:
                                editorNodeActionComponent = new EditorNodeActionComponentComplexEnemyWolvesGoHuman(
                                    nodeAction,
                                    component = new NodeActionComponentComplexEnemyWolvesGoHuman(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        Humans = ((SerialNodeActionComponentComplexEnemyWolvesGoHuman)serialNodeActionComponent).humanObjectNames.Select(
                                            obj => obj.Equals("")
                                                ? null
                                                : getObjectComponentFromScene<ComplexEnemy>(obj)
                                        ).ToList(),
                                        Wolves = ((SerialNodeActionComponentComplexEnemyWolvesGoHuman)serialNodeActionComponent).wolfObjectNames.Select(
                                            obj => obj.Equals("")
                                                ? null
                                                : getObjectComponentFromScene<ComplexEnemy>(obj)
                                        ).ToList()
                                    });
                                break;
                            case NodeActionComponentType.PLAYER_INPUT:
                                editorNodeActionComponent = new EditorNodeActionComponentPlayerInput(
                                    nodeAction,
                                    component = new NodeActionComponentPlayerInput(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        AllowInput = ((SerialNodeActionComponentPlayerInput)serialNodeActionComponent).allowInput
                                    });
                                break;
                            case NodeActionComponentType.FADE:
                                editorNodeActionComponent = new EditorNodeActionComponentFade(
                                    nodeAction,
                                    component = new NodeActionComponentFade(nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        ToBlack = ((SerialNodeActionComponentFade)serialNodeActionComponent).toBlack,
                                    });
                                break;
                            case NodeActionComponentType.ANIMATIONS:
                                editorNodeActionComponent = new EditorNodeActionComponentTriggerAnimations(
                                    nodeAction,
                                    component = new NodeActionComponentTriggerAnimations(
                                        nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        Gestures = getObjectComponentFromScene<NPCGestures>(((SerialNodeActionComponentTriggerAnimations)serialNodeActionComponent).objectID),
                                        GestureType = (NPCGestures.GestureType) Enum.Parse(typeof(NPCGestures.GestureType), ((SerialNodeActionComponentTriggerAnimations)serialNodeActionComponent).gestureControl)
                                    });
                                break;
                            case NodeActionComponentType.CHANGE_AMBIENT_SOUND:
                                editorNodeActionComponent = new EditorNodeActionComponentChangeAmbientSound(
                                    nodeAction,
                                    component = new NodeActionComponentChangeAmbientSound(
                                        nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        MusicState = (MusicManager.MusicState) Enum.Parse(typeof(MusicManager.MusicState), ((SerialNodeActionComponentChangeAmbientSound) serialNodeActionComponent).sound)
                                    });
                                break;
                            case NodeActionComponentType.INVERSE_ACTIVE:
                                editorNodeActionComponent = new EditorNodeActionComponentInverseActive(
                                    nodeAction,
                                    component = new NodeActionComponentInverseActive(
                                        nodeAction.getNode<NodeAction>(), serialNodeActionComponent.id)
                                    {
                                        Target = getObjectFromScene(((SerialNodeActionComponentInverseActive)serialNodeActionComponent).target)
                                    });
                                break;
                        }

                        nodeAction.addActionComponent(editorNodeActionComponent);
                        nodeAction.getNode<NodeAction>().addComponent(component);

                    }
                    break;
                case NodeType.LISTENER:
                    SerialNodeListener serialNodeListener = (SerialNodeListener)node;

                    EditorNodeListener nodeListener = (EditorNodeListener)nodes[refID];

                    foreach (SerialNodeListenerComponent serialNodeListenerComponent in serialNodeListener.components)
                    {
                        SequenceConnectionPoint point = null;
                        NodeListenerComponent component = null;
                        EditorNodeListenerComponent editorNodeListenerComponent = null;
                        switch ((NodeListenerComponentType)Enum.Parse(typeof(NodeListenerComponentType), serialNodeListenerComponent.type))
                        {
                            case NodeListenerComponentType.PLAYER_HIT_NPC:
                                editorNodeListenerComponent = new EditorNodeListenerComponentPlayerHitNPC(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentPlayerHitNPC(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchNpc = getObjectComponentFromScene<NPC>(((SerialNodeListenerComponentPlayerHitNPC)serialNodeListenerComponent).objectLocalIdInFile),
                                        RequireHitCountToPass = ((SerialNodeListenerComponentPlayerHitNPC)serialNodeListenerComponent).requirePass
                                    });
                                break;
                            case NodeListenerComponentType.RADIUS_WITHIN_OBJECT:
                                editorNodeListenerComponent = new EditorNodeListenerComponentPlayerWithinRadius(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentPlayerWithinRadius(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchObject = getObjectComponentFromScene<Transform>(((SerialNodeListenerComponentPlayerWithinRadius)serialNodeListenerComponent).objectLocalIdInFile),
                                        Radius = ((SerialNodeListenerComponentPlayerWithinRadius)serialNodeListenerComponent).radius
                                    });
                                break;
                            case NodeListenerComponentType.RADIUS_OUTSIDE_OBJECT:
                                editorNodeListenerComponent = new EditorNodeListenerComponentPlayerOutsideRadius(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentPlayerOutsideOfRadius(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchObject = getObjectComponentFromScene<Transform>(((SerialNodeListenerComponentPlayerOutsideRadius)serialNodeListenerComponent).objectLocalIdInFile),
                                        Radius = ((SerialNodeListenerComponentPlayerOutsideRadius)serialNodeListenerComponent).radius
                                    });
                                break;
                            case NodeListenerComponentType.NPC_HIT_NPC:
                                editorNodeListenerComponent = new EditorNodeListenerComponentNPCHitNPC(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentNPCHitNPC(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        NpcAttacker = getObjectComponentFromScene<NPC>(((SerialNodeListenerComponentNPCHitNPC)serialNodeListenerComponent).attacker),
                                        NpcTarget = getObjectComponentFromScene<NPC>(((SerialNodeListenerComponentNPCHitNPC)serialNodeListenerComponent).target),
                                        RequireHitCountToPass = ((SerialNodeListenerComponentNPCHitNPC)serialNodeListenerComponent).requirePass,
                                    });
                                break;
                            case NodeListenerComponentType.NPC_HIT_PLAYER:
                                editorNodeListenerComponent = new EditorNodeListenerComponentNPCHitPlayer(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentNPCHitPlayer(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchNpc = getObjectComponentFromScene<NPC>(((SerialNodeListenerComponentNPCHitPlayer)serialNodeListenerComponent).objectLocalIdInFile),
                                        RequireHitCountToPass = ((SerialNodeListenerComponentNPCHitPlayer)serialNodeListenerComponent).requirePass,
                                    });
                                break;
                            case NodeListenerComponentType.NPC_TARGETS_PLAYER:
                                editorNodeListenerComponent = new EditorNodeListenerComponentNPCTargetsPlayer(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentNPCTargetsPlayer(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchNpc = getObjectComponentFromScene<NPC>(((SerialNodeListenerComponentNPCTargetsPlayer)serialNodeListenerComponent).objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_HIT_ENEMY:
                                editorNodeListenerComponent = new EditorNodeListenerComponentPlayerHitEnemy(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentPlayerHitEnemy(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchEnemy = getObjectComponentFromScene<ComplexEnemy>(((SerialNodeListenerComponentPlayerHitEnemy)serialNodeListenerComponent).objectLocalIdInFile),
                                        RequireHitCountToPass = ((SerialNodeListenerComponentPlayerHitEnemy)serialNodeListenerComponent).requirePass,
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_HIT_OBJECT:
                                editorNodeListenerComponent = new EditorNodeListenerComponentPlayerHitObject(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentPlayerHitObject(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchObject = getObjectFromScene(((SerialNodeListenerComponentPlayerHitObject)serialNodeListenerComponent).objectLocalIdInFile),
                                        RequireHitCountToPass = ((SerialNodeListenerComponentPlayerHitObject)serialNodeListenerComponent).requirePass,
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_KILLED_ENEMY:
                                editorNodeListenerComponent = new EditorNodeListenerComponentPlayerKilledEnemy(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentPlayerKilledEnemy(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchEnemy = getObjectComponentFromScene<ComplexEnemy>(((SerialNodeListenerComponentPlayerKillEnemy)serialNodeListenerComponent).objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_KILLED_NPC:
                                editorNodeListenerComponent = new EditorNodeListenerComponentPlayerKilledNPC(
                                    nodeListener,
                                    point = new SequenceConnectionPoint(nodeListener, SequenceConnectionPointType.Out, Styles.outPoint, CallbackActions.OnClickOutPoint, false),
                                    component = new NodeListenerComponentPlayerKilledNPC(nodeListener.getNode<NodeListener>(), serialNodeListenerComponent.id, serialNodeListenerComponent.moralityChange)
                                    {
                                        WatchNpc = getObjectComponentFromScene<NPC>(((SerialNodeListenerComponentPlayerKillNPC)serialNodeListenerComponent).objectLocalIdInFile)
                                    });
                                break;
                        }

                        nodeListener.addListenerComponent(editorNodeListenerComponent);
                        nodeListener.OutPoints.Add(point);
                        nodeListener.getNode<NodeListener>().addComponent(component);

                    }

                    int componentID = 0;
                    foreach (SerialNodeListenerComponent serialListenerComponent in serialNodeListener.components)
                    {
                        selectedOutPoint = nodes[refID].OutPoints[componentID];
                        foreach (int link in serialListenerComponent.outLinkIDs)
                        {
                            foreach (EditorNode inNodes in nodes)
                            {
                                if (inNodes.Id == link)
                                {
                                    selectedInPoint = inNodes.InPoint;
                                    CreateConnection();
                                    break;
                                }
                            }
                        }

                        foreach (int link in serialListenerComponent.cancelLinkIDs)
                        {
                            foreach (EditorNode inNodes in nodes)
                            {
                                if (inNodes.Id == link)
                                {
                                    selectedInPoint = inNodes.CancelPoint;
                                    CreateConnection();
                                    break;
                                }
                            }
                        }

                        componentID++;
                    }

                    break;
                case NodeType.GROUP_TRIGGER:
                    selectedOutPoint = nodes[refID].OutPoints[0];
                    selectedOutPoint.editorNode.getNode<NodeGroupTrigger>().UseWeighting = false;

                    foreach (int link in node.outLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.InPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    foreach (int link in node.cancelLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.CancelPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }
                    break;
                case NodeType.RANDOM:
                    selectedOutPoint = nodes[refID].OutPoints[0];
                    selectedOutPoint.editorNode.getNode<NodeRandom>().UseWeighting = false;

                    foreach (int link in node.outLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.InPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    foreach (int link in node.cancelLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.CancelPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }
                    break;
                case NodeType.MORALITY_CHANGE:
                    SerialNodeMoralityChange serialNodeMoralityChange = (SerialNodeMoralityChange) node;

                    nodes[refID].getNode<NodeMoralityChange>().Change = serialNodeMoralityChange.amount;

                    selectedOutPoint = nodes[refID].OutPoints[0];

                    foreach (int link in node.outLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.InPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    foreach (int link in node.cancelLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.CancelPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    break;
                case NodeType.EXIT:
                    SerialNodeExit serialNodeExit = (SerialNodeExit) node;
                    nodes[refID].getNode<NodeExit>().DisableCollider = serialNodeExit.disableCollider;
                    break;
                case NodeType.TIMER:
                    SerialNodeTimer serialNodeTimer = (SerialNodeTimer)node;

                    nodes[refID].getNode<NodeTimer>().TotalWaitTime = serialNodeTimer.maxWaitTime;

                    selectedOutPoint = nodes[refID].OutPoints[0];

                    foreach (int link in node.outLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.InPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    foreach (int link in node.cancelLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.CancelPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    break;
                case NodeType.ENTRY:
                case NodeType.WAIT:
                    selectedOutPoint = nodes[refID].OutPoints[0];

                    foreach (int link in node.outLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.InPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    foreach (int link in node.cancelLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.CancelPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }
                    break;
                case NodeType.MESSAGE:
                    SerialNodeMessage serialNodeMessage = (SerialNodeMessage)node;

                    ((EditorNodeMessage)nodes[refID]).Text = serialNodeMessage.dialog;
                    ((EditorNodeMessage)nodes[refID]).getNode<NodeMessage>().NpcReference =
                        getObjectComponentFromScene<NPC>(serialNodeMessage.objectLocalIdInFile);
                    ((EditorNodeMessage)nodes[refID]).getNode<NodeMessage>().ComplexEnemyReference =
                        getObjectComponentFromScene<ComplexEnemy>(serialNodeMessage.complexEnemyLocalID);
                    //((EditorNodeMessage) nodes[refID]).Color = convertFloatToColor(serialNodeMessage.color);
                    ((EditorNodeMessage)nodes[refID]).getNode<NodeMessage>().KillMessgaeOnDeath = serialNodeMessage.killMessageOnDeath;
                    ((EditorNodeMessage) nodes[refID]).getNode<NodeMessage>().WaitTime = serialNodeMessage.waitTime;
                    selectedOutPoint = nodes[refID].OutPoints[0];

                    foreach (int link in serialNodeMessage.outLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.InPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    foreach (int link in serialNodeMessage.cancelLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.CancelPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    break;
                case NodeType.TUTORIAL_MESSAGE:
                    SerialNodeTutorialMessage serialNodeTutorialMessage = (SerialNodeTutorialMessage)node;

                    ((EditorNodeTutorialMessage)nodes[refID]).Text = serialNodeTutorialMessage.message;
                    ((EditorNodeTutorialMessage)nodes[refID]).getNode<NodeTutorialMessage>().WaitTime = serialNodeTutorialMessage.waitTime;
                    selectedOutPoint = nodes[refID].OutPoints[0];

                    foreach (int link in serialNodeTutorialMessage.outLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.InPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    foreach (int link in serialNodeTutorialMessage.cancelLinkIDs)
                    {
                        foreach (EditorNode outNode in nodes)
                        {
                            if (outNode.Id == link)
                            {
                                selectedInPoint = outNode.CancelPoint;
                                CreateConnection();
                                break;
                            }
                        }
                    }

                    break;
                case NodeType.SELECTION:
                    SerialNodeSelection serialNodeSelection = (SerialNodeSelection)node;

                    EditorNodeSelection nodeSelection = (EditorNodeSelection)nodes[refID];

                    foreach (SerialSelection serialSelection in serialNodeSelection.selections)
                    {
                        SequenceConnectionPoint point;
                        NodeSelectionComponent component;
                        nodeSelection.addNewMessageComponent(
                            new EditorNodeSelectionComponent(
                                point = new SequenceConnectionPoint(nodeSelection, SequenceConnectionPointType.Out, Styles.outPoint, OnClickOutPoint, false),
                                component = new NodeSelectionComponent(serialSelection.dialog, serialSelection.weight, serialSelection.id)));
                        nodeSelection.OutPoints.Add(point);   
                        nodeSelection.getNode<NodeSelection>().addComponent(component);
                    }

                    int selectionID = 0;
                    foreach (SerialSelection serialSelection in serialNodeSelection.selections)
                    {
                        selectedOutPoint = nodes[refID].OutPoints[selectionID];
                        foreach (int link in serialSelection.outLinkIDs)
                        {
                            foreach (EditorNode inNodes in nodes)
                            {
                                if (inNodes.Id == link)
                                {
                                    selectedInPoint = inNodes.InPoint;
                                    CreateConnection();
                                    break;
                                }
                            }
                        }

                        foreach (int link in serialSelection.cancelLinkIDs)
                        {
                            foreach (EditorNode inNodes in nodes)
                            {
                                if (inNodes.Id == link)
                                {
                                    selectedInPoint = inNodes.CancelPoint;
                                    CreateConnection();
                                    break;
                                }
                            }
                        }

                        selectionID++;
                    }

                    break;
            }

            refID++;
        }

        selectedInPoint = null;
        selectedOutPoint = null;

        changeOverToGameNodes();
    }

    private GameObject getObjectFromScene(string file)
    {
        GameObject obj = GameObject.Find(file);

        if (obj != null)
            return obj;

        return null;
    }

    private T getObjectComponentFromScene<T>(string file) where T: UnityEngine.Object
    {
        GameObject obj = GameObject.Find(file);

        if(obj != null)
            return obj.GetComponent<T>();

        return null;
    }

    private Component getComponentWithInterface<T>(GameObject obj)
    {
        if (obj == null)
            return null;

        foreach (Component component in obj.GetComponents<Component>())
        {
            if (component.GetType().GetInterfaces().Contains(typeof(T)))
                return component;
        }

        return null;
    }

    private void changeOverToGameNodes()
    {
        //Change over to real time system if needed
        if (ActiveSequenceContainer.gameUsingContainer.Contains(ActiveSequenceContainer.editorUsingContainer))
        {
            SequenceContainer gameContainer =
                ActiveSequenceContainer.gameUsingContainer.Find(x => x.Equals(ActiveSequenceContainer.editorUsingContainer));
            usingGame = true;
            foreach (SequenceNode node in ActiveSequenceContainer.gameUsingNodes[gameContainer])
            {
                foreach (EditorNode eNode in nodes)
                {
                    if (eNode.getNode<SequenceNode>().Id == node.Id)
                    {
                        eNode.setNode(node);
                        break;
                    }
                }
            }

            ActiveSequenceContainer.editorUsingNodes.Clear();
            ActiveSequenceContainer.editorUsingNodes.AddRange(ActiveSequenceContainer.gameUsingNodes[gameContainer]);
        }
    }

    private float[] convertColorToFloat(Color color)
    {
        return new[] { color.r, color.g, color.b, color.a };
    }

    private Color convertFloatToColor(float[] color)
    {
        return new Color
        {
            r = color[0],
            g = color[1],
            b = color[2],
            a = color[3]
        };
    }

    private void OnLostFocus()
    {
        if (EditorApplication.isPlaying)
        {
            Debug.LogWarning("Cancelling Save due to in-game");
            return;
        }

            usingGame = false;
        if (_sequenceContainer == null)
        {
            nodes.Clear();
            connections.Clear();
            ActiveSequenceContainer.editorUsingNodes.Clear();
            Debug.LogError("Cancelling Save due to no Sequence Container");
            return;
        }

        List<SerialNode> serialNodes = new List<SerialNode>();

        foreach (EditorNode node in nodes)
        {
            switch (node.Type)
            {
                case NodeType.GROUP_TRIGGER:
                    SerialNodeGroupTrigger serialNodeGroupTrigger = new SerialNodeGroupTrigger
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeGroupTrigger.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeGroupTrigger.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeGroupTrigger);
                    break;
                case NodeType.RANDOM:
                    SerialNodeRandom serialNodeRandom = new SerialNodeRandom()
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeRandom.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeRandom.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeRandom);
                    break;
                case NodeType.MORALITY_CHANGE:
                    SerialNodeMoralityChange serialNodeMoralityChange = new SerialNodeMoralityChange
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        amount = ((EditorNodeMoralityChange)node).Amount,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeMoralityChange.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeMoralityChange.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeMoralityChange);
                    break;
                case NodeType.ACTION:
                    SerialNodeAction serialNodeAction = new SerialNodeAction
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    EditorNodeAction nodeAction = (EditorNodeAction)node;

                    foreach (EditorNodeActionComponent component in nodeAction.Components)
                    {
                        SerialNodeActionComponent serialNodeActionComponent;
                        switch (component.Component.ComponentType)
                        {
                            case NodeActionComponentType.CUSTOM:
                                serialNodeActionComponent = new SerialNodeActionComponentCustom
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeActionComponentCustom)component).UnityComponent != null ? ((EditorNodeActionComponentCustom)component).UnityComponent.name : ""
                                };
                                break;
                            case NodeActionComponentType.PARTICLE_SYSTEM:
                                serialNodeActionComponent = new SerialNodeActionComponentParticleSystem
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeActionComponentParticleSystem)component).ParticleSystem != null ? ((EditorNodeActionComponentParticleSystem)component).ParticleSystem.name : ""
                                };
                                break;
                            case NodeActionComponentType.SOUND:
                                serialNodeActionComponent = new SerialNodeActionComponentSound
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeActionComponentSound)component).SoundPlayer != null ? ((EditorNodeActionComponentSound)component).SoundPlayer.name : ""
                                };
                                break;
                            case NodeActionComponentType.TELEPORT:
                                serialNodeActionComponent = new SerialNodeActionComponentTeleport
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    gameObject = ((EditorNodeActionComponentTeleport)component).GameObject != null ? ((EditorNodeActionComponentTeleport)component).GameObject.name : "",
                                    moveToLocation = ((EditorNodeActionComponentTeleport)component).MoveToLocation != null ? ((EditorNodeActionComponentTeleport)component).MoveToLocation.name : ""
                                };
                                break;
                            case NodeActionComponentType.NPC_RUN_AWAY:
                                serialNodeActionComponent = new SerialNodeActionComponentNPCRunAway
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    npc = ((EditorNodeActionComponentNPCRunAway)component).NPC != null ? ((EditorNodeActionComponentNPCRunAway)component).NPC.name : "",
                                    moveToLocation = ((EditorNodeActionComponentNPCRunAway)component).MoveToLocation != null ? ((EditorNodeActionComponentNPCRunAway)component).MoveToLocation.name : ""
                                };
                                break;
                            case NodeActionComponentType.REPLACE:
                                serialNodeActionComponent = new SerialNodeActionComponentReplace
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    replaceThis = ((EditorNodeActionComponentReplace)component).ToBeReplace != null ? ((EditorNodeActionComponentReplace)component).ToBeReplace.name : "",
                                    replaceWith = ((EditorNodeActionComponentReplace)component).Replacer != null ? ((EditorNodeActionComponentReplace)component).Replacer.name : ""
                                };
                                break;
                            case NodeActionComponentType.COMPLEX_ENEMY_FIGHT:
                                serialNodeActionComponent = new SerialNodeActionComponentComplexEnemyFight
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    objectNames = ((EditorNodeActionComponentComplexEnemyFight)component).ComplexEnemies.Select(complexEnemy => complexEnemy != null ? complexEnemy.name : "").ToList()
                                };
                                break;
                            case NodeActionComponentType.WOLVES_GOES_HOSTILE:
                                serialNodeActionComponent = new SerialNodeActionComponentComplexEnemyWolvesGoHostile
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    humanObjectNames = ((EditorNodeActionComponentComplexEnemyWolvesGoHostile)component).Humans.Select(complexEnemy => complexEnemy != null ? complexEnemy.name : "").ToList(),
                                    wolfObjectNames = ((EditorNodeActionComponentComplexEnemyWolvesGoHostile)component).Wolves.Select(complexEnemy => complexEnemy != null ? complexEnemy.name : "").ToList()
                                };
                                break;
                            case NodeActionComponentType.HUMAN_GOES_HOSTILE:
                                serialNodeActionComponent = new SerialNodeActionComponentComplexEnemyWolvesGoHuman
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    humanObjectNames = ((EditorNodeActionComponentComplexEnemyWolvesGoHuman)component).Humans.Select(complexEnemy => complexEnemy != null ? complexEnemy.name : "").ToList(),
                                    wolfObjectNames = ((EditorNodeActionComponentComplexEnemyWolvesGoHuman)component).Wolves.Select(complexEnemy => complexEnemy != null ? complexEnemy.name : "").ToList()
                                };
                                break;
                            case NodeActionComponentType.PLAYER_INPUT:
                                serialNodeActionComponent = new SerialNodeActionComponentPlayerInput
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    allowInput = ((EditorNodeActionComponentPlayerInput)component).AllowInput
                                };
                                break;
                            case NodeActionComponentType.FADE:
                                serialNodeActionComponent = new SerialNodeActionComponentFade()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    toBlack = ((EditorNodeActionComponentFade) component).ToBlack
                                };
                                break;
                            case NodeActionComponentType.ANIMATIONS:
                                serialNodeActionComponent = new SerialNodeActionComponentTriggerAnimations()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    objectID = ((EditorNodeActionComponentTriggerAnimations)component).Gestures != null ? ((EditorNodeActionComponentTriggerAnimations)component).Gestures.name : "",
                                    gestureControl = ((EditorNodeActionComponentTriggerAnimations) component).GestureType.ToString()
                                };
                                break;
                            case NodeActionComponentType.CHANGE_AMBIENT_SOUND:
                                serialNodeActionComponent = new SerialNodeActionComponentChangeAmbientSound
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    sound = ((EditorNodeActionComponentChangeAmbientSound) component).MusicState
                                        .ToString()
                                };
                                break;
                            case NodeActionComponentType.INVERSE_ACTIVE:
                                serialNodeActionComponent = new SerialNodeActionComponentInverseActive
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    id = component.Component.Id,
                                    target = ((EditorNodeActionComponentInverseActive) component).Target != null ? ((EditorNodeActionComponentInverseActive)component).Target.name : "",
                                };
                                break;
                            default:
                                serialNodeActionComponent = null;
                                break;
                        }

                        serialNodeAction.components.Add(serialNodeActionComponent);
                    }

                    serialNodes.Add(serialNodeAction);
                    break;
                case NodeType.LISTENER:
                    SerialNodeListener serialNodeListener = new SerialNodeListener
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    EditorNodeListener nodeListener = (EditorNodeListener)node;

                    foreach (EditorNodeListenerComponent component in nodeListener.Components)
                    {
                        SerialNodeListenerComponent serialNodeListenerComponent;
                        switch (component.Component.ComponentType)
                        {
                            case NodeListenerComponentType.PLAYER_HIT_NPC:
                                serialNodeListenerComponent = new SerialNodeListenerComponentPlayerHitNPC
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentPlayerHitNPC)component).NPCReference != null ? ((EditorNodeListenerComponentPlayerHitNPC)component).NPCReference.name : "",
                                    requirePass = ((EditorNodeListenerComponentPlayerHitNPC)component).RequirePass
                                };
                                break;
                            case NodeListenerComponentType.RADIUS_WITHIN_OBJECT:
                                serialNodeListenerComponent = new SerialNodeListenerComponentPlayerWithinRadius
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentPlayerWithinRadius)component).WatchObject != null ? ((EditorNodeListenerComponentPlayerWithinRadius)component).WatchObject.name : "",
                                    radius = ((EditorNodeListenerComponentPlayerWithinRadius)component).Radius
                                };
                                break;
                            case NodeListenerComponentType.RADIUS_OUTSIDE_OBJECT:
                                serialNodeListenerComponent = new SerialNodeListenerComponentPlayerOutsideRadius
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentPlayerOutsideRadius)component).WatchObject != null ? ((EditorNodeListenerComponentPlayerOutsideRadius)component).WatchObject.name : "",
                                    radius = ((EditorNodeListenerComponentPlayerOutsideRadius)component).Radius
                                };
                                break;
                            case NodeListenerComponentType.PLAYER_HIT_ENEMY:
                                serialNodeListenerComponent = new SerialNodeListenerComponentPlayerHitEnemy()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentPlayerHitEnemy)component).NPCReference != null ? ((EditorNodeListenerComponentPlayerHitEnemy)component).NPCReference.name : "",
                                    requirePass = ((EditorNodeListenerComponentPlayerHitEnemy)component).RequirePass
                                };
                                break;
                            case NodeListenerComponentType.PLAYER_HIT_OBJECT:
                                serialNodeListenerComponent = new SerialNodeListenerComponentPlayerHitObject()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentPlayerHitObject)component).NPCReference != null ? ((EditorNodeListenerComponentPlayerHitObject)component).NPCReference.name : "",
                                    requirePass = ((EditorNodeListenerComponentPlayerHitObject)component).RequirePass
                                };
                                break;
                            case NodeListenerComponentType.PLAYER_KILLED_ENEMY:
                                serialNodeListenerComponent = new SerialNodeListenerComponentPlayerKillEnemy()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentPlayerKilledEnemy)component).NPCReference != null ? ((EditorNodeListenerComponentPlayerKilledEnemy)component).NPCReference.name : ""
                                };
                                break;
                            case NodeListenerComponentType.PLAYER_KILLED_NPC:
                                serialNodeListenerComponent = new SerialNodeListenerComponentPlayerKillNPC()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentPlayerKilledNPC)component).NPCReference != null ? ((EditorNodeListenerComponentPlayerKilledNPC)component).NPCReference.name : ""
                                };
                                break;
                            case NodeListenerComponentType.NPC_TARGETS_PLAYER:
                                serialNodeListenerComponent = new SerialNodeListenerComponentNPCTargetsPlayer()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentNPCTargetsPlayer)component).NPCReference != null ? ((EditorNodeListenerComponentNPCTargetsPlayer)component).NPCReference.name : ""
                                };
                                break;
                            case NodeListenerComponentType.NPC_HIT_PLAYER:
                                serialNodeListenerComponent = new SerialNodeListenerComponentNPCHitPlayer()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    objectLocalIdInFile = ((EditorNodeListenerComponentNPCHitPlayer)component).NPCReference != null ? ((EditorNodeListenerComponentNPCHitPlayer)component).NPCReference.name : "",
                                    requirePass = ((EditorNodeListenerComponentNPCHitPlayer)component).RequirePass
                                };
                                break;
                            case NodeListenerComponentType.NPC_HIT_NPC:
                                serialNodeListenerComponent = new SerialNodeListenerComponentNPCHitNPC()
                                {
                                    type = component.Component.ComponentType.ToString(),
                                    moralityChange = component.Component.MoralityChange,
                                    id = component.Component.Id,
                                    attacker = ((EditorNodeListenerComponentNPCHitNPC)component).NPCAttacker != null ? ((EditorNodeListenerComponentNPCHitNPC)component).NPCAttacker.name : "",
                                    target = ((EditorNodeListenerComponentNPCHitNPC)component).NPCTarget != null ? ((EditorNodeListenerComponentNPCHitNPC)component).NPCTarget.name : "",
                                    requirePass = ((EditorNodeListenerComponentNPCHitNPC)component).RequirePass
                                };
                                break;
                            default:
                                serialNodeListenerComponent = null;
                                break;
                        }

                        foreach (SequenceConnection sq in component.OutPoint.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeListenerComponent.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeListenerComponent.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }

                        serialNodeListener.components.Add(serialNodeListenerComponent);
                    }

                    serialNodes.Add(serialNodeListener);
                    break;
                case NodeType.TIMER:
                    SerialNodeTimer serialNodeTimer = new SerialNodeTimer
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWaitTime = node.getNode<NodeTimer>().TotalWaitTime,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeTimer.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeTimer.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeTimer);
                    break;
                case NodeType.WAIT:
                    SerialNodeWait serialNodeWait = new SerialNodeWait
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeWait.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeWait.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeWait);
                    break;
                case NodeType.ENTRY:
                    SerialNodeEntry serialNodeEntry = new SerialNodeEntry
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeEntry.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeEntry.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeEntry);
                    break;
                case NodeType.EXIT:
                    SerialNodeExit serialNodeExit = new SerialNodeExit
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable,
                        disableCollider = node.getNode<NodeExit>().DisableCollider
                    };

                    serialNodes.Add(serialNodeExit);
                    break;
                case NodeType.MESSAGE:
                    SerialNodeMessage serialNodeMessage = new SerialNodeMessage
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        dialog = ((EditorNodeMessage)node).Text,
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        objectLocalIdInFile = node.getNode<NodeMessage>().NpcReference != null ? node.getNode<NodeMessage>().NpcReference.name : "",
                        complexEnemyLocalID = node.getNode<NodeMessage>().ComplexEnemyReference != null ? node.getNode<NodeMessage>().ComplexEnemyReference.name : "",
                        killMessageOnDeath = node.getNode<NodeMessage>().KillMessgaeOnDeath,
//                        color = convertColorToFloat(node.getNode<NodeMessage>().Color),
                        waitTime = node.getNode<NodeMessage>().WaitTime,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeMessage.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeMessage.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeMessage);
                    break;
                case NodeType.TUTORIAL_MESSAGE:
                    SerialNodeTutorialMessage serialNodeTutorialMessage = new SerialNodeTutorialMessage
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        message = ((EditorNodeTutorialMessage)node).Text,
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        waitTime = node.getNode<NodeTutorialMessage>().WaitTime,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    foreach (SequenceConnectionPoint scp in node.OutPoints)
                    {
                        foreach (SequenceConnection sq in scp.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialNodeTutorialMessage.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialNodeTutorialMessage.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }
                    }

                    serialNodes.Add(serialNodeTutorialMessage);
                    break;
                case NodeType.SELECTION:
                    SerialNodeSelection serialNodeSelection = new SerialNodeSelection
                    {
                        id = node.Id,
                        positionX = node.rect.x,
                        positionY = node.rect.y,
                        type = node.Type.ToString(),
                        useWeighting = node.UseWeighting,
                        weight = node.Weighting,
                        maxWeight = node.MaxWeighting,
                        cancellable = node.Cancellable
                    };

                    EditorNodeSelection nodeSelection = (EditorNodeSelection) node;

                    foreach (EditorNodeSelectionComponent component in nodeSelection.messageComponents)
                    {
                        SerialSelection serialSelection = new SerialSelection
                        {
                            dialog = component.Message,
                            weight = component.Weighting,
                            id = component.Component.Id
                        };

                        foreach (SequenceConnection sq in component.OutPoint.getConnections())
                        {
                            if (sq.inPoint.type == SequenceConnectionPointType.Cancel)
                                serialSelection.cancelLinkIDs.Add(sq.inPoint.editorNode.Id);
                            else
                                serialSelection.outLinkIDs.Add(sq.inPoint.editorNode.Id);
                        }

                        serialNodeSelection.selections.Add(serialSelection);
                    }

                    serialNodes.Add(serialNodeSelection);
                    break;
            }
        }

        string json = JsonConvert.SerializeObject(serialNodes, new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects
        });

        string dir = Application.dataPath + "/Dialog/" + SceneManager.GetActiveScene().name;
        Directory.CreateDirectory(dir);

        using (StreamWriter writetext = new StreamWriter(dir + "/" + IONodes.directoryStructureBuilder(_sequenceContainer.transform) + ".dialog"))
        {
            writetext.WriteLine(json);
        }
    }

    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
    {
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing) + 1;
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing) + 1;

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        offset += drag * 0.5f;
        Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        for (int i = 0; i < widthDivs; i++)
        {
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
        }

        for (int j = 0; j < heightDivs; j++)
        {
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    private void DrawNodes()
    {
        if (nodes != null)
        {
            foreach (EditorNode t in nodes)
            {
                t.Draw();
            }
        }
    }

    private void DrawConnections()
    {
        if (connections == null) return;

        //Requires Each. Some weird some going with Unity holding a async event even though it passes code. Foreach throws Collection Modication error when a connection is removed.
        for (int i = 0; i < connections.Count; i++)
        {
            connections[i].Draw();
        }
    }

    private void ProcessEvents(Event e)
    {
        drag = Vector2.zero;

        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 0)
                {
                    ClearConnectionSelection();
                }

                if (e.button == 1)
                {
                    ProcessContextMenu(e.mousePosition);
                }
                break;

            case EventType.MouseDrag:
                if (e.button == 0)
                {
                    OnDrag(e.delta);
                }
                break;
        }
    }

    private void ProcessNodeEvents(Event e)
    {
        if (nodes != null)
        {
            for (int i = nodes.Count - 1; i >= 0; i--)
            {
                bool guiChanged = nodes[i].ProcessEvents(e);

                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }
    }

    private void DrawConnectionLine(Event e)
    {
        if (selectedInPoint != null && selectedOutPoint == null)
        {
            Handles.DrawBezier(
                selectedInPoint.rect.center,
                e.mousePosition,
                selectedInPoint.rect.center + Vector2.left * 50f,
                e.mousePosition - Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        if (selectedOutPoint != null && selectedInPoint == null)
        {
            Handles.DrawBezier(
                selectedOutPoint.rect.center,
                e.mousePosition,
                selectedOutPoint.rect.center - Vector2.left * 50f,
                e.mousePosition + Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }
    }

    private void ProcessContextMenu(Vector2 mousePosition)
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Add NPC Dialogue Node"), false, () => OnClickAddNode(mousePosition, NodeType.MESSAGE, -1));
        genericMenu.AddItem(new GUIContent("Add Selection Node"), false, () => OnClickAddNode(mousePosition, NodeType.SELECTION, -1));
        genericMenu.AddItem(new GUIContent("Add Listener Node"), false, () => OnClickAddNode(mousePosition, NodeType.LISTENER, -1));
        genericMenu.AddItem(new GUIContent("Add Action Node"), false, () => OnClickAddNode(mousePosition, NodeType.ACTION, -1));
        genericMenu.AddItem(new GUIContent("Add Timer Node"), false, () => OnClickAddNode(mousePosition, NodeType.TIMER, -1));
        genericMenu.AddItem(new GUIContent("Add Entry Node"), false, () => OnClickAddNode(mousePosition, NodeType.ENTRY, -1));
        genericMenu.AddItem(new GUIContent("Add Exit Node"), false, () => OnClickAddNode(mousePosition, NodeType.EXIT, -1));
        genericMenu.AddItem(new GUIContent("Add Player Message Node"), false, () => OnClickAddNode(mousePosition, NodeType.TUTORIAL_MESSAGE, -1));
        genericMenu.AddItem(new GUIContent("Add Morality Change Node"), false, () => OnClickAddNode(mousePosition, NodeType.MORALITY_CHANGE, -1));
        genericMenu.AddItem(new GUIContent("Add Group Trigger Node"), false, () => OnClickAddNode(mousePosition, NodeType.GROUP_TRIGGER, -1));
        genericMenu.AddItem(new GUIContent("Add Random Node"), false, () => OnClickAddNode(mousePosition, NodeType.RANDOM, -1));
        genericMenu.ShowAsContext();
    }

    private void OnDrag(Vector2 delta)
    {
        drag = delta;

        if (nodes != null)
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                nodes[i].Drag(delta);
            }
        }

        GUI.changed = true;
    }

    private EditorNode OnClickAddNode(Vector2 mousePosition, NodeType type, int id)
    {
        if (nodes == null)
        {
            nodes = new List<EditorNode>();
        }

        EditorNode node = null;
        SequenceNode sNode = null;
        switch (type)
        {
            case NodeType.TIMER:
                nodes.Add(node = new EditorNodeTimer(mousePosition, 400, 76, sNode = new NodeTimer(id)));
                break;
            case NodeType.ENTRY:
                bool exit = false;
                nodes.ForEach(x =>
                {
                    if (x.Type == NodeType.ENTRY)
                    {
                        exit = true;
                    }
                });
                if (exit)
                    return null;

                nodes.Add(node = new EditorNodeEntry(mousePosition, 400, 55, sNode = new NodeEntry(id)));
                break;
            case NodeType.EXIT:
                nodes.Add(node = new EditorNodeExit(mousePosition, 400, 55, sNode = new NodeExit(id)));
                break;
            case NodeType.SELECTION:
                nodes.Add(node = new EditorNodeSelection(mousePosition, 400, 76, sNode = new NodeSelection(id)));
                break;
            case NodeType.MESSAGE:
                nodes.Add(node = new EditorNodeMessage(mousePosition, 400, 76, sNode = new NodeMessage(id)));
                break;
            case NodeType.WAIT:
                nodes.Add(node = new EditorNodeWait(mousePosition, 400, 76, sNode = new NodeWait(id)));
                break;
            case NodeType.LISTENER:
                nodes.Add(node = new EditorNodeListener(mousePosition, 400, 76, sNode = new NodeListener(id)));
                break;
            case NodeType.MORALITY_CHANGE:
                nodes.Add(node = new EditorNodeMoralityChange(mousePosition, 400, 76, sNode = new NodeMoralityChange(id)));
                break;
            case NodeType.ACTION:
                nodes.Add(node = new EditorNodeAction(mousePosition, 400, 76, sNode = new NodeAction(id)));
                break;
            case NodeType.GROUP_TRIGGER:
                nodes.Add(node = new EditorNodeGroupTrigger(mousePosition, 400, 76, sNode = new NodeGroupTrigger(id)));
                break;
            case NodeType.TUTORIAL_MESSAGE:
                nodes.Add(node = new EditorNodeTutorialMessage(mousePosition, 400, 76, sNode = new NodeTutorialMessage(id)));
                break;
            case NodeType.RANDOM:
                nodes.Add(node = new EditorNodeRandom(mousePosition, 400, 76, sNode = new NodeRandom(id)));
                break;
        }

        ActiveSequenceContainer.editorUsingNodes.Add(sNode);
        SequenceContainer container =
            ActiveSequenceContainer.gameUsingContainer.Find(
                x => x.Equals(ActiveSequenceContainer.editorUsingContainer));
        if (container != null)
            ActiveSequenceContainer.addToGameNodes(sNode, container);

        return node;
    }

    private void OnClickInPoint(SequenceConnectionPoint inPoint)
    {
        selectedInPoint = inPoint;

        if (selectedOutPoint != null)
        {
            if (selectedOutPoint.editorNode != selectedInPoint.editorNode)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickOutPoint(SequenceConnectionPoint outPoint)
    {
        selectedOutPoint = outPoint;

        if (selectedInPoint != null)
        {
            if (selectedOutPoint.editorNode != selectedInPoint.editorNode)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickRemoveNode(EditorNode editorNode)
    {
        if (connections != null)
        {
            List<SequenceConnection> connectionsToRemove = connections.Where(
                t => (editorNode.InPoint != null && editorNode.InPoint.Equals(t.inPoint)) 
                || editorNode.OutPoints.Contains(t.outPoint) || editorNode.CancelPoint.Equals(t.inPoint)
            ).ToList();

            foreach (SequenceConnection t in connectionsToRemove)
            {
                t.inPoint.removeConnection(t);
                t.outPoint.removeConnection(t);
                connections.Remove(t);
            }
        }

        nodes.Remove(editorNode);
    }

    private void OnClickRemoveSubConnection(SequenceConnectionPoint point)
    {
        List<SequenceConnection> connectionsToRemove = new List<SequenceConnection>();

        for (int i = 0; i < connections.Count; i++)
        {
            if (connections[i].inPoint == point || connections[i].outPoint == point)
            {
                connectionsToRemove.Add(connections[i]);
            }
        }

        foreach (SequenceConnection t in connectionsToRemove)
        {
            t.inPoint.removeConnection(t);
            t.outPoint.removeConnection(t);
            connections.Remove(t);

            SequenceNode inNode = t.inPoint.editorNode.getNode<SequenceNode>();
            SequenceNode outNode = t.outPoint.editorNode.getNode<SequenceNode>();

            inNode.removeOutLink(outNode);
            outNode.removeInLink(inNode);
        }
    }

    private void OnClickRemoveConnection(SequenceConnection connection)
    {
        connections.Remove(connection);
        connection.inPoint.removeConnection(connection);
        connection.outPoint.removeConnection(connection);

        SequenceNode inNode = connection.inPoint.editorNode.getNode<SequenceNode>();
        SequenceNode outNode = connection.outPoint.editorNode.getNode<SequenceNode>();

        inNode.removeOutLink(outNode);
        outNode.removeInLink(inNode);
    }

    private void CreateConnection()
    {
        if (connections == null)
        {
            connections = new List<SequenceConnection>();
        }

        SequenceConnection connection;
        connections.Add(connection = new SequenceConnection(selectedInPoint, selectedOutPoint));
        selectedInPoint.addConnection(connection);

        selectedOutPoint.addConnection(connection);

        SequenceNode inPointNode = selectedInPoint.editorNode.getNode<SequenceNode>();
        SequenceNode outPointNode = selectedOutPoint.editorNode.getNode<SequenceNode>();

        if (outPointNode.GetType().IsAssignableFrom(typeof(NodeSelection)))
        {
            foreach (EditorNodeSelectionComponent component in ((EditorNodeSelection)selectedOutPoint.editorNode).messageComponents)
            {
                if (component.Component.Id == outPointNode.Id)
                {
                    component.Component.addNode(inPointNode);
                }
            }
        }
        else if (outPointNode.GetType().IsAssignableFrom(typeof(NodeListener)))
        {
            foreach (EditorNodeListenerComponent component in ((EditorNodeListener)selectedOutPoint.editorNode).Components)
            {
                if (component.Component.Id == outPointNode.Id)
                {
                    component.Component.addNode(inPointNode);
                }
            }
        }
        else
        {
            outPointNode.registerNewInLink(inPointNode);
        }

        if (selectedInPoint.type == SequenceConnectionPointType.Cancel)
            inPointNode.registerNewCancelLink(outPointNode);
        else
            inPointNode.registerNewOutLink(outPointNode);
    }

    private void ClearConnectionSelection()
    {
        selectedInPoint = null;
        selectedOutPoint = null;
    }
}