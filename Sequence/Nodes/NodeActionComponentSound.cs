﻿/**
 * Author: Sam Murphy
 */

public class NodeActionComponentSound : NodeActionComponent
{
    public NodeActionComponentSound(NodeAction action, int id) : base(action, id, NodeActionComponentType.SOUND)
    {
        SoundPlayer = null;
    }

    public SoundPlayer SoundPlayer { get; set; }

    public override void execute()
    {
        SoundPlayer.Play();
    }
}