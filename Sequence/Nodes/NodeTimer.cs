﻿/**
 * Author: Sam Murphy
 */

public class NodeTimer : SequenceNode
{
    public NodeTimer(int id) : base(id, NodeType.TIMER)
    {
        Current = 0.0f;
        UseWeighting = false;
    }

    public float TotalWaitTime { get; set; }

    public float Current { get; private set; }

    public override void execute()
    {
    }

    public override void cleanup()
    {
        Current = 0.0f;
    }

    public override void timer(float timestep)
    {
        if (Active)
        {
            Current += timestep;
            if (Current >= TotalWaitTime)
                move();
        }
    }
}