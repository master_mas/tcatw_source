﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class NodeSelection : SequenceNode
{
    private readonly Dictionary<int, int> mapping = new Dictionary<int, int>();
    private readonly List<NodeSelectionComponent> nodeSelectionComponents = new List<NodeSelectionComponent>();

    public NodeSelection(int id) : base(id, NodeType.SELECTION)
    {
    }

    public override void execute()
    {
        var dialog = new string[nodeSelectionComponents.Count];

        for (var i = 0; i < nodeSelectionComponents.Count; i++)
            dialog[i] = nodeSelectionComponents[i].Message;
//            mapping.Add(i, i);

        PlayerDialog.instance.playDialog(dialog, selection);
    }

    private void selection(int selectionID)
    {
        if (!Active)
            return;

        switch (nodeSelectionComponents.Count)
        {
            case 1:
                if (selectionID != 2)
                {
                    destroy();
                    return;
                }
                else
                {
                    selectionID = 1;
                }
                break;
            case 2:
                if (selectionID == 2)
                {
                    destroy();
                    return;
                }
                else if (selectionID == 3)
                {
                    selectionID = 2;
                }
                break;
        }

        MoralityController.Self.Weight += nodeSelectionComponents[selectionID - 1].Weighting;
        move(nodeSelectionComponents[selectionID - 1].OutLinkNodes);
    }

    public void move(List<SequenceNode> outPoints)
    {
        destroy();

        var activateNodes = new List<SequenceNode>();

        foreach (var node in outPoints)
        {
            if (!node.UseWeighting)
            {
                if (node.Type == NodeType.GROUP_TRIGGER)
                    activateNodes.Insert(0, node);
                else
                    activateNodes.Add(node);
                continue;
            }

            var weight = MoralityController.Self.Weight;

            if (weight < 0 && weight <= node.maxWeighting && node.Weighting <= weight)
                activateNodes.Add(node);
            else if (weight >= 0 && weight >= node.Weighting && weight <= node.maxWeighting)
                activateNodes.Add(node);
        }

        activateNodes.ForEach(x =>
        {
            if (x.isTriggerCancellable(this))
                x.destroy();
            else
                x.init();
        });
    }

    public override void cleanup()
    {
        PlayerDialog.instance.reset();
        mapping.Clear();
    }

    public NodeSelectionComponent getComponentByID(int id)
    {
        foreach (var component in nodeSelectionComponents)
            if (component.Id == id)
                return component;

        return null;
    }

    public void addComponent(NodeSelectionComponent component)
    {
        nodeSelectionComponents.Add(component);
    }

    public void removeComponent(NodeSelectionComponent component)
    {
        nodeSelectionComponents.Remove(component);
    }
}