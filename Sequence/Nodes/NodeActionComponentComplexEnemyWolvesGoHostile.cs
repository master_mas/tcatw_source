﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class NodeActionComponentComplexEnemyWolvesGoHostile : NodeActionComponent
{
    private List<ComplexEnemy> humans = new List<ComplexEnemy>();
    private List<ComplexEnemy> wolves = new List<ComplexEnemy>();

    public NodeActionComponentComplexEnemyWolvesGoHostile(NodeAction action, int id) : base(action, id,
        NodeActionComponentType.WOLVES_GOES_HOSTILE)
    {
    }

    public List<ComplexEnemy> Wolves
    {
        get { return wolves; }
        set { wolves = value; }
    }

    public List<ComplexEnemy> Humans
    {
        get { return humans; }
        set { humans = value; }
    }

    public override void execute()
    {
        wolves.ForEach(complexEnemy =>
        {
            if (complexEnemy != null)
                complexEnemy.SetupPlayerEnemy();
        });

        humans.ForEach(complexEnemy =>
        {
            if (complexEnemy != null)
                complexEnemy.SetupPlayerAlly();
        });
    }
}