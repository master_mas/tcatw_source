﻿/**
 * Author: Sam Murphy
 */

public class NodeActionComponentPlayerInput : NodeActionComponent
{
    public NodeActionComponentPlayerInput(NodeAction action, int id) : base(action, id,
        NodeActionComponentType.PLAYER_INPUT)
    {
        AllowInput = true;
    }

    public bool AllowInput { get; set; }

    public override void execute()
    {
        PlayerController.instance.isInputAllowed = AllowInput;
    }
}