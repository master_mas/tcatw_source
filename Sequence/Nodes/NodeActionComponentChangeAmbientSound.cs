﻿public class NodeActionComponentChangeAmbientSound : NodeActionComponent
{
    public NodeActionComponentChangeAmbientSound(NodeAction action, int id) : base(action, id, NodeActionComponentType.CHANGE_AMBIENT_SOUND)
    {
    }

    public override void execute()
    {
        MusicManager.instance.ChangeMusicTo(MusicState);
    }

    public MusicManager.MusicState MusicState { get; set; }
}