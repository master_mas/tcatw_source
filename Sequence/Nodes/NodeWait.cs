﻿using System;

/**
 * Author: Sam Murphy
 */

public class NodeWait : SequenceNode
{
    public float lastTime = -1;
    public float timer;

    public NodeWait(int id) : base(id, NodeType.WAIT)
    {
    }

    public override void execute()
    {
        lastTime = DateTime.UtcNow.Second;
    }

    public override void cleanup()
    {
        lastTime = -1;
        timer = 0;
    }
}