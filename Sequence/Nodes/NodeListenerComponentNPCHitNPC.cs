﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentNPCHitNPC : NodeListenerComponent
{
    private int currentHits;

    public NodeListenerComponentNPCHitNPC(NodeListener listener, int id, float moralityChange) : base(listener, id,
        moralityChange, NodeListenerComponentType.NPC_HIT_NPC)
    {
    }

    public NPC NpcTarget { get; set; }

    public NPC NpcAttacker { get; set; }

    public int RequireHitCountToPass { get; set; }

    public override void destroy()
    {
        currentHits = 0;
    }

    public override void NPCHitsNPC(GameObject npcTarget, GameObject npcAttacker)
    {
        if (NpcTarget != null && NpcAttacker != null && npcTarget.Equals(NpcTarget.gameObject) &&
            npcAttacker.Equals(NpcAttacker.gameObject))
        {
            currentHits++;
            if (currentHits >= RequireHitCountToPass)
                move();
        }
    }
}