﻿/**
 * Author: Sam Murphy
 */

public class NodeGroupTrigger : SequenceNode
{
    public NodeGroupTrigger(int id) : base(id, NodeType.GROUP_TRIGGER)
    {
        Cancellable = false;
        UseWeighting = false;
    }

    public override void execute()
    {
        move();
    }

    public override void cleanup()
    {
    }
}