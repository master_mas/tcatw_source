﻿/**
 * Author: Sam Murphy
 */

public class NodeMoralityChange : SequenceNode
{
    public NodeMoralityChange(int id) : base(id, NodeType.MORALITY_CHANGE)
    {
    }

    public float Change { get; set; }

    public override void execute()
    {
        MoralityController.Self.Weight += Change;
        move();
    }

    public override void cleanup()
    {
    }
}