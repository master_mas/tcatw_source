﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeExit : SequenceNode
{
    public NodeExit(int id) : base(id, NodeType.EXIT)
    {
        DisableCollider = true;
        UseWeighting = false;
        Cancellable = false;
    }

    public bool DisableCollider { get; set; }

    public override void cleanup()
    {
    }

    public override void execute()
    {
        ActiveSequenceContainer.stopSequence(sequenceContainer);
        if (DisableCollider)
            sequenceContainer.GetComponent<Collider>().enabled = false;
    }
}