﻿using System.Linq;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeActionComponentCustom : NodeActionComponent
{
    public NodeActionComponentCustom(NodeAction action, int id) : base(action, id, NodeActionComponentType.CUSTOM)
    {
        WatchObject = null;
    }

    public Component WatchObject { get; set; }

    public override void execute()
    {
        if (WatchObject != null)
            if (WatchObject.GetType().GetInterfaces().Contains(typeof(NodeActionComponentCustomExecutor)))
                ((NodeActionComponentCustomExecutor) WatchObject).execute(false);
    }
}