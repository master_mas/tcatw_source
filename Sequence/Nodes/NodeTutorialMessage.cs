﻿using System;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class NodeTutorialMessage : SequenceNode
{
    private float waitTime;

    public NodeTutorialMessage(int id) : base(id, NodeType.TUTORIAL_MESSAGE)
    {
        Text = "";
    }

    public float WaitTime
    {
        get { return waitTime; }
        set { waitTime = value; }
    }

    public string Text { get; set; }

    public override void execute()
    {
        PlayerDialog.instance.displayStageMessage(Text, waitTime, move);
    }

    public override void cleanup()
    {
        PlayerDialog.instance.clearStageMessage();
    }
}