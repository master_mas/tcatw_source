﻿using System.Collections.Generic;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListener : SequenceNode
{
    private readonly List<NodeListenerComponent> components = new List<NodeListenerComponent>();

    public NodeListener(int id) : base(id, NodeType.LISTENER)
    {
        SelectionIndex = 0;
    }

    public int SelectionIndex { get; set; }

    public override void execute()
    {
        components.ForEach(x => x.execute());
    }

    public override void cleanup()
    {
        components.ForEach(x => x.destroy());
    }

    public override void timer(float timestep)
    {
        components.ForEach(x => x.update(timestep));
    }

    public void addComponent(NodeListenerComponent component)
    {
        components.Add(component);
    }

    public void removeComponent(NodeListenerComponent component)
    {
        components.Remove(component);
    }

    public NodeListenerComponent getComponentByID(int id)
    {
        foreach (var component in components)
            if (component.Id == id)
                return component;

        return null;
    }

    public override void GizmoDraw()
    {
        components.ForEach(x => x.GizmosDraw());
    }

    public override void PlayerHitNPC(NPC npc)
    {
        components.ForEach(x => x.PlayerHitNPC(npc));
    }

    public override void PlayerHitObject(GameObject obj)
    {
        components.ForEach(x => x.PlayerHitObject(obj));
    }

    public override void PlayerKilledNPC(GameObject npc)
    {
        components.ForEach(x => x.PlayerKilledNPC(npc));
    }

    public override void NPCHitPlayer(GameObject npc)
    {
        components.ForEach(x => x.NPCHitPlayer(npc));
    }

    public override void NPCTargetsPlayer(GameObject npc)
    {
        components.ForEach(x => x.NPCTargetsPlayer(npc));
    }

    public override void PlayerHitEnemy(GameObject enemy)
    {
        components.ForEach(x => x.PlayerHitEnemy(enemy));
    }

    public override void PlayerKilledEnemy(GameObject enemy)
    {
        components.ForEach(x => x.PlayerKilledEnemy(enemy));
    }

    public override void NPCHitsNPC(GameObject npcTarget, GameObject npcAttacker)
    {
        components.ForEach(x => x.NPCHitsNPC(npcTarget, npcAttacker));
    }
}