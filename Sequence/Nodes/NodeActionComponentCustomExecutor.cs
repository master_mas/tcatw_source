﻿/**
 * Author: Sam Murphy
 */

public interface NodeActionComponentCustomExecutor
{
    void execute(bool random);
}