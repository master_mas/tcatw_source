﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentNPCTargetsPlayer : NodeListenerComponent
{
    public NodeListenerComponentNPCTargetsPlayer(NodeListener listener, int id, float moralityChange) : base(listener,
        id, moralityChange, NodeListenerComponentType.NPC_TARGETS_PLAYER)
    {
    }

    public NPC WatchNpc { get; set; }

    public override void NPCHitPlayer(GameObject npc)
    {
        if (WatchNpc != null && npc.Equals(WatchNpc.gameObject))
            move();
    }
}