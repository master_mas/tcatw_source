﻿using UnityEngine;
using UnityEngine.AI;

/**
 * Author: Sam Murphy
 */

public class NodeActionComponentTeleport : NodeActionComponent
{
    public NodeActionComponentTeleport(NodeAction action, int id) : base(action, id, NodeActionComponentType.TELEPORT)
    {
    }

    public Transform GameObject { get; set; }

    public Transform MoveToLocation { get; set; }

    public override void execute()
    {
        if (GameObject != null && MoveToLocation != null)
        {
            var agents1 = GameObject.GetComponentsInChildren<NavMeshAgent>();
            var agents2 = GameObject.GetComponentsInChildren<NavMeshAgent>();

            foreach (var navMeshAgent in agents1)
                navMeshAgent.enabled = false;

            foreach (var navMeshAgent in agents2)
                navMeshAgent.enabled = false;

            GameObject.position = MoveToLocation.position;
            GameObject.rotation = MoveToLocation.rotation;

            foreach (var navMeshAgent in agents1)
                navMeshAgent.enabled = true;

            foreach (var navMeshAgent in agents2)
                navMeshAgent.enabled = true;
        }
    }
}