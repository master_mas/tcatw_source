﻿/**
 * Author: Sam Murphy
 */

using System.Diagnostics;

public class NodeActionComponentTriggerAnimations : NodeActionComponent
{
    public NodeActionComponentTriggerAnimations(NodeAction action, int id) : base(action, id,
        NodeActionComponentType.ANIMATIONS)
    {
    }

    public ComplexEnemy ComplexEnemy { get; set; }

    public NPC Npc { get; set; }

    public NPCGestures Gestures { get; set; }

    public NPCGestures.GestureType GestureType { get; set; }

    public override void execute()
    {
        if (Gestures != null)
            Gestures.DynamicInvokeControl(GestureType);
    }
}