﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class NodeAction : SequenceNode
{
    private readonly List<NodeActionComponent> components = new List<NodeActionComponent>();

    public NodeAction(int id) : base(id, NodeType.ACTION)
    {
        SelectionIndex = 0;
        Cancellable = false;
    }

    public int SelectionIndex { get; set; }

    public override void execute()
    {
        components.ForEach(x => x.execute());
        destroy();
    }

    public override void cleanup()
    {
        components.ForEach(x => x.destroy());
    }

    public override void timer(float timestep)
    {
        components.ForEach(x => x.update(timestep));
    }

    public override void preInit()
    {
        components.ForEach(x => x.preInit());
    }

    public void addComponent(NodeActionComponent component)
    {
        components.Add(component);
    }

    public void removeComponent(NodeActionComponent component)
    {
        components.Remove(component);
    }

    public NodeActionComponent getComponentByID(int id)
    {
        foreach (var component in components)
            if (component.Id == id)
                return component;

        return null;
    }
}