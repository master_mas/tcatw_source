﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeActionComponentParticleSystem : NodeActionComponent
{
    public NodeActionComponentParticleSystem(NodeAction action, int id) : base(action, id,
        NodeActionComponentType.PARTICLE_SYSTEM)
    {
    }

    public ParticleSystem ParticleSystem { get; set; }

    public override void execute()
    {
        if (ParticleSystem != null)
            ParticleSystem.Play();
    }
}