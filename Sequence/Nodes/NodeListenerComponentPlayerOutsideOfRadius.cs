﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentPlayerOutsideOfRadius : NodeListenerComponent
{
    private GameObject player;

    public NodeListenerComponentPlayerOutsideOfRadius(NodeListener listener, int id, float moralityChange) : base(
        listener, id, moralityChange, NodeListenerComponentType.RADIUS_OUTSIDE_OBJECT)
    {
        WatchObject = null;
    }

    public Transform WatchObject { get; set; }

    public float Radius { get; set; }

    public override void execute()
    {
        player = PlayerManager.instance.GetPlayer();
    }

    public override void destroy()
    {
        player = null;
    }

    public override void update(float delta)
    {
        if (WatchObject != null && player != null)
            if ((WatchObject.position - player.transform.position).magnitude > Radius)
                move();
    }

    public override void GizmosDraw()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(WatchObject.position, Radius);
    }
}