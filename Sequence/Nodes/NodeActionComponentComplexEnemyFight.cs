﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class NodeActionComponentComplexEnemyFight : NodeActionComponent
{
    private List<ComplexEnemy> _complexEnemies = new List<ComplexEnemy>();

    public NodeActionComponentComplexEnemyFight(NodeAction action, int id) : base(action, id,
        NodeActionComponentType.COMPLEX_ENEMY_FIGHT)
    {
    }

    public List<ComplexEnemy> ComplexEnemies
    {
        get { return _complexEnemies; }
        set { _complexEnemies = value; }
    }

    public override void execute()
    {
        _complexEnemies.ForEach(complexEnemy =>
        {
            if (complexEnemy != null)
                complexEnemy.SetupAIvsAI();
        });
    }
}