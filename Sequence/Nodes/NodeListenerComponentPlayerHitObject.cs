﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentPlayerHitObject : NodeListenerComponent
{
    private int currentHits;

    public NodeListenerComponentPlayerHitObject(NodeListener listener, int id, float moralityChange) : base(listener,
        id, moralityChange, NodeListenerComponentType.PLAYER_HIT_OBJECT)
    {
    }

    public GameObject WatchObject { get; set; }

    public int RequireHitCountToPass { get; set; }

    public override void destroy()
    {
        currentHits = 0;
    }

    public override void PlayerHitObject(GameObject obj)
    {
        if (WatchObject != null && obj.Equals(WatchObject))
        {
            currentHits++;
            if (currentHits >= RequireHitCountToPass)
                move();
        }
    }
}