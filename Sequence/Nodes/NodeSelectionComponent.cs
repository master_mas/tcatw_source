﻿using System;
using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class NodeSelectionComponent
{
    public NodeSelectionComponent(string message, float weighting, int id)
    {
        OutLinkNodes = new List<SequenceNode>();
        Message = message;
        Weighting = weighting;
        if (id == -1)
            Id = (int) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds - new Random().Next(100);
        else
            Id = id;
    }

    public string Message { get; set; }

    public float Weighting { get; set; }

    public List<SequenceNode> OutLinkNodes { get; private set; }

    public int Id { get; private set; }

    public void addNode(SequenceNode node)
    {
        OutLinkNodes.Add(node);
    }

    public void removeNode(SequenceNode node)
    {
        OutLinkNodes.Remove(node);
    }
}