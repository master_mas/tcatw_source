﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentPlayerKilledEnemy : NodeListenerComponent
{
    public NodeListenerComponentPlayerKilledEnemy(NodeListener listener, int id, float moralityChange) : base(listener,
        id, moralityChange, NodeListenerComponentType.PLAYER_KILLED_ENEMY)
    {
    }

    public ComplexEnemy WatchEnemy { get; set; }

    public override void PlayerKilledEnemy(GameObject enemy)
    {
        if (WatchEnemy != null && WatchEnemy.gameObject.Equals(enemy))
            move();
    }
}