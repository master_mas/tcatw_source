﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

/**
 * Author: Sam Murphy
 */

public abstract class SequenceNode
{
    protected readonly List<SequenceNode> cancelPoints = new List<SequenceNode>();
    protected readonly List<SequenceNode> inPoints = new List<SequenceNode>();
    protected readonly List<SequenceNode> outPoints = new List<SequenceNode>();

    private bool cancellable = true;

    public float maxWeighting = 0;

    protected SequenceContainer sequenceContainer;

    protected SequenceNode(int id, NodeType type)
    {
        Weighting = 0;
        UseWeighting = true;
        Active = false;
        Type = type;

        if (id == -1)
            do
            {
                Id = (int) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds - new Random(DateTime.UtcNow.Millisecond).Next(100);
            } while (ActiveSequenceContainer.checkIfEditorIDExists(id));
        else
            Id = id;
    }

    public SequenceContainer SequenceContainer
    {
        get { return sequenceContainer; }
        set { sequenceContainer = value; }
    }

    public int Id { get; private set; }

    public NodeType Type { get; private set; }

    public bool Active { get; set; }

    public bool UseWeighting { get; set; }

    public float Weighting { get; set; }

    public bool Cancellable
    {
        get { return cancellable; }
        set { cancellable = value; }
    }

    public void init()
    {
        if (Active)
            return;

        try
        {
            ActiveSequenceContainer.armedNodes[sequenceContainer].Add(this);
            Active = true;
            execute();
        }
        catch (KeyNotFoundException)
        {
            Debug.LogError("Failed to Start Node: " + GetType().Name + " due to Sequence Container not in Armed Nodes");
        }
    }

    public virtual void timer(float timestep)
    {
    }

    public abstract void execute();

    public abstract void cleanup();

    public virtual void preInit() { }

    public virtual void GizmoDraw() { }

    public void destroy()
    {
        Active = false;
        try
        {
            ActiveSequenceContainer.armedNodes[sequenceContainer].Remove(this);
        }
        catch (KeyNotFoundException e)
        {
            Debug.LogError("Key not found when removing node from armed Nodes. Rouge Node!");
        }
        cleanup();
    }

    public virtual void move()
    {
        if (!Active)
            return;

        destroy();

        var activateNodes = new List<SequenceNode>();

        foreach (var node in outPoints)
        {
            if (!node.UseWeighting)
            {
                if (node.Type == NodeType.GROUP_TRIGGER)
                    activateNodes.Insert(0, node);
                else
                    activateNodes.Add(node);
                continue;
            }

            var weight = MoralityController.Self.Weight;

            if (weight < 0 && weight <= node.maxWeighting && node.Weighting <= weight)
                activateNodes.Add(node);
            else if (weight >= 0 && weight >= node.Weighting && weight <= node.maxWeighting)
                activateNodes.Add(node);
        }

        activateNodes.ForEach(x =>
        {
            if (x.isTriggerCancellable(this))
                x.destroy();
            else
                x.init();
        });
    }

    public bool isTriggerCancellable(SequenceNode node)
    {
        return cancelPoints.Contains(node);
    }

    public void registerNewOutLink(SequenceNode node)
    {
        if (!outPoints.Contains(node))
            outPoints.Add(node);
    }

    public void removeOutLink(SequenceNode node)
    {
        outPoints.Remove(node);
    }

    public void registerNewInLink(SequenceNode node)
    {
        if (!inPoints.Contains(node))
            inPoints.Add(node);
    }

    public void registerNewCancelLink(SequenceNode node)
    {
        if (!cancelPoints.Contains(node))
            cancelPoints.Add(node);
    }

    public void removeInLink(SequenceNode node)
    {
        inPoints.Remove(node);
    }

    public override bool Equals(object obj)
    {
        if (obj != null && obj.GetType().IsAssignableFrom(typeof(SequenceNode)))
            return ((SequenceNode) obj).Id == Id;
        return base.Equals(obj);
    }

    public void log(object data)
    {
        Debug.Log(data);
    }

    public virtual void PlayerHitNPC(NPC npc)
    {
    }

    public virtual void PlayerHitObject(GameObject obj)
    {
    }

    public virtual void PlayerKilledNPC(GameObject npc)
    {
    }

    public virtual void NPCHitPlayer(GameObject npc)
    {
    }

    public virtual void NPCTargetsPlayer(GameObject npc)
    {
    }

    public virtual void PlayerHitEnemy(GameObject enemy)
    {
//        Debug.Log("Notifing all activate nodes of Player hit Enemy: " + enemy.name);
        Debug.Log(ActiveSequenceContainer.armedNodes[sequenceContainer].Count);
    }

    public virtual void PlayerKilledEnemy(GameObject enemy)
    {
    }

    public virtual void NPCHitsNPC(GameObject npcTarget, GameObject npcAttacker)
    {
    }

    public override string ToString()
    {
        return GetType().Name + ": " + Id;
    }
}