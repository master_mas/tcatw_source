﻿/**
 * Author: Sam Murphy
 */

public enum NodeType
{
    SELECTION,
    ENTRY,
    EXIT,
    MESSAGE,
    WAIT,
    TIMER,
    LISTENER,
    MORALITY_CHANGE,
    ACTION,
    GROUP_TRIGGER,
    TUTORIAL_MESSAGE,
    RANDOM
}