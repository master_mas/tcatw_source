﻿using UnityEngine;

public class NodeActionComponentInverseActive : NodeActionComponent
{
    private GameObject target;

    public NodeActionComponentInverseActive(NodeAction action, int id) : base(action, id, NodeActionComponentType.INVERSE_ACTIVE) { }

    public override void execute()
    {
        target.SetActive(!target.activeSelf);
    }

    public GameObject Target
    {
        get { return target; }
        set { target = value; }
    }
}
