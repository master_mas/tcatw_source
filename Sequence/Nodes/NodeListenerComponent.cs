﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

/**
 * Author: Sam Murphy
 */

public abstract class NodeListenerComponent
{
    protected NodeListener nodeListener;

    protected NodeListenerComponent(NodeListener listener, int id, float moralityChange,
        NodeListenerComponentType componentType)
    {
        OutLinkNodes = new List<SequenceNode>();
        MoralityChange = moralityChange;
        ComponentType = componentType;
        nodeListener = listener;

        if (id == -1)
            Id = (int) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds - new Random().Next(100);
        else
            Id = id;
    }

    public int Id { get; private set; }

    public float MoralityChange { get; set; }

    public NodeListenerComponentType ComponentType { get; private set; }

    public List<SequenceNode> OutLinkNodes { get; private set; }

    public virtual void execute()
    {
    }

    public virtual void update(float delta)
    {
    }

    public virtual void destroy()
    {
    }

    protected void move()
    {
        nodeListener.destroy();

        var activateNodes = new List<SequenceNode>();

        foreach (var node in OutLinkNodes)
        {
            if (!node.UseWeighting)
            {
                if (node.Type == NodeType.GROUP_TRIGGER)
                    activateNodes.Insert(0, node);
                else
                    activateNodes.Add(node);
                continue;
            }

            var weight = MoralityController.Self.Weight;

            //Lower
            if (weight < 0 && weight <= node.maxWeighting && node.Weighting <= weight)
                activateNodes.Add(node);
            //Higher
            else if (weight >= 0 && weight >= node.Weighting && weight <= node.maxWeighting)
                activateNodes.Add(node);
        }

        activateNodes.ForEach(x =>
        {
            if (x.isTriggerCancellable(nodeListener))
                x.destroy();
            else
                x.init();
        });
    }

    public void addNode(SequenceNode node)
    {
        OutLinkNodes.Add(node);
    }

    public void removeNode(SequenceNode node)
    {
        OutLinkNodes.Remove(node);
    }

    public virtual void GizmosDraw() { }

    public virtual void PlayerHitNPC(NPC npc)
    {
    }

    public virtual void PlayerHitObject(GameObject obj)
    {
    }

    public virtual void PlayerKilledNPC(GameObject npc)
    {
    }

    public virtual void NPCHitPlayer(GameObject npc)
    {
    }

    public virtual void NPCTargetsPlayer(GameObject npc)
    {
    }

    public virtual void PlayerHitEnemy(GameObject enemy)
    {
    }

    public virtual void PlayerKilledEnemy(GameObject enemy)
    {
    }

    public virtual void NPCHitsNPC(GameObject npcTarget, GameObject npcAttacker)
    {
    }
}