﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentPlayerHitEnemy : NodeListenerComponent
{
    private int currentHits;

    public NodeListenerComponentPlayerHitEnemy(NodeListener listener, int id, float moralityChange) : base(listener, id,
        moralityChange, NodeListenerComponentType.PLAYER_HIT_ENEMY)
    {
    }

    public ComplexEnemy WatchEnemy { get; set; }

    public int RequireHitCountToPass { get; set; }

    public override void destroy()
    {
        currentHits = 0;
    }

    public override void PlayerHitEnemy(GameObject enemy)
    {
        Debug.Log("Yes, this happens");
        if (WatchEnemy != null && enemy.Equals(WatchEnemy.gameObject))
        {
            currentHits++;
            if (currentHits >= RequireHitCountToPass)
                move();
        }
    }
}