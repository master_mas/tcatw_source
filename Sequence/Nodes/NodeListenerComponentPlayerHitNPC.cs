﻿/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentPlayerHitNPC : NodeListenerComponent
{
    private int currentHits;

    public NodeListenerComponentPlayerHitNPC(NodeListener listener, int id, float moralityChange) : base(listener, id,
        moralityChange, NodeListenerComponentType.PLAYER_HIT_NPC)
    {
    }

    public NPC WatchNpc { get; set; }

    public int RequireHitCountToPass { get; set; }

    public override void destroy()
    {
        currentHits = 0;
    }

    public override void PlayerHitNPC(NPC npc)
    {
        if (WatchNpc != null && npc.Equals(WatchNpc))
        {
            currentHits++;
            if (currentHits >= RequireHitCountToPass)
                move();
        }
    }
}