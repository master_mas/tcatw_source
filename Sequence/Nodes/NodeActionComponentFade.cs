﻿/**
 * Author: Sam Murphy
 */

using UnityEngine;

public class NodeActionComponentFade : NodeActionComponent
{
    public NodeActionComponentFade(NodeAction action, int id) : base(action, id, NodeActionComponentType.FADE)
    {
    }

    public bool ToBlack { get; set; }

    public override void execute()
    {
        FadeToBlack.instance.sequenceExecutor(ToBlack);
    }
}