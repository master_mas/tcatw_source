﻿using System;
using System.Collections.Generic;

public class NodeRandom : SequenceNode
{
    public NodeRandom(int id) : base(id, NodeType.RANDOM)
    {
        Cancellable = false;
        UseWeighting = false;
    }

    public override void execute()
    {
        move();
    }

    public override void move()
    {
        if (!Active)
            return;

        destroy();

        var activateNodes = new List<SequenceNode>();
        var randomNodes = new List<SequenceNode>();

        foreach (var node in outPoints)
        {
            if (!node.UseWeighting)
            {
                if (node.Type == NodeType.GROUP_TRIGGER)
                    activateNodes.Insert(0, node);
                else                    
                    activateNodes.Add(node);
            }

            var weight = MoralityController.Self.Weight;

            if (weight < 0 && weight <= node.maxWeighting && node.Weighting <= weight)
                activateNodes.Add(node);
            else if (weight >= 0 && weight >= node.Weighting && weight <= node.maxWeighting)
                activateNodes.Add(node);
        }

        activateNodes.ForEach(x =>
        {
            if (x.isTriggerCancellable(this))
                x.destroy();
            else
                randomNodes.Add(x);
        });

        if (randomNodes.Count == 0)
        {
            return;
        }

        randomNodes[new Random(DateTime.UtcNow.Second).Next(randomNodes.Count)].init();
    }

    public override void cleanup() { }
}