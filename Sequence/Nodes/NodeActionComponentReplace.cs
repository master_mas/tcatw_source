﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeActionComponentReplace : NodeActionComponent
{
    public NodeActionComponentReplace(NodeAction action, int id) : base(action, id, NodeActionComponentType.REPLACE)
    {
    }

    public GameObject ToBeReplace { get; set; }

    public GameObject Replacer { get; set; }

    public override void execute()
    {
        Replacer.SetActive(true);
        Replacer.transform.position = ToBeReplace.transform.position;
        Replacer.transform.rotation = ToBeReplace.transform.rotation;
        ToBeReplace.SetActive(false);
    }
}