﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentPlayerKilledNPC : NodeListenerComponent
{
    public NodeListenerComponentPlayerKilledNPC(NodeListener listener, int id, float moralityChange) : base(listener,
        id, moralityChange, NodeListenerComponentType.PLAYER_KILLED_NPC)
    {
    }

    public NPC WatchNpc { get; set; }

    public override void PlayerKilledNPC(GameObject npc)
    {
        if (WatchNpc != null && WatchNpc.gameObject.Equals(npc))
            move();
    }
}