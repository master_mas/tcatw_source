﻿using System;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class NodeMessage : SequenceNode
{
    private Color color;
    private ComplexEnemy complexEnemyReference;
    private bool killMessgaeOnDeath;
    private NPC npcReference;
    private float waitTime;

    public NodeMessage(int id) : base(id, NodeType.MESSAGE)
    {
        Text = "";
    }

    public float WaitTime
    {
        get { return waitTime; }
        set { waitTime = value; }
    }

    public string Text { get; set; }

    public NPC NpcReference
    {
        get { return npcReference; }
        set { npcReference = value; }
    }

    public ComplexEnemy ComplexEnemyReference
    {
        get { return complexEnemyReference; }
        set { complexEnemyReference = value; }
    }

    public Color Color
    {
        get { return color; }
        set { color = value; }
    }

    public bool KillMessgaeOnDeath
    {
        get { return killMessgaeOnDeath; }
        set { killMessgaeOnDeath = value; }
    }

    public override void execute()
    {
        if (npcReference != null)
            sequenceContainer.StartCoroutine(
                npcReference.PlayDialogue(Text, waitTime, move));
        else if (complexEnemyReference != null)
            sequenceContainer.StartCoroutine(
                complexEnemyReference.PlayDialogue(Text, waitTime, move));
        else
            move();
    }

    public override void cleanup()
    {
        if (!killMessgaeOnDeath && npcReference != null)
            npcReference.StopDialogue();
    }

    public override void PlayerKilledNPC(GameObject npc)
    {
        if (npcReference != null && npc.Equals(npcReference.gameObject))
            if (!killMessgaeOnDeath)
                npcReference.StopDialogue();
    }

    public override void PlayerKilledEnemy(GameObject enemy)
    {
        if (complexEnemyReference != null && enemy.Equals(npcReference.gameObject))
            if (!killMessgaeOnDeath)
                complexEnemyReference.StopDialogue();
    }
}