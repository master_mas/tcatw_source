﻿/**
 * Author: Sam Murphy
 */

public class NodeEntry : SequenceNode
{
    public NodeEntry(int id) : base(id, NodeType.ENTRY)
    {
        UseWeighting = false;
        Cancellable = false;
    }

    public override void cleanup()
    {
    }

    public override void execute()
    {
        move();
    }
}