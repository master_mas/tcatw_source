﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentPlayerWithinRadius : NodeListenerComponent
{
    private GameObject player;

    public NodeListenerComponentPlayerWithinRadius(NodeListener listener, int id, float moralityChange) : base(listener,
        id, moralityChange, NodeListenerComponentType.RADIUS_WITHIN_OBJECT)
    {
        WatchObject = null;
    }

    public Transform WatchObject { get; set; }

    public float Radius { get; set; }

    public override void execute()
    {
        MoralityController.Self.changeListeners.Add(setPlayer);
        setPlayer();
    }

    private void setPlayer()
    {
        player = PlayerManager.instance.GetPlayer();
    }

    public override void destroy()
    {
        MoralityController.Self.changeListeners.Remove(setPlayer);
        player = null;
    }

    public override void update(float delta)
    {
        if (WatchObject != null && player != null)
            if ((WatchObject.position - player.transform.position).magnitude <= Radius)
                move();
    }

    public override void GizmosDraw()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(WatchObject.position, Radius);
    }
}