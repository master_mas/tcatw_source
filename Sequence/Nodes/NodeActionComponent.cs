﻿using System;

/**
 * Author: Sam Murphy
 */

public abstract class NodeActionComponent
{
    private readonly NodeActionComponentType componentType;
    protected NodeAction nodeAction;

    protected NodeActionComponent(NodeAction action, int id, NodeActionComponentType componentType)
    {
        this.componentType = componentType;
        nodeAction = action;

        if (id == -1)
            Id = (int) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds - new Random().Next(100);
        else
            Id = id;
    }

    public int Id { get; private set; }

    public NodeActionComponentType ComponentType
    {
        get { return componentType; }
    }

    public virtual void execute()
    {
    }

    public virtual void update(float delta)
    {
    }

    public virtual void destroy()
    {
    }

    public virtual void preInit()
    {
    }
}