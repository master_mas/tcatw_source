﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeListenerComponentNPCHitPlayer : NodeListenerComponent
{
    private int currentHits;

    public NodeListenerComponentNPCHitPlayer(NodeListener listener, int id, float moralityChange) : base(listener, id,
        moralityChange, NodeListenerComponentType.NPC_HIT_PLAYER)
    {
    }

    public NPC WatchNpc { get; set; }

    public int RequireHitCountToPass { get; set; }

    public override void destroy()
    {
        currentHits = 0;
    }

    public override void NPCHitPlayer(GameObject npc)
    {
        if (WatchNpc != null && npc.Equals(WatchNpc.gameObject))
        {
            currentHits++;
            if (currentHits >= RequireHitCountToPass)
                move();
        }
    }
}