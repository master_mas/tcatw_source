﻿using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class NodeActionComponentNPCRunAway : NodeActionComponent
{
    public NodeActionComponentNPCRunAway(NodeAction action, int id) : base(action, id,
        NodeActionComponentType.NPC_RUN_AWAY)
    {
    }

    public NPC Npc { get; set; }

    public Transform Location { get; set; }

    public override void execute()
    {
        Npc.RunAway(Location);
    }
}