﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

/**
 * Container for an entire sequence.
 * Author: Sam Murphy
 */

/**
 * Unity Flags. Requires Edit Mode, not Play Mode. When attaching this component to objects, 
 * they also need a Collider and Sequence Update Manager. Allowed only one Sequence Container per object
 */
[ExecuteInEditMode]
[RequireComponent(typeof(Collider), typeof(SequenceUpdateManager))]
[DisallowMultipleComponent]
public class SequenceContainer : MonoBehaviour
{
    private static int idCounter = 0;

    private int id;
    public bool debugMode = false;
    private bool disableCollider = false;

    public void Awake()
    {
        id = idCounter++;
    }

    public GameObject Instantiate(Object original, Transform parent)
    {
        return (GameObject) Instantiate(original, parent, false);
    }

    public void DestroyObject(GameObject obj)
    {
        Destroy(obj);
    }

    public void armSequence()
    {
        ActiveSequenceContainer.gameUsingContainer.Add(this);
        ActiveSequenceContainer.armedNodes.Add(this, new List<SequenceNode>());
        ActiveSequenceContainer.gameUsingNodes.Add(this, new List<SequenceNode>());
//        ActiveSequenceContainer.gameUsingNodes.Clear();

        //Path to Game Directory
        var dir = Application.dataPath + "/Dialog/" + SceneManager.GetActiveScene().name;

        var data = IONodes.load(dir + "/" + IONodes.directoryStructureBuilder(transform) + ".dialog");

        if (data == null)
        {
            Debug.LogError("Sequencer Failed to Load: " + dir + "/" + IONodes.directoryStructureBuilder(transform) + ".dialog");
        }

        //Build all the nodes using Object Initialisers
        foreach (var node in data)
            switch ((NodeType) Enum.Parse(typeof(NodeType), node.type))
            {
                case NodeType.MORALITY_CHANGE:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeMoralityChange(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        Change = ((SerialNodeMoralityChange) node).amount,
                        SequenceContainer = this
                    });
                    break;
                case NodeType.GROUP_TRIGGER:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeGroupTrigger(node.id)
                    {
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        SequenceContainer = this
                    });
                    break;
                case NodeType.RANDOM:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeRandom(node.id)
                    {
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        SequenceContainer = this
                    });
                    break;
                case NodeType.ENTRY:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeEntry(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        SequenceContainer = this
                    });
                    break;
                case NodeType.EXIT:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeExit(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        DisableCollider = ((SerialNodeExit) node).disableCollider,
                        SequenceContainer = this
                    });
                    break;
                case NodeType.TIMER:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeTimer(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        TotalWaitTime = ((SerialNodeTimer) node).maxWaitTime,
                        maxWeighting = node.maxWeight,
                        SequenceContainer = this
                    });
                    break;
                case NodeType.MESSAGE:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeMessage(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        Text = ((SerialNodeMessage) node).dialog,
                        NpcReference = getObjectComponentFromScene<NPC>(((SerialNodeMessage) node).objectLocalIdInFile),
                        ComplexEnemyReference =
                            getObjectComponentFromScene<ComplexEnemy>(((SerialNodeMessage) node).complexEnemyLocalID),
                        WaitTime = ((SerialNodeMessage) node).waitTime,
                        KillMessgaeOnDeath = ((SerialNodeMessage) node).killMessageOnDeath,
                        SequenceContainer = this
                        //Color = convertFloatToColor(((SerialNodeMessage)node).color)
                    });
                    break;
                case NodeType.TUTORIAL_MESSAGE:
                    NodeTutorialMessage nodeTutorialMessage;
                    ActiveSequenceContainer.gameUsingNodes[this].Add(nodeTutorialMessage = new NodeTutorialMessage(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        Text = ((SerialNodeTutorialMessage) node).message,
                        WaitTime = ((SerialNodeTutorialMessage) node).waitTime,
                        SequenceContainer = this
                    });
                    break;
                case NodeType.LISTENER:
                    NodeListener nodeListener;
                    ActiveSequenceContainer.gameUsingNodes[this].Add(nodeListener = new NodeListener(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        SequenceContainer = this
                    });

                    var serialNodeListener = (SerialNodeListener) node;
                    foreach (var component in serialNodeListener.components)
                        switch ((NodeListenerComponentType) Enum.Parse(typeof(NodeListenerComponentType),
                            component.type))
                        {
                            case NodeListenerComponentType.RADIUS_WITHIN_OBJECT:
                                nodeListener.addComponent(
                                    new NodeListenerComponentPlayerWithinRadius(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        Radius = ((SerialNodeListenerComponentPlayerWithinRadius) component).radius,
                                        WatchObject =
                                            getObjectComponentFromScene<Transform>(
                                                ((SerialNodeListenerComponentPlayerWithinRadius) component)
                                                .objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.RADIUS_OUTSIDE_OBJECT:
                                nodeListener.addComponent(
                                    new NodeListenerComponentPlayerOutsideOfRadius(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        Radius = ((SerialNodeListenerComponentPlayerOutsideRadius) component).radius,
                                        WatchObject =
                                            getObjectComponentFromScene<Transform>(
                                                ((SerialNodeListenerComponentPlayerOutsideRadius) component)
                                                .objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_HIT_NPC:
                                nodeListener.addComponent(
                                    new NodeListenerComponentPlayerHitNPC(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        RequireHitCountToPass =
                                            ((SerialNodeListenerComponentPlayerHitNPC) component).requirePass,
                                        WatchNpc = getObjectComponentFromScene<NPC>(
                                            ((SerialNodeListenerComponentPlayerHitNPC) component).objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_HIT_ENEMY:
                                nodeListener.addComponent(
                                    new NodeListenerComponentPlayerHitEnemy(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        RequireHitCountToPass =
                                            ((SerialNodeListenerComponentPlayerHitEnemy) component).requirePass,
                                        WatchEnemy =
                                            getObjectComponentFromScene<ComplexEnemy>(
                                                ((SerialNodeListenerComponentPlayerHitEnemy) component)
                                                .objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_HIT_OBJECT:
                                nodeListener.addComponent(
                                    new NodeListenerComponentPlayerHitObject(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        RequireHitCountToPass = ((SerialNodeListenerComponentPlayerHitObject) component)
                                            .requirePass,
                                        WatchObject =
                                            getObjectFromScene(((SerialNodeListenerComponentPlayerHitObject) component)
                                                .objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_KILLED_ENEMY:
                                nodeListener.addComponent(
                                    new NodeListenerComponentPlayerKilledEnemy(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        WatchEnemy =
                                            getObjectComponentFromScene<ComplexEnemy>(
                                                ((SerialNodeListenerComponentPlayerKillEnemy) component)
                                                .objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.PLAYER_KILLED_NPC:
                                nodeListener.addComponent(
                                    new NodeListenerComponentPlayerKilledNPC(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        WatchNpc = getObjectComponentFromScene<NPC>(
                                            ((SerialNodeListenerComponentPlayerKillNPC) component).objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.NPC_HIT_PLAYER:
                                nodeListener.addComponent(
                                    new NodeListenerComponentNPCHitPlayer(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        RequireHitCountToPass =
                                            ((SerialNodeListenerComponentNPCHitPlayer) component).requirePass,
                                        WatchNpc = getObjectComponentFromScene<NPC>(
                                            ((SerialNodeListenerComponentNPCHitPlayer) component).objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.NPC_TARGETS_PLAYER:
                                nodeListener.addComponent(
                                    new NodeListenerComponentNPCTargetsPlayer(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        WatchNpc = getObjectComponentFromScene<NPC>(
                                            ((SerialNodeListenerComponentNPCTargetsPlayer) component)
                                            .objectLocalIdInFile)
                                    });
                                break;
                            case NodeListenerComponentType.NPC_HIT_NPC:
                                nodeListener.addComponent(
                                    new NodeListenerComponentNPCHitNPC(nodeListener, component.id,
                                        component.moralityChange)
                                    {
                                        RequireHitCountToPass =
                                            ((SerialNodeListenerComponentNPCHitNPC) component).requirePass,
                                        NpcAttacker =
                                            getObjectComponentFromScene<NPC>(
                                                ((SerialNodeListenerComponentNPCHitNPC) component).attacker),
                                        NpcTarget = getObjectComponentFromScene<NPC>(
                                            ((SerialNodeListenerComponentNPCHitNPC) component).target)
                                    });
                                break;
                        }

                    break;
                case NodeType.ACTION:
                    NodeAction nodeAction;
                    ActiveSequenceContainer.gameUsingNodes[this].Add(nodeAction = new NodeAction(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        Cancellable = node.cancellable,
                        SequenceContainer = this
                    });

                    var serialNodeAction = (SerialNodeAction) node;
                    foreach (var component in serialNodeAction.components)
                        switch ((NodeActionComponentType) Enum.Parse(typeof(NodeActionComponentType),
                            component.type))
                        {
                            case NodeActionComponentType.CUSTOM:
                                nodeAction.addComponent(new NodeActionComponentCustom(nodeAction, component.id)
                                {
                                    WatchObject =
                                        getComponentWithInterface<NodeActionComponentCustomExecutor>(
                                            getObjectFromScene(((SerialNodeActionComponentCustom) component)
                                                .objectLocalIdInFile))
                                });
                                break;
                            case NodeActionComponentType.PARTICLE_SYSTEM:
                                nodeAction.addComponent(new NodeActionComponentParticleSystem(nodeAction, component.id)
                                {
                                    ParticleSystem =
                                        getObjectComponentFromScene<ParticleSystem>(
                                            ((SerialNodeActionComponentParticleSystem) component).objectLocalIdInFile)
                                });
                                break;
                            case NodeActionComponentType.SOUND:
                                nodeAction.addComponent(new NodeActionComponentSound(nodeAction, component.id)
                                {
                                    SoundPlayer =
                                        getObjectComponentFromScene<SoundPlayer>(
                                            ((SerialNodeActionComponentSound) component).objectLocalIdInFile)
                                });
                                break;
                            case NodeActionComponentType.TELEPORT:
                                nodeAction.addComponent(new NodeActionComponentTeleport(nodeAction, component.id)
                                {
                                    GameObject =
                                        getObjectComponentFromScene<Transform>(
                                            ((SerialNodeActionComponentTeleport) component).gameObject),
                                    MoveToLocation =
                                        getObjectComponentFromScene<Transform>(
                                            ((SerialNodeActionComponentTeleport) component).moveToLocation)
                                });
                                break;
                            case NodeActionComponentType.NPC_RUN_AWAY:
                                nodeAction.addComponent(new NodeActionComponentNPCRunAway(nodeAction, component.id)
                                {
                                    Npc = getObjectComponentFromScene<NPC>(
                                        ((SerialNodeActionComponentNPCRunAway) component).npc),
                                    Location = getObjectComponentFromScene<Transform>(
                                        ((SerialNodeActionComponentNPCRunAway) component).moveToLocation)
                                });
                                break;
                            case NodeActionComponentType.REPLACE:
                                nodeAction.addComponent(new NodeActionComponentReplace(nodeAction, component.id)
                                {
                                    ToBeReplace =
                                        getObjectFromScene(((SerialNodeActionComponentReplace) component).replaceThis),
                                    Replacer = getObjectFromScene(((SerialNodeActionComponentReplace) component)
                                        .replaceWith)
                                });
                                break;
                            case NodeActionComponentType.COMPLEX_ENEMY_FIGHT:
                                nodeAction.addComponent(
                                    new NodeActionComponentComplexEnemyFight(nodeAction, component.id)
                                    {
                                        ComplexEnemies = ((SerialNodeActionComponentComplexEnemyFight) component)
                                            .objectNames.Select(
                                                obj => obj.Equals("")
                                                    ? null
                                                    : getObjectComponentFromScene<ComplexEnemy>(obj)
                                            ).ToList()
                                    });
                                break;
                            case NodeActionComponentType.WOLVES_GOES_HOSTILE:
                                nodeAction.addComponent(
                                    new NodeActionComponentComplexEnemyWolvesGoHostile(nodeAction, component.id)
                                    {
                                        Humans = ((SerialNodeActionComponentComplexEnemyWolvesGoHostile) component)
                                            .humanObjectNames.Select(
                                                obj => obj.Equals("")
                                                    ? null
                                                    : getObjectComponentFromScene<ComplexEnemy>(obj)
                                            ).ToList(),
                                        Wolves = ((SerialNodeActionComponentComplexEnemyWolvesGoHostile) component)
                                            .wolfObjectNames.Select(
                                                obj => obj.Equals("")
                                                    ? null
                                                    : getObjectComponentFromScene<ComplexEnemy>(obj)
                                            ).ToList()
                                    });
                                break;
                            case NodeActionComponentType.HUMAN_GOES_HOSTILE:
                                nodeAction.addComponent(
                                    new NodeActionComponentComplexEnemyWolvesGoHuman(nodeAction, component.id)
                                    {
                                        Humans = ((SerialNodeActionComponentComplexEnemyWolvesGoHuman) component)
                                            .humanObjectNames.Select(
                                                obj => obj.Equals("")
                                                    ? null
                                                    : getObjectComponentFromScene<ComplexEnemy>(obj)
                                            ).ToList(),
                                        Wolves = ((SerialNodeActionComponentComplexEnemyWolvesGoHuman) component)
                                            .wolfObjectNames.Select(
                                                obj => obj.Equals("")
                                                    ? null
                                                    : getObjectComponentFromScene<ComplexEnemy>(obj)
                                            ).ToList()
                                    });
                                break;
                            case NodeActionComponentType.PLAYER_INPUT:
                                nodeAction.addComponent(new NodeActionComponentPlayerInput(nodeAction, component.id)
                                {
                                    AllowInput = ((SerialNodeActionComponentPlayerInput) component).allowInput
                                });
                                break;
                            case NodeActionComponentType.FADE:
                                nodeAction.addComponent(new NodeActionComponentFade(nodeAction, component.id)
                                {
                                    ToBlack = ((SerialNodeActionComponentFade) component).toBlack
                                });
                                break;
                            case NodeActionComponentType.ANIMATIONS:
                                nodeAction.addComponent(
                                    new NodeActionComponentTriggerAnimations(nodeAction, component.id)
                                    {
                                        Gestures = getObjectComponentFromScene<NPCGestures>(
                                            ((SerialNodeActionComponentTriggerAnimations) component).objectID),
                                        GestureType = (NPCGestures.GestureType) Enum.Parse(typeof(NPCGestures.GestureType), 
                                            ((SerialNodeActionComponentTriggerAnimations)component).gestureControl)
                                    });
                                break;
                            case NodeActionComponentType.CHANGE_AMBIENT_SOUND:
                                nodeAction.addComponent(new NodeActionComponentChangeAmbientSound(nodeAction, component.id)
                                {
                                    MusicState = (MusicManager.MusicState)Enum.Parse(typeof(MusicManager.MusicState),
                                        ((SerialNodeActionComponentChangeAmbientSound)component).sound)
                                });
                                break;
                            case NodeActionComponentType.INVERSE_ACTIVE:
                                nodeAction.addComponent(new NodeActionComponentInverseActive(nodeAction, component.id)
                                {
                                    Target = getObjectFromScene(((SerialNodeActionComponentInverseActive)component).target)
                                });
                                break;
                        }
                    break;
                case NodeType.SELECTION:
                    NodeSelection nodeSelection;
                    ActiveSequenceContainer.gameUsingNodes[this].Add(nodeSelection = new NodeSelection(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        Cancellable = node.cancellable,
                        SequenceContainer = this
                    });

                    var selection = (SerialNodeSelection) node;

                    foreach (var selectionData in selection.selections)
                        nodeSelection.addComponent(new NodeSelectionComponent(selectionData.dialog,
                            selectionData.weight, selectionData.id));

                    break;
                case NodeType.WAIT:
                    ActiveSequenceContainer.gameUsingNodes[this].Add(new NodeWait(node.id)
                    {
                        UseWeighting = node.useWeighting,
                        Weighting = node.weight,
                        maxWeighting = node.maxWeight,
                        SequenceContainer = this
                    });
                    break;
            }

        //Create all Node Links
        var refID = 0;
        foreach (var node in data)
        {
            switch ((NodeType) Enum.Parse(typeof(NodeType), node.type))
            {
                case NodeType.ENTRY:
                case NodeType.MESSAGE:
                case NodeType.WAIT:
                case NodeType.TIMER:
                case NodeType.MORALITY_CHANGE:
                case NodeType.GROUP_TRIGGER:
                case NodeType.TUTORIAL_MESSAGE:
                case NodeType.RANDOM:
                    foreach (var link in node.cancelLinkIDs)
                    foreach (var sNode in ActiveSequenceContainer.gameUsingNodes[this])
                        if (sNode.Id == link)
                        {
                            var org = ActiveSequenceContainer.gameUsingNodes[this][refID];
                            sNode.registerNewCancelLink(org);
                            org.registerNewOutLink(sNode);
                        }

                    foreach (var link in node.outLinkIDs)
                    foreach (var sNode in ActiveSequenceContainer.gameUsingNodes[this])
                        if (sNode.Id == link)
                        {
                            var org = ActiveSequenceContainer.gameUsingNodes[this][refID];
                            sNode.registerNewInLink(org);
                            org.registerNewOutLink(sNode);
                        }
                    break;
                case NodeType.LISTENER:
                    var serialNodeListener = (SerialNodeListener) node;
                    var nodeListener = (NodeListener) ActiveSequenceContainer.gameUsingNodes[this][refID];

                    foreach (var nodeListenerComponent in serialNodeListener.components)
                    foreach (var link in nodeListenerComponent.cancelLinkIDs)
                    foreach (var sNode in ActiveSequenceContainer.gameUsingNodes[this])
                        if (sNode.Id == link)
                        {
                            var component = nodeListener.getComponentByID(nodeListenerComponent.id);
                            sNode.registerNewCancelLink(nodeListener);
                            component.OutLinkNodes.Add(sNode);
                        }

                    foreach (var nodeListenerComponent in serialNodeListener.components)
                    foreach (var link in nodeListenerComponent.outLinkIDs)
                    foreach (var sNode in ActiveSequenceContainer.gameUsingNodes[this])
                        if (sNode.Id == link)
                        {
                            var component =
                                nodeListener.getComponentByID(nodeListenerComponent.id);
                            sNode.registerNewInLink(nodeListener);
                            component.OutLinkNodes.Add(sNode);
                        }

                    break;
                case NodeType.SELECTION:
                    var selection = (SerialNodeSelection) node;
                    var nodeSelection = (NodeSelection) ActiveSequenceContainer.gameUsingNodes[this][refID];

                    foreach (var selectionData in selection.selections)
                    foreach (var link in selectionData.cancelLinkIDs)
                    foreach (var sNode in ActiveSequenceContainer.gameUsingNodes[this])
                        if (sNode.Id == link)
                        {
                            var component = nodeSelection.getComponentByID(selectionData.id);
                            component.OutLinkNodes.Add(sNode);
                            sNode.registerNewCancelLink(nodeSelection);
                        }

                    foreach (var selectionData in selection.selections)
                    foreach (var link in selectionData.outLinkIDs)
                    foreach (var sNode in ActiveSequenceContainer.gameUsingNodes[this])
                        if (sNode.Id == link)
                        {
                            var component = nodeSelection.getComponentByID(selectionData.id);
                            component.OutLinkNodes.Add(sNode);
                            sNode.registerNewInLink(nodeSelection);
                        }

                    break;
            }
            refID++;
        }

        //Preinit all Nodes for firing
        ActiveSequenceContainer.gameUsingNodes[this].ForEach(x => x.preInit());

        //Trigger starting node
        SequenceNode[] entryArray = ActiveSequenceContainer.gameUsingNodes[this].Where(x => x.Type == NodeType.ENTRY).ToArray();
        if(entryArray.Length > 0)
            entryArray[0].init();
    }

    private GameObject getObjectFromScene(string file)
    {
        var obj = GameObject.Find(file);

        if (obj != null)
            return obj;

        return null;
    }

    private T getObjectComponentFromScene<T>(string file) where T : Object
    {
        var obj = GameObject.Find(file);
        if (obj != null)
            return obj.GetComponent<T>();

        return null;
    }

    private Component getComponentWithInterface<T>(GameObject obj)
    {
        if (obj == null)
            return null;

        foreach (var component in obj.GetComponents<Component>())
            if (component.GetType().GetInterfaces().Contains(typeof(T)))
                return component;

        return null;
    }

    private float[] convertColorToFloat(Color color)
    {
        return new[] {color.r, color.g, color.b, color.a};
    }

    private Color convertFloatToColor(float[] color)
    {
        return new Color
        {
            r = color[0],
            g = color[1],
            b = color[2],
            a = color[3]
        };
    }

    public override string ToString()
    {
        return GetType().Name + ": " + id;
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (disableCollider)
            return;

        if (collider.tag.Equals("Player"))
            armSequence();
    }

    public void OnTriggerExit(Collider collider)
    {
        if (disableCollider)
            return;

        if (collider.tag.Equals("Player"))
            ActiveSequenceContainer.stopSequence(this);
    }

    private void Start()
    {
        try
        {
            MoralityController.Self.changeListeners.Add(() =>
            {
                disableCollider = true;
                StartCoroutine(triggerReset());
            });
        }
        catch (NullReferenceException)
        {
            Debug.LogError("Failed to Register Listener for Morality Change. There will be errors when changing player states in sequencing.");
        }
    }

    private IEnumerator triggerReset()
    {
        yield return new WaitForSeconds(1);
        disableCollider = false;
    }
}