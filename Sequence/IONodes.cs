﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

/**
 * Reading of the JSON file for node structure
 * Author: Sam Murphy
 */

public static class IONodes
{
    public static List<SerialNode> load(string file)
    {
        var json = "[]";

        try
        {
            using (var readtext = new StreamReader(file))
            {
                json = readtext.ReadLine();
            }
        }
        catch (NullReferenceException)
        {
            return null;
        }
        catch (FileNotFoundException)
        {
            return null;
        }


        var data = JsonConvert.DeserializeObject<List<SerialNode>>(json, new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects
        });

        return data;
    }

    public static string directoryStructureBuilder(Transform obj)
    {
        if (obj.parent != null)
            return directoryStructureBuilder(obj.transform.parent) + "." + obj.name;

        return obj.name;
    }
}