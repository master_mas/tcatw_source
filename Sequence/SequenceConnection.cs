﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

/**
 * Connection between two nodes. Unity Editor Visual
 * Author: Sam Murphy
 */

public class SequenceConnection
{
    public SequenceConnectionPoint inPoint;
    public Action<SequenceConnection> OnClickRemoveConnection;
    public SequenceConnectionPoint outPoint;

    public SequenceConnection(SequenceConnectionPoint inPoint, SequenceConnectionPoint outPoint)
    {
        this.inPoint = inPoint;
        this.outPoint = outPoint;
        OnClickRemoveConnection = CallbackActions.OnClickRemoveConnection;
    }

    public void Draw()
    {
        Handles.DrawBezier(
            inPoint.rect.center,
            outPoint.rect.center,
            inPoint.type == SequenceConnectionPointType.Cancel
                ? inPoint.rect.center
                : inPoint.rect.center + Vector2.left * 50f,
            outPoint.rect.center - Vector2.left * 50f,
            inPoint.type == SequenceConnectionPointType.Cancel
                ? (EditorGUIUtility.isProSkin ? Color.red : new Color(0, 100 / 255.0f, 0))
                : Color.white,
            null,
            2f
        );

        if (inPoint.type == SequenceConnectionPointType.Cancel)
        {
            //Cancel
            Handles.color = Color.red;
            if (Handles.Button((inPoint.rect.center - Vector2.left * 37.5f + outPoint.rect.center) * 0.5f,
                Quaternion.identity, 4, 8, Handles.RectangleCap))
                if (OnClickRemoveConnection != null)
                    OnClickRemoveConnection(this);
            Handles.color = Color.white;
        }
        else
        {
            if (Handles.Button((inPoint.rect.center + outPoint.rect.center) * 0.5f, Quaternion.identity, 4, 8,
                Handles.RectangleCap))
                if (OnClickRemoveConnection != null)
                    OnClickRemoveConnection(this);
        }
    }
}
#endif