﻿using System;
using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialNodeActionComponent
{
    public List<int> cancelLinkIDs = new List<int>();
    public int id;
    public List<int> outLinkIDs = new List<int>();
    public string type;
}