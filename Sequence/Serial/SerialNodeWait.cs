﻿using System;
using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialNodeWait : SerialNode
{
    public List<int> outLinkIDs = new List<int>();
}