﻿/**
 * Author: Sam Murphy
 */

public class SerialNodeListenerComponentPlayerOutsideRadius : SerialNodeListenerComponent
{
    public string objectLocalIdInFile;
    public float radius;
}