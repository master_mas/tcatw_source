﻿/**
 * Author: Sam Murphy
 */

public class SerialNodeActionComponentReplace : SerialNodeActionComponent
{
    public string replaceThis;
    public string replaceWith;
}