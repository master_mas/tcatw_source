﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class SerialNodeActionComponentComplexEnemyWolvesGoHostile : SerialNodeActionComponent
{
    public List<string> humanObjectNames = new List<string>();
    public List<string> wolfObjectNames = new List<string>();
}