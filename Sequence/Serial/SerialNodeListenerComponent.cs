﻿using System;
using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

[Serializable]
public abstract class SerialNodeListenerComponent
{
    public List<int> cancelLinkIDs = new List<int>();
    public int id;
    public float moralityChange;
    public List<int> outLinkIDs = new List<int>();
    public string type;
}