﻿using System;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialNodeTimer : SerialNode
{
    public float maxWaitTime;
}