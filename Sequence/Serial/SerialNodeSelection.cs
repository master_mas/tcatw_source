﻿using System;
using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialNodeSelection : SerialNode
{
    public float entryWeight;
    public List<SerialSelection> selections = new List<SerialSelection>();
    public string title;
}