﻿using System;
using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialSelection
{
    public List<int> cancelLinkIDs = new List<int>();

    public string dialog;
    public int id = -1;

    public List<int> outLinkIDs = new List<int>();
    public float weight;
}