﻿using System;
using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialNode
{
    public bool cancellable;
    public List<int> cancelLinkIDs = new List<int>();
    public int id;
    public float maxWeight;

    public List<int> outLinkIDs = new List<int>();

    public float positionX;
    public float positionY;
    public string type;
    public bool useWeighting;
    public float weight;
}