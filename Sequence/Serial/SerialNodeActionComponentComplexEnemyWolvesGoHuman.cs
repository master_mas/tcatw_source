﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class SerialNodeActionComponentComplexEnemyWolvesGoHuman : SerialNodeActionComponent
{
    public List<string> humanObjectNames = new List<string>();
    public List<string> wolfObjectNames = new List<string>();
}