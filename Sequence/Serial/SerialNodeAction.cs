﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class SerialNodeAction : SerialNode
{
    public List<SerialNodeActionComponent> components = new List<SerialNodeActionComponent>();
}