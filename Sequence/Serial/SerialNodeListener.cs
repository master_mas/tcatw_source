﻿using System.Collections.Generic;

/**
 * Author: Sam Murphy
 */

public class SerialNodeListener : SerialNode
{
    public List<SerialNodeListenerComponent> components = new List<SerialNodeListenerComponent>();
}