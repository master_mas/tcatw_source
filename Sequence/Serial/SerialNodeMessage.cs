﻿using System;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialNodeMessage : SerialNode
{
    public float[] color;
    public string complexEnemyLocalID;
    public string dialog;
    public bool killMessageOnDeath;
    public string objectLocalIdInFile;
    public float waitTime;
}