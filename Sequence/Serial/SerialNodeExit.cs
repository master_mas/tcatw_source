﻿using System;

/**
 * Author: Sam Murphy
 */

[Serializable]
public class SerialNodeExit : SerialNode
{
    public bool disableCollider;
}