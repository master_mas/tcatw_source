﻿/**
 * Author: Sam Murphy
 */

public class SerialNodeTutorialMessage : SerialNode
{
    public string message;
    public float waitTime;
}