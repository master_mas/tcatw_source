﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * Connection point for the node to connect to other nodes. Unity Editor Visual
 * Author: Sam Murphy
 */

public enum SequenceConnectionPointType
{
    In,
    Out,
    Cancel
}

public class SequenceConnectionPoint
{
    private readonly bool useDefault;

    private readonly List<SequenceConnection> connections = new List<SequenceConnection>();
    public EditorNode editorNode;
    public float offset;
    public Action<SequenceConnectionPoint> OnClickConnectionPoint;
    public Rect rect;
    public GUIStyle style;

    public SequenceConnectionPointType type;

    public SequenceConnectionPoint(EditorNode editorNode, SequenceConnectionPointType type, GUIStyle style,
        Action<SequenceConnectionPoint> OnClickConnectionPoint, bool useDefault = true)
    {
        this.editorNode = editorNode;
        this.type = type;
        this.style = style;
        this.OnClickConnectionPoint = OnClickConnectionPoint;
        this.useDefault = useDefault;

        if (type == SequenceConnectionPointType.In && useDefault)
            rect = new Rect(0, 0, 15f, 30f);
        else if (type == SequenceConnectionPointType.In && !useDefault)
            rect = new Rect(0, 0, 10f, 30f);
        else if (type == SequenceConnectionPointType.Out && useDefault)
            rect = new Rect(0, 0, 15f, 30f);
        else if (type == SequenceConnectionPointType.Cancel)
            rect = new Rect(0, 0, 30f, 15f);
        else
            rect = new Rect(0, 0, 15f, 15f);
    }

    public void Draw()
    {
        if (type == SequenceConnectionPointType.Cancel)
        {
            rect.y = editorNode.rect.y - rect.height / 2;
        }
        else
        {
            if (useDefault)
                rect.y = editorNode.rect.y + editorNode.rect.height * 0.5f - rect.height * 0.5f;
            else
                rect.y = editorNode.rect.y + offset + 60f;
        }


        switch (type)
        {
            case SequenceConnectionPointType.In:
                rect.x = editorNode.rect.x - rect.width + 8f;
                break;
            case SequenceConnectionPointType.Out:
                rect.x = editorNode.rect.x + editorNode.rect.width - 8f;
                break;
            case SequenceConnectionPointType.Cancel:
                rect.x = editorNode.rect.x + editorNode.rect.width / 2 - rect.width / 2;
                break;
        }

        if (GUI.Button(rect, "", style))
            if (OnClickConnectionPoint != null)
                OnClickConnectionPoint(this);
    }

    public void changeYPosition(float set)
    {
        offset = set;
    }

    public void addConnection(SequenceConnection connection)
    {
        connections.Add(connection);
    }

    public void removeConnection(SequenceConnection connection)
    {
        connections.Remove(connection);
    }

    public List<SequenceConnection> getConnections()
    {
        return connections;
    }
}
#endif