﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

public class EditorNodeActionComponentInverseActive : EditorNodeActionComponent
{
    public EditorNodeActionComponentInverseActive(EditorNodeAction parent, NodeActionComponent component) : base(parent, component) { }

    public override void draw()
    {
        generateTitleField("Inverse Active State");

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Target Object");

        Target = (GameObject) EditorGUILayout.ObjectField(Target, typeof(GameObject), true);
        GUILayout.EndHorizontal();
    }

    public GameObject Target
    {
        get { return ((NodeActionComponentInverseActive) Component).Target; }
        set { ((NodeActionComponentInverseActive) Component).Target = value; }
    }
}

#endif