﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

namespace Assets.Scripts.Sequence.EditorNodes
{
    public class EditorNodeActionComponentReplace : EditorNodeActionComponent
    {
        public EditorNodeActionComponentReplace(EditorNodeAction parent, NodeActionComponent component) : base(parent,
            component)
        {
        }

        public GameObject ToBeReplace
        {
            get { return ((NodeActionComponentReplace) component).ToBeReplace; }
            set { ((NodeActionComponentReplace) component).ToBeReplace = value; }
        }

        public GameObject Replacer
        {
            get { return ((NodeActionComponentReplace) component).Replacer; }
            set { ((NodeActionComponentReplace) component).Replacer = value; }
        }

        public override void draw()
        {
            generateTitleField("Game Object Replace");

            GUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
            EditorGUILayout.LabelField("To be Replaced");
            ToBeReplace = (GameObject) EditorGUILayout.ObjectField(ToBeReplace, typeof(GameObject), true);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
            EditorGUILayout.LabelField("Replace with");
            Replacer = (GameObject) EditorGUILayout.ObjectField(Replacer, typeof(GameObject), true);
            GUILayout.EndHorizontal();
        }

        public override float getHeight()
        {
            return 54;
        }
    }
}
#endif