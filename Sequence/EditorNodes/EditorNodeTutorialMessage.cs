﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeTutorialMessage : EditorNode
{
    public EditorNodeTutorialMessage(Vector2 position, float width, float height, SequenceNode node) : base(position,
        width, height, node)
    {
        createInPoint();
        createOutPoint();

        rect.height = 215;
    }

    public float WaitTime
    {
        get { return getNode<NodeTutorialMessage>().WaitTime; }
        set { getNode<NodeTutorialMessage>().WaitTime = value; }
    }

    public string Text
    {
        get { return getNode<NodeTutorialMessage>().Text; }
        set { getNode<NodeTutorialMessage>().Text = value; }
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        generateTitleField("Player Message");
        generateWeightField();

        GUILayout.BeginHorizontal();

        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        WaitTime = EditorGUILayout.FloatField("Time to Wait:", WaitTime);
        GUILayout.EndHorizontal();
        Text = GUILayout.TextArea(Text, 1000, GUILayout.MinHeight(87));
        GUILayout.EndArea();
    }
}
#endif