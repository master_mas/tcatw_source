﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentPlayerInput : EditorNodeActionComponent
{
    public EditorNodeActionComponentPlayerInput(EditorNodeAction parent, NodeActionComponent component) : base(parent,
        component)
    {
    }

    public bool AllowInput
    {
        get { return ((NodeActionComponentPlayerInput) component).AllowInput; }
        set { ((NodeActionComponentPlayerInput) component).AllowInput = value; }
    }

    public override void draw()
    {
        generateTitleField("Player Input");
        GUILayout.BeginHorizontal();

        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Allow Input");
        AllowInput = EditorGUILayout.Toggle("", AllowInput);

        GUILayout.EndHorizontal();
    }
}

#endif