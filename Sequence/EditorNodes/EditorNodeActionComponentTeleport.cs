﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentTeleport : EditorNodeActionComponent
{
    public EditorNodeActionComponentTeleport(EditorNodeAction parent, NodeActionComponent component) : base(parent,
        component)
    {
    }

    public Transform GameObject
    {
        get { return ((NodeActionComponentTeleport) component).GameObject; }
        set { ((NodeActionComponentTeleport) component).GameObject = value; }
    }

    public Transform MoveToLocation
    {
        get { return ((NodeActionComponentTeleport) component).MoveToLocation; }
        set { ((NodeActionComponentTeleport) component).MoveToLocation = value; }
    }

    public override void draw()
    {
        generateTitleField("Teleport Action");

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("GameObject");
//        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        GameObject = (Transform) EditorGUILayout.ObjectField(GameObject, typeof(Transform), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Move To");
//        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        MoveToLocation = (Transform) EditorGUILayout.ObjectField(MoveToLocation, typeof(Transform), true);
        GUILayout.EndHorizontal();
    }

    public override float getHeight()
    {
        return 54;
    }
}
#endif