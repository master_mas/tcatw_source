﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentFade : EditorNodeActionComponent
{
    public EditorNodeActionComponentFade(EditorNodeAction parent, NodeActionComponent component) : base(parent,
        component)
    {
    }

    public bool ToBlack
    {
        get { return ((NodeActionComponentFade) component).ToBlack; }
        set { ((NodeActionComponentFade) component).ToBlack = value; }
    }

    public override void draw()
    {
        generateTitleField("Fade");

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Fade to Black");
        ToBlack = EditorGUILayout.Toggle(ToBlack);
        GUILayout.EndHorizontal();
    }
}
#endif