﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentPlayerHitEnemy : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentPlayerHitEnemy(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "Player hit ComplexEnemy")
    {
    }

    public ComplexEnemy NPCReference
    {
        get { return ((NodeListenerComponentPlayerHitEnemy) component).WatchEnemy; }
        set { ((NodeListenerComponentPlayerHitEnemy) component).WatchEnemy = value; }
    }

    public int RequirePass
    {
        get { return ((NodeListenerComponentPlayerHitEnemy) component).RequireHitCountToPass; }
        set { ((NodeListenerComponentPlayerHitEnemy) component).RequireHitCountToPass = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPCReference = (ComplexEnemy) EditorGUILayout.ObjectField(NPCReference, typeof(ComplexEnemy), true);
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUIUtility.fieldWidth = VALUE_FIELD_SPACING;
        RequirePass = EditorGUILayout.IntField("Min Hit", RequirePass);
        GUILayout.EndHorizontal();
    }
}
#endif