﻿#if UNITY_EDITOR

/**
 * Author: Sam Murphy
 */

public class EditorNodeSelectionComponent
{
    public EditorNodeSelectionComponent(SequenceConnectionPoint outPoint, NodeSelectionComponent component)
    {
        OutPoint = outPoint;
        Component = component;
    }

    public string Message
    {
        get { return Component.Message; }
        set { Component.Message = value; }
    }

    public float Weighting
    {
        get { return Component.Weighting; }
        set { Component.Weighting = value; }
    }

    public SequenceConnectionPoint OutPoint { get; set; }

    public NodeSelectionComponent Component { get; set; }
}

#endif