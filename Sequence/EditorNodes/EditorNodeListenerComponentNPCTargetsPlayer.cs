﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentNPCTargetsPlayer : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentNPCTargetsPlayer(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "NPC targets Player")
    {
    }

    public NPC NPCReference
    {
        get { return ((NodeListenerComponentNPCTargetsPlayer) component).WatchNpc; }
        set { ((NodeListenerComponentNPCTargetsPlayer) component).WatchNpc = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPCReference = (NPC) EditorGUILayout.ObjectField(NPCReference, typeof(NPC), true);
        GUILayout.EndHorizontal();
    }
}
#endif