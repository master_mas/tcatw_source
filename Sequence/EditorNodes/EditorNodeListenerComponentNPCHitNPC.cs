﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentNPCHitNPC : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentNPCHitNPC(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "NPC hit NPC")
    {
    }

    public NPC NPCTarget
    {
        get { return ((NodeListenerComponentNPCHitNPC) component).NpcTarget; }
        set { ((NodeListenerComponentNPCHitNPC) component).NpcTarget = value; }
    }

    public NPC NPCAttacker
    {
        get { return ((NodeListenerComponentNPCHitNPC) component).NpcAttacker; }
        set { ((NodeListenerComponentNPCHitNPC) component).NpcAttacker = value; }
    }

    public int RequirePass
    {
        get { return ((NodeListenerComponentNPCHitNPC) component).RequireHitCountToPass; }
        set { ((NodeListenerComponentNPCHitNPC) component).RequireHitCountToPass = value; }
    }

    public override void draw()
    {
        generateTitleField();

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Target");
        NPCTarget = (NPC) EditorGUILayout.ObjectField(NPCTarget, typeof(NPC), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Attacker");
        NPCAttacker = (NPC) EditorGUILayout.ObjectField(NPCAttacker, typeof(NPC), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Min Hit");
        RequirePass = EditorGUILayout.IntField("", RequirePass);
        GUILayout.EndHorizontal();
    }

    public override float getHeight()
    {
        return base.getHeight() + 19 * 2;
    }
}
#endif