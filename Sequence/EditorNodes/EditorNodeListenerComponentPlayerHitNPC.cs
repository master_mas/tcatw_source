﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentPlayerHitNPC : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentPlayerHitNPC(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "Player hit NPC")
    {
    }

    public NPC NPCReference
    {
        get { return ((NodeListenerComponentPlayerHitNPC) component).WatchNpc; }
        set { ((NodeListenerComponentPlayerHitNPC) component).WatchNpc = value; }
    }

    public int RequirePass
    {
        get { return ((NodeListenerComponentPlayerHitNPC) component).RequireHitCountToPass; }
        set { ((NodeListenerComponentPlayerHitNPC) component).RequireHitCountToPass = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPCReference = (NPC) EditorGUILayout.ObjectField(NPCReference, typeof(NPC), true);
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUIUtility.fieldWidth = VALUE_FIELD_SPACING;
        RequirePass = EditorGUILayout.IntField("Min Hit", RequirePass);
        GUILayout.EndHorizontal();
    }
}
#endif