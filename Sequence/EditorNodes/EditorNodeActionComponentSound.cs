﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentSound : EditorNodeActionComponent
{
    public EditorNodeActionComponentSound(EditorNodeAction parent, NodeActionComponent component) : base(parent,
        component)
    {
    }

    public SoundPlayer SoundPlayer
    {
        get { return ((NodeActionComponentSound) component).SoundPlayer; }
        set { ((NodeActionComponentSound) component).SoundPlayer = value; }
    }

    public override void draw()
    {
        generateTitleField("Sound Action");
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Sound Player");
        SoundPlayer = (SoundPlayer) EditorGUILayout.ObjectField(SoundPlayer, typeof(SoundPlayer), true);

        GUILayout.EndHorizontal();
    }
}
#endif