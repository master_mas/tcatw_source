﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListener : EditorNode
{
    private readonly List<EditorNodeListenerComponent> components = new List<EditorNodeListenerComponent>();

    private readonly string[] types =
    {
        "Player Within Radius of Gameobject",
        "Player Outside Radius of Gameobject",
        "Player Hits NPC",
        "Player Hits ComplexEnemy",
        "Player Hits Object",
        "Player Killed NPC",
        "Player Killed ComplexEnemy",
        "NPC Targets Player",
        "NPC Hits Player",
        "NPC Hits NPC"
    };

    public EditorNodeListener(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        inPoint = createInPoint();

        rect.height = calculateHeight();
    }

    public List<EditorNodeListenerComponent> Components
    {
        get { return components; }
    }

    private int SelectionIndex
    {
        get { return getNode<NodeListener>().SelectionIndex; }
        set { getNode<NodeListener>().SelectionIndex = value; }
    }

    public void addListenerComponent(EditorNodeListenerComponent component)
    {
        components.Add(component);
        rect.height = calculateHeight();
    }

    public void removeListenerComponent(EditorNodeListenerComponent component)
    {
        components.Remove(component);
        outPoints.Remove(component.OutPoint);
        CallbackActions.OnClickRemoveSubConnection(component.OutPoint);
        rect.height = calculateHeight();
    }

    private float calculateHeight()
    {
        float total = 146;

        components.ForEach(x => total += x.getHeight() + Styles.DEFAULT_COMPLEX_NODE_COMPONENT_SPACING);

        return total;
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);
        generateTitleField("Listener");
        generateWeightField();

        SelectionIndex = EditorGUILayout.Popup("Listener Type to add", SelectionIndex, types);

        if (GUILayout.Button("Add Listener (" + types[SelectionIndex] + ")"))
        {
            SequenceConnectionPoint point = null;
            NodeListenerComponent component = null;
            switch (SelectionIndex)
            {
                case 0:
                    addListenerComponent(new EditorNodeListenerComponentPlayerWithinRadius(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentPlayerWithinRadius(getNode<NodeListener>(), -1, 0)));
                    break;
                case 1:
                    addListenerComponent(new EditorNodeListenerComponentPlayerOutsideRadius(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentPlayerOutsideOfRadius(getNode<NodeListener>(), -1, 0)));
                    break;
                case 2:
                    addListenerComponent(new EditorNodeListenerComponentPlayerHitNPC(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentPlayerHitNPC(getNode<NodeListener>(), -1, 0)));
                    break;
                case 3:
                    addListenerComponent(new EditorNodeListenerComponentPlayerHitEnemy(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentPlayerHitEnemy(getNode<NodeListener>(), -1, 0)));
                    break;
                case 4:
                    addListenerComponent(new EditorNodeListenerComponentPlayerHitObject(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentPlayerHitObject(getNode<NodeListener>(), -1, 0)));
                    break;
                case 5:
                    addListenerComponent(new EditorNodeListenerComponentPlayerKilledNPC(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentPlayerKilledNPC(getNode<NodeListener>(), -1, 0)));
                    break;
                case 6:
                    addListenerComponent(new EditorNodeListenerComponentPlayerKilledEnemy(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentPlayerKilledEnemy(getNode<NodeListener>(), -1, 0)));
                    break;
                case 7:
                    addListenerComponent(new EditorNodeListenerComponentNPCTargetsPlayer(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentNPCTargetsPlayer(getNode<NodeListener>(), -1, 0)));
                    break;
                case 8:
                    addListenerComponent(new EditorNodeListenerComponentNPCHitPlayer(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentNPCHitPlayer(getNode<NodeListener>(), -1, 0)));
                    break;
                case 9:
                    addListenerComponent(new EditorNodeListenerComponentNPCHitNPC(
                        this,
                        point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                            CallbackActions.OnClickOutPoint, false),
                        component = new NodeListenerComponentNPCHitNPC(getNode<NodeListener>(), -1, 0)));
                    break;
            }

            outPoints.Add(point);
            getNode<NodeListener>().addComponent(component);
        }

        float componentHeight = 0;
        components.ForEach(x =>
        {
            x.OutPoint.changeYPosition(72 + componentHeight);
            componentHeight += x.getHeight() + Styles.DEFAULT_COMPLEX_NODE_COMPONENT_SPACING;
            x.draw();
            GUILayout.Space(Styles.DEFAULT_COMPLEX_NODE_COMPONENT_SPACING);
        });

        GUILayout.EndArea();
    }
}
#endif