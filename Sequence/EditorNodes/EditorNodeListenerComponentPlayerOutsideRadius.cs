﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentPlayerOutsideRadius : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentPlayerOutsideRadius(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "Game Object outside Radius")
    {
    }

    public Transform WatchObject
    {
        get { return ((NodeListenerComponentPlayerOutsideOfRadius) component).WatchObject; }
        set { ((NodeListenerComponentPlayerOutsideOfRadius) component).WatchObject = value; }
    }

    public float Radius
    {
        get { return ((NodeListenerComponentPlayerOutsideOfRadius) component).Radius; }
        set { ((NodeListenerComponentPlayerOutsideOfRadius) component).Radius = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        WatchObject = (Transform) EditorGUILayout.ObjectField(WatchObject, typeof(Transform), true);
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUIUtility.fieldWidth = VALUE_FIELD_SPACING;
        Radius = EditorGUILayout.FloatField("Radius", Radius);
        GUILayout.EndHorizontal();
    }
}
#endif