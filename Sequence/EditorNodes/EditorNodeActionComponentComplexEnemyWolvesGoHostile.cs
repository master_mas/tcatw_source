﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentComplexEnemyWolvesGoHostile : EditorNodeActionComponent
{
    public EditorNodeActionComponentComplexEnemyWolvesGoHostile(EditorNodeAction parent, NodeActionComponent component)
        : base(parent, component)
    {
    }

    public List<ComplexEnemy> Wolves
    {
        get { return ((NodeActionComponentComplexEnemyWolvesGoHostile) component).Wolves; }
    }

    public List<ComplexEnemy> Humans
    {
        get { return ((NodeActionComponentComplexEnemyWolvesGoHostile) component).Humans; }
    }

    public override float getHeight()
    {
        return Wolves.Count * 19 + Humans.Count * 19 + base.getHeight() + 18;
    }

    public override void draw()
    {
        generateTitleField("Wolves go Hostile");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Complex ComplexEnemy - Wolf"))
        {
            Wolves.Add(null);
            parent.externalCalculateHeight();
        }
        GUILayout.EndHorizontal();

        for (var i = 0; i < Wolves.Count; ++i)
        {
            GUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
            EditorGUILayout.LabelField("Complex ComplexEnemy - Wolf");
            Wolves[i] = (ComplexEnemy) EditorGUILayout.ObjectField(Wolves[i], typeof(ComplexEnemy), true);

            if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.MaxHeight(15)))
            {
                Wolves.RemoveAt(i);
                parent.externalCalculateHeight();
            }

            GUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Complex ComplexEnemy - Human"))
        {
            Humans.Add(null);
            parent.externalCalculateHeight();
        }
        GUILayout.EndHorizontal();

        for (var i = 0; i < Humans.Count; ++i)
        {
            GUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
            EditorGUILayout.LabelField("Complex ComplexEnemy - Human");
            Humans[i] = (ComplexEnemy) EditorGUILayout.ObjectField(Humans[i], typeof(ComplexEnemy), true);

            if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.MaxHeight(15)))
            {
                Humans.RemoveAt(i);
                parent.externalCalculateHeight();
            }

            GUILayout.EndHorizontal();
        }
    }
}
#endif