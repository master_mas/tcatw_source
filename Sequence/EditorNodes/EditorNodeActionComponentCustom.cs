﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentCustom : EditorNodeActionComponent
{
    public EditorNodeActionComponentCustom(EditorNodeAction parent, NodeActionComponent component) : base(parent,
        component)
    {
    }

    public GameObject UnityComponent
    {
        get
        {
            return ((NodeActionComponentCustom) component).WatchObject != null
                ? ((NodeActionComponentCustom) component).WatchObject.gameObject
                : null;
        }
        set
        {
            var obj = value;

            if (obj == null)
            {
                ((NodeActionComponentCustom) component).WatchObject = null;
                return;
            }

            foreach (var component in obj.GetComponents<Component>())
                if (component.GetType().GetInterfaces().Contains(typeof(NodeActionComponentCustomExecutor)))
                    ((NodeActionComponentCustom) this.component).WatchObject = component;
        }
    }

    public override void draw()
    {
        generateTitleField("Custom Action");
        GUILayout.BeginHorizontal();

        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Executor");
        UnityComponent = (GameObject) EditorGUILayout.ObjectField(UnityComponent, typeof(GameObject), true);

        GUILayout.EndHorizontal();
    }
}
#endif