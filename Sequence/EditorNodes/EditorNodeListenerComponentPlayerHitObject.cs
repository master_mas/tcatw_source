﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentPlayerHitObject : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentPlayerHitObject(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "Player hit Object")
    {
    }

    public GameObject NPCReference
    {
        get { return ((NodeListenerComponentPlayerHitObject) component).WatchObject; }
        set { ((NodeListenerComponentPlayerHitObject) component).WatchObject = value; }
    }

    public int RequirePass
    {
        get { return ((NodeListenerComponentPlayerHitObject) component).RequireHitCountToPass; }
        set { ((NodeListenerComponentPlayerHitObject) component).RequireHitCountToPass = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPCReference = (GameObject) EditorGUILayout.ObjectField(NPCReference, typeof(GameObject), true);
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUIUtility.fieldWidth = VALUE_FIELD_SPACING;
        RequirePass = EditorGUILayout.IntField("Min Hit", RequirePass);
        GUILayout.EndHorizontal();
    }
}
#endif