﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeMessage : EditorNode
{
    public EditorNodeMessage(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        createInPoint();
        createOutPoint();

        rect.height = 271;
    }

    public float WaitTime
    {
        get { return getNode<NodeMessage>().WaitTime; }
        set { getNode<NodeMessage>().WaitTime = value; }
    }

    public string Text
    {
        get { return getNode<NodeMessage>().Text; }
        set { getNode<NodeMessage>().Text = value; }
    }

    public Color Color
    {
        get { return getNode<NodeMessage>().Color; }
        set { getNode<NodeMessage>().Color = value; }
    }

    public bool KillMessageOnDeath
    {
        get { return getNode<NodeMessage>().KillMessgaeOnDeath; }
        set { getNode<NodeMessage>().KillMessgaeOnDeath = value; }
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        generateTitleField("Non Playable Entity Dialogue");
        generateWeightField();

        GUILayout.BeginHorizontal();

        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        WaitTime = EditorGUILayout.FloatField("Time to Wait:", WaitTime);
        GUILayout.EndHorizontal();

        if (getNode<NodeMessage>().ComplexEnemyReference != null)
            GUI.enabled = false;
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        getNode<NodeMessage>().NpcReference =
            (NPC) EditorGUILayout.ObjectField("NPC", getNode<NodeMessage>().NpcReference, typeof(NPC), true);
        GUILayout.EndHorizontal();
        GUI.enabled = true;

        if (getNode<NodeMessage>().NpcReference != null)
            GUI.enabled = false;
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        getNode<NodeMessage>().ComplexEnemyReference = (ComplexEnemy) EditorGUILayout.ObjectField(
            "Complex ComplexEnemy", getNode<NodeMessage>().ComplexEnemyReference, typeof(ComplexEnemy), true);
        GUILayout.EndHorizontal();
        GUI.enabled = true;

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        getNode<NodeMessage>().KillMessgaeOnDeath =
            EditorGUILayout.Toggle("Death Message", getNode<NodeMessage>().KillMessgaeOnDeath);
        GUILayout.EndHorizontal();

        //        GUILayout.BeginHorizontal();
        //        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        //        EditorGUILayout.LabelField("Text Colour");
        //        Color = EditorGUILayout.ColorField(Color);;
        //        GUILayout.EndHorizontal();

        Text = GUILayout.TextArea(Text, 1000, GUILayout.MinHeight(87));
        GUILayout.EndArea();
    }
}
#endif