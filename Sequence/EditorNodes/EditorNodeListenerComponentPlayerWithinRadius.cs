﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentPlayerWithinRadius : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentPlayerWithinRadius(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "Game Object within Radius")
    {
    }

    public Transform WatchObject
    {
        get { return ((NodeListenerComponentPlayerWithinRadius) component).WatchObject; }
        set { ((NodeListenerComponentPlayerWithinRadius) component).WatchObject = value; }
    }

    public float Radius
    {
        get { return ((NodeListenerComponentPlayerWithinRadius) component).Radius; }
        set { ((NodeListenerComponentPlayerWithinRadius) component).Radius = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        WatchObject = (Transform) EditorGUILayout.ObjectField(WatchObject, typeof(Transform), true);
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUIUtility.fieldWidth = VALUE_FIELD_SPACING;
        Radius = EditorGUILayout.FloatField("Radius", Radius);
        GUILayout.EndHorizontal();
    }
}
#endif