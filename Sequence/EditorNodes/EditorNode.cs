﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public abstract class EditorNode
{
    public static GUIStyle titleSkin;
    protected SequenceConnectionPoint cancelPoint;
    public GUIStyle defaultNodeStyle;

    protected SequenceConnectionPoint inPoint;
    public bool isDragged;
    public bool isSelected;

    private SequenceNode node;

    private bool oldState;

    public Action<EditorNode> OnRemoveNode;
    protected List<SequenceConnectionPoint> outPoints = new List<SequenceConnectionPoint>();
    public Rect rect;
    public GUIStyle selectedNodeStyle;

    public GUIStyle style;

    protected string title = "";

    protected EditorNode(Vector2 position, float width, float height, SequenceNode node)
    {
        this.node = node;
        rect = new Rect(position.x, position.y, width, height);

        style = Styles.node;
        defaultNodeStyle = style;
        selectedNodeStyle = Styles.nodeSelected;
        OnRemoveNode = CallbackActions.OnClickRemoveNode;

        if (titleSkin == null)
            try
            {
                titleSkin = new GUIStyle(GUI.skin.textField)
                {
                    alignment = TextAnchor.MiddleCenter
                };
            }
            catch (ArgumentException e)
            {
                Debug.LogError(e);
            }

        createCancelPoint();
    }

    public SequenceConnectionPoint InPoint
    {
        get { return inPoint; }
        set { inPoint = value; }
    }

    public List<SequenceConnectionPoint> OutPoints
    {
        get { return outPoints; }
        set { outPoints = value; }
    }

    public int Id
    {
        get { return node.Id; }
    }

    public NodeType Type
    {
        get { return getNode<SequenceNode>().Type; }
    }

    public float Weighting
    {
        get { return node.Weighting; }
        set { node.Weighting = value; }
    }

    public float MaxWeighting
    {
        get { return node.maxWeighting; }
        set { node.maxWeighting = value; }
    }

    public bool UseWeighting
    {
        get { return node.UseWeighting; }
        set { node.UseWeighting = value; }
    }

    public bool Cancellable
    {
        get { return node.Cancellable; }
        set { node.Cancellable = value; }
    }

    public SequenceConnectionPoint CancelPoint
    {
        get { return cancelPoint; }
    }

    public void Drag(Vector2 delta)
    {
        rect.position += delta;
    }

    public virtual void Draw()
    {
        outPoints.ForEach(x => { x.Draw(); });
        if (inPoint != null)
            inPoint.Draw();

        if (Cancellable)
            cancelPoint.Draw();
    }

    public bool ProcessEvents(Event e)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 0)
                    if (rect.Contains(e.mousePosition))
                    {
                        isDragged = true;
                        GUI.changed = true;
                        isSelected = true;
                        style = selectedNodeStyle;
                    }
                    else
                    {
                        GUI.changed = true;
                        isSelected = false;
                        style = defaultNodeStyle;
                    }

                if (e.button == 1 && isSelected && rect.Contains(e.mousePosition))
                {
                    ProcessContextMenu();
                    e.Use();
                }
                break;

            case EventType.MouseUp:
                isDragged = false;
                break;

            case EventType.MouseDrag:
                if (e.button == 0 && isDragged)
                {
                    Drag(e.delta);
                    e.Use();
                    return true;
                }
                break;
        }

        return false;
    }

    private void ProcessContextMenu()
    {
        var genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Remove " + title + " Node"), false, OnClickRemoveNode);
        genericMenu.ShowAsContext();
    }

    private void OnClickRemoveNode()
    {
        if (OnRemoveNode != null)
            OnRemoveNode(this);
    }

    public SequenceConnectionPoint createConnectionPoint(SequenceConnectionPointType type)
    {
        GUIStyle style = null;

        switch (type)
        {
            case SequenceConnectionPointType.In:
                style = Styles.inPoint;
                break;
            case SequenceConnectionPointType.Cancel:
                style = Styles.cancelPoint;
                break;
            case SequenceConnectionPointType.Out:
                style = Styles.outPoint;
                break;
        }

        return new SequenceConnectionPoint(this, type, style,
            type == SequenceConnectionPointType.Out ? CallbackActions.OnClickOutPoint : CallbackActions.OnClickInPoint);
    }

    protected SequenceConnectionPoint createCancelPoint()
    {
        var point = createConnectionPoint(SequenceConnectionPointType.Cancel);
        cancelPoint = point;
        return point;
    }

    protected SequenceConnectionPoint createInPoint()
    {
        var point = createConnectionPoint(SequenceConnectionPointType.In);
        inPoint = point;
        return point;
    }

    protected SequenceConnectionPoint createOutPoint()
    {
        var point = createConnectionPoint(SequenceConnectionPointType.Out);
        outPoints.Add(point);
        return point;
    }

    protected virtual bool internalUpdate()
    {
        return false;
    }

    public bool updateNode()
    {
        var uiChange = internalUpdate();

        if (oldState != node.Active)
        {
            if (node.Active)
            {
                defaultNodeStyle = Styles.nodeActive;
                selectedNodeStyle = Styles.nodeActiveSelected;
            }
            else
            {
                defaultNodeStyle = Styles.node;
                selectedNodeStyle = Styles.nodeSelected;
            }

            style = defaultNodeStyle;

            oldState = node.Active;

            return true;
        }

        return uiChange;
    }

    public T getNode<T>() where T : SequenceNode
    {
        try
        {
            return (T)node;
        }
        catch (InvalidCastException)
        {
            Debug.LogError(node.GetType().Name + " is being cased to " + typeof(T).Name);
            throw;
        }
    }

    public void setNode(SequenceNode node)
    {
        this.node = node;
    }

    protected void generateWeightField()
    {
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING - 1;
        UseWeighting = EditorGUILayout.Toggle("Enabled Morality", UseWeighting);
        GUILayout.EndHorizontal();

        if (!UseWeighting)
            GUI.enabled = false;

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        Weighting = EditorGUILayout.FloatField("Minimum Entry:", Weighting);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        MaxWeighting = EditorGUILayout.FloatField("Maximum Entry:", MaxWeighting);
        GUILayout.EndHorizontal();

        if (!UseWeighting)
            GUI.enabled = true;
    }

    protected void generateTitleField(string title)
    {
        this.title = title;
        GUI.enabled = false;
        GUILayout.TextField(title + (ActiveSequenceContainer.editorUsingContainer.debugMode ? " : " + Id : ""),
            titleSkin);
        GUI.enabled = true;
    }
}
#endif