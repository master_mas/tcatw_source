﻿#if UNITY_EDITOR
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public abstract class EditorNodeListenerComponent
{
    protected static float OBJECT_FIELD_SPACING = 150f;
    protected static float LABEL_FIELD_SPACING = 50f;
    protected static float VALUE_FIELD_SPACING = 40f;
    protected NodeListenerComponent component;
    protected EditorNodeListener parent;

    public EditorNodeListenerComponent(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component, string name)
    {
        OutPoint = outPoint;
        this.component = component;
        this.parent = parent;
        Name = name;
    }

    public SequenceConnectionPoint OutPoint { get; set; }

    public NodeListenerComponent Component
    {
        get { return component; }
        set { component = value; }
    }

    public string Name { get; private set; }

    protected GUIStyle Center
    {
        get { return EditorNode.titleSkin; }
    }

    public abstract void draw();

    public virtual float getHeight()
    {
        return 36;
    }

    protected void generateRemoveButton()
    {
        if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.MaxHeight(15)))
            parent.removeListenerComponent(this);
    }

    protected void generateTitleField()
    {
        GUILayout.BeginHorizontal();
        GUI.enabled = false;
        GUILayout.TextField(Name + (ActiveSequenceContainer.editorUsingContainer.debugMode ? " : " + component.Id : ""),
            EditorNode.titleSkin);
        GUI.enabled = true;
        generateRemoveButton();
        GUILayout.EndHorizontal();
    }
}
#endif