﻿#if UNITY_EDITOR
using UnityEngine;

public class EditorNodeRandom : EditorNode
{
    public EditorNodeRandom(Vector2 position, float width, float height, SequenceNode node) : base(position, width, height, node)
    {
        rect.height = 54;
        createInPoint();
        createOutPoint();
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        generateTitleField("Random");

        GUILayout.EndArea();
    }
}

#endif