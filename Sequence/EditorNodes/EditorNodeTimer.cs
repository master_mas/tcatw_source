﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeTimer : EditorNode
{
    public EditorNodeTimer(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        createInPoint();
        createOutPoint();

        rect.height = 147;
    }

    protected override bool internalUpdate()
    {
        return true;
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        generateTitleField("Timer");
        generateWeightField();
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        getNode<NodeTimer>().TotalWaitTime =
            EditorGUILayout.FloatField("Wait Time: ", getNode<NodeTimer>().TotalWaitTime);
        GUILayout.EndHorizontal();
        generateTitleField("Current Wait Time: " + getNode<NodeTimer>().Current + " seconds");
        title = "Timer";

        GUILayout.Space(10);

        GUILayout.EndArea();
    }
}

#endif