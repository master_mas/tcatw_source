﻿#if UNITY_EDITOR
using System;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class EditorNodeActionComponentChangeAmbientSound : EditorNodeActionComponent
{
    public EditorNodeActionComponentChangeAmbientSound(EditorNodeAction parent, NodeActionComponent component) : base(parent, component)
    {
    }

    public override void draw()
    {
        generateTitleField("Change Ambient Sound");

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Music");

        MusicState = Enum.GetValues(typeof(MusicManager.MusicState)).Cast<MusicManager.MusicState>().ToArray()[
            EditorGUILayout.Popup(Array.IndexOf(Enum.GetValues(typeof(MusicManager.MusicState)), MusicState),
                Enum.GetNames(typeof(MusicManager.MusicState)))];
        GUILayout.EndHorizontal();
    }

    public MusicManager.MusicState MusicState
    {
        get { return ((NodeActionComponentChangeAmbientSound) Component).MusicState; }
        set { ((NodeActionComponentChangeAmbientSound) Component).MusicState = value; }
    }
}
#endif