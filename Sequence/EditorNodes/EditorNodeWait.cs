﻿#if UNITY_EDITOR

using System;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeWait : EditorNode
{
    public EditorNodeWait(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        createOutPoint();
        createInPoint();
    }

    protected override bool internalUpdate()
    {
        if (getNode<NodeWait>().lastTime != -1)
        {
            float now = DateTime.UtcNow.Second;
            getNode<NodeWait>().timer += now - getNode<NodeWait>().lastTime;
            getNode<NodeWait>().lastTime = now;

            return true;
        }

        return false;
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        var titleSkin = new GUIStyle(GUI.skin.textField)
        {
            alignment = TextAnchor.MiddleCenter
        };
        GUI.enabled = false;
        GUILayout.TextField("Wait", titleSkin);
        GUILayout.TextField("Current Wait Time: " + getNode<NodeWait>().timer + " seconds", titleSkin);
        GUI.enabled = true;

        GUILayout.Space(10);

        GUILayout.EndArea();
    }
}

#endif