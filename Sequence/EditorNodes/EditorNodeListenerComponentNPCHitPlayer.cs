﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentNPCHitPlayer : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentNPCHitPlayer(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "NPC hit Player")
    {
    }

    public NPC NPCReference
    {
        get { return ((NodeListenerComponentNPCHitPlayer) component).WatchNpc; }
        set { ((NodeListenerComponentNPCHitPlayer) component).WatchNpc = value; }
    }

    public int RequirePass
    {
        get { return ((NodeListenerComponentNPCHitPlayer) component).RequireHitCountToPass; }
        set { ((NodeListenerComponentNPCHitPlayer) component).RequireHitCountToPass = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPCReference = (NPC) EditorGUILayout.ObjectField(NPCReference, typeof(NPC), true);
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUIUtility.fieldWidth = VALUE_FIELD_SPACING;
        RequirePass = EditorGUILayout.IntField("Min Hit", RequirePass);
        GUILayout.EndHorizontal();
    }
}
#endif