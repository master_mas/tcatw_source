﻿#if UNITY_EDITOR
using System.Collections.Generic;
using Assets.Scripts.Sequence.EditorNodes;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeAction : EditorNode
{
    private readonly List<EditorNodeActionComponent> components = new List<EditorNodeActionComponent>();

    private readonly string[] types =
    {
        "Custom", "Particle System", "Sound", "Teleport", "NPC Run Away", "Replace",
        "Complex ComplexEnemy Fight Complex ComplexEnemy", "Wolves go Hostile", "Humans go Hostile", "Player Input",
        "Fade", "Trigger Animations", "Change Ambient Sound", "Inverse Active State"
    };

    public EditorNodeAction(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        inPoint = createInPoint();

        rect.height = calculateHeight();
    }

    public List<EditorNodeActionComponent> Components
    {
        get { return components; }
    }

    private int SelectionIndex
    {
        get { return getNode<NodeAction>().SelectionIndex; }
        set { getNode<NodeAction>().SelectionIndex = value; }
    }

    public void addActionComponent(EditorNodeActionComponent component)
    {
        components.Add(component);
        rect.height = calculateHeight();
    }

    public void removeActionComponent(EditorNodeActionComponent component)
    {
        components.Remove(component);
        rect.height = calculateHeight();
    }

    public void externalCalculateHeight()
    {
        rect.height = calculateHeight();
    }

    private float calculateHeight()
    {
        float total = 146;

        components.ForEach(x => total += x.getHeight() + Styles.DEFAULT_COMPLEX_NODE_COMPONENT_SPACING);

        return total;
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);
        generateTitleField("Action");
        generateWeightField();

        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        SelectionIndex = EditorGUILayout.Popup("Action Type to add", SelectionIndex, types);

        if (GUILayout.Button("Add Action (" + types[SelectionIndex] + ")"))
        {
            NodeActionComponent component = null;
            switch (SelectionIndex)
            {
                case 0:
                    addActionComponent(new EditorNodeActionComponentCustom(
                        this,
                        component = new NodeActionComponentCustom(getNode<NodeAction>(), -1)));
                    break;
                case 1:
                    addActionComponent(new EditorNodeActionComponentParticleSystem(
                        this,
                        component = new NodeActionComponentParticleSystem(getNode<NodeAction>(), -1)));
                    break;
                case 2:
                    addActionComponent(new EditorNodeActionComponentSound(
                        this,
                        component = new NodeActionComponentSound(getNode<NodeAction>(), -1)));
                    break;
                case 3:
                    addActionComponent(new EditorNodeActionComponentTeleport(
                        this,
                        component = new NodeActionComponentTeleport(getNode<NodeAction>(), -1)));
                    break;
                case 4:
                    addActionComponent(new EditorNodeActionComponentNPCRunAway(
                        this,
                        component = new NodeActionComponentNPCRunAway(getNode<NodeAction>(), -1)));
                    break;
                case 5:
                    addActionComponent(new EditorNodeActionComponentReplace(
                        this,
                        component = new NodeActionComponentReplace(getNode<NodeAction>(), -1)));
                    break;
                case 6:
                    addActionComponent(new EditorNodeActionComponentComplexEnemyFight(
                        this,
                        component = new NodeActionComponentComplexEnemyFight(getNode<NodeAction>(), -1)));
                    break;
                case 7:
                    addActionComponent(new EditorNodeActionComponentComplexEnemyWolvesGoHostile(
                        this,
                        component = new NodeActionComponentComplexEnemyWolvesGoHostile(getNode<NodeAction>(), -1)));
                    break;
                case 8:
                    addActionComponent(new EditorNodeActionComponentComplexEnemyWolvesGoHuman(
                        this,
                        component = new NodeActionComponentComplexEnemyWolvesGoHuman(getNode<NodeAction>(), -1)));
                    break;
                case 9:
                    addActionComponent(new EditorNodeActionComponentPlayerInput(
                        this,
                        component = new NodeActionComponentPlayerInput(getNode<NodeAction>(), -1)));
                    break;
                case 10:
                    addActionComponent(new EditorNodeActionComponentFade(
                        this,
                        component = new NodeActionComponentFade(getNode<NodeAction>(), -1)));
                    break;
                case 11:
                    addActionComponent(new EditorNodeActionComponentTriggerAnimations(
                        this,
                        component = new NodeActionComponentTriggerAnimations(getNode<NodeAction>(), -1)));
                    break;
                case 12:
                    addActionComponent(new EditorNodeActionComponentChangeAmbientSound(
                        this, 
                        component = new NodeActionComponentChangeAmbientSound(getNode<NodeAction>(), -1)));
                    break;
                case 13:
                    addActionComponent(new EditorNodeActionComponentInverseActive(
                        this,
                        component = new NodeActionComponentInverseActive(getNode<NodeAction>(), -1)));
                    break;
            }

            getNode<NodeAction>().addComponent(component);
        }

        components.ForEach(x =>
        {
            x.draw();
            GUILayout.Space(Styles.DEFAULT_COMPLEX_NODE_COMPONENT_SPACING);
        });

        GUILayout.EndArea();
    }
}
#endif