﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentPlayerKilledNPC : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentPlayerKilledNPC(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "Player killed NPC")
    {
    }

    public NPC NPCReference
    {
        get { return ((NodeListenerComponentPlayerKilledNPC) component).WatchNpc; }
        set { ((NodeListenerComponentPlayerKilledNPC) component).WatchNpc = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPCReference = (NPC) EditorGUILayout.ObjectField(NPCReference, typeof(NPC), true);
        GUILayout.EndHorizontal();
    }
}
#endif