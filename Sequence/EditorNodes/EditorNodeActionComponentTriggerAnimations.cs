﻿#if UNITY_EDITOR
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentTriggerAnimations : EditorNodeActionComponent
{
    public EditorNodeActionComponentTriggerAnimations(EditorNodeAction parent, NodeActionComponent component) : base(
        parent, component)
    {
    }

    public NPCGestures Gestures
    {
        get { return ((NodeActionComponentTriggerAnimations) component).Gestures; }
        set { ((NodeActionComponentTriggerAnimations) component).Gestures = value; }
    }

    public NPC Npc
    {
        get { return ((NodeActionComponentTriggerAnimations) component).Npc; }
        set { ((NodeActionComponentTriggerAnimations) component).Npc = value; }
    }

    public ComplexEnemy ComplexEnemy
    {
        get { return ((NodeActionComponentTriggerAnimations) component).ComplexEnemy; }
        set { ((NodeActionComponentTriggerAnimations) component).ComplexEnemy = value; }
    }

    public NPCGestures.GestureType GestureType
    {
        get { return ((NodeActionComponentTriggerAnimations) component).GestureType; }
        set { ((NodeActionComponentTriggerAnimations) component).GestureType = value; }
    }

    public override float getHeight()
    {
//        return 72;
        return 54;
    }

    public override void draw()
    {
        generateTitleField("Trigger Animations");

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Gesture Control");
        Gestures = (NPCGestures) EditorGUILayout.ObjectField(Gestures, typeof(NPCGestures), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Gesture to Use");

        GestureType = Enum.GetValues(typeof(NPCGestures.GestureType)).Cast<NPCGestures.GestureType>().ToArray()[
            EditorGUILayout.Popup(Array.IndexOf(Enum.GetValues(typeof(NPCGestures.GestureType)), GestureType),
                Enum.GetNames(typeof(NPCGestures.GestureType)))];
        GUILayout.EndHorizontal();

        //Animation Controller
    }
}
#endif