﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeListenerComponentPlayerKilledEnemy : EditorNodeListenerComponent
{
    public EditorNodeListenerComponentPlayerKilledEnemy(EditorNodeListener parent, SequenceConnectionPoint outPoint,
        NodeListenerComponent component) : base(parent, outPoint, component, "Player killed ComplexEnemy")
    {
    }

    public ComplexEnemy NPCReference
    {
        get { return ((NodeListenerComponentPlayerKilledEnemy) component).WatchEnemy; }
        set { ((NodeListenerComponentPlayerKilledEnemy) component).WatchEnemy = value; }
    }

    public override void draw()
    {
        generateTitleField();
        GUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPCReference = (ComplexEnemy) EditorGUILayout.ObjectField(NPCReference, typeof(ComplexEnemy), true);
        GUILayout.EndHorizontal();
    }
}
#endif