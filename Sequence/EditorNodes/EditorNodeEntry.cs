﻿#if UNITY_EDITOR
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeEntry : EditorNode
{
    public EditorNodeEntry(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        createOutPoint();
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        var titleSkin = new GUIStyle(GUI.skin.textField)
        {
            alignment = TextAnchor.MiddleCenter
        };
        GUI.enabled = false;
        GUILayout.TextField("Entry", titleSkin);
        GUI.enabled = true;

        GUILayout.EndArea();
    }
}
#endif