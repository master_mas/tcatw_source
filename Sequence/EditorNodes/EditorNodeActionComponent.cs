﻿#if UNITY_EDITOR
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public abstract class EditorNodeActionComponent
{
    protected NodeActionComponent component;
    protected EditorNodeAction parent;

    protected static float OBJECT_FIELD_SPACING = 150f;
    protected static float LABEL_FIELD_SPACING = 50f;

    public EditorNodeActionComponent(EditorNodeAction parent, NodeActionComponent component)
    {
        this.component = component;
        this.parent = parent;
    }

    public NodeActionComponent Component
    {
        get { return component; }
        set { component = value; }
    }

    public abstract void draw();

    public virtual float getHeight()
    {
        return 36;
    }

    protected void generateRemoveButton()
    {
        if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.MaxHeight(15)))
            parent.removeActionComponent(this);
    }

    protected void generateTitleField(string title)
    {
        GUILayout.BeginHorizontal();
        GUI.enabled = false;
        GUILayout.TextField(title + (ActiveSequenceContainer.editorUsingContainer.debugMode ? " : " + component.Id : ""), EditorNode.titleSkin);
        GUI.enabled = true;
        generateRemoveButton();
        GUILayout.EndHorizontal();
    }
}
#endif
