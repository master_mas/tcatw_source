﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentParticleSystem : EditorNodeActionComponent
{
    public EditorNodeActionComponentParticleSystem(EditorNodeAction parent, NodeActionComponent component) : base(
        parent, component)
    {
    }

    public ParticleSystem ParticleSystem
    {
        get { return ((NodeActionComponentParticleSystem) component).ParticleSystem; }
        set { ((NodeActionComponentParticleSystem) component).ParticleSystem = value; }
    }

    public override void draw()
    {
        generateTitleField("Particle System Action");
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Particle System");
        ParticleSystem = (ParticleSystem) EditorGUILayout.ObjectField(ParticleSystem, typeof(ParticleSystem), true);

        GUILayout.EndHorizontal();
    }
}
#endif