﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeSelection : EditorNode
{
    public SequenceConnectionPoint inPoint;
    public List<EditorNodeSelectionComponent> messageComponents = new List<EditorNodeSelectionComponent>(3);

    public EditorNodeSelection(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        inPoint = createInPoint();
        calcuateHeight();
    }

    public void addNewMessageComponent(EditorNodeSelectionComponent component)
    {
        messageComponents.Add(component);
        calcuateHeight();
    }

    public void removeMessageComponent(EditorNodeSelectionComponent component)
    {
        messageComponents.Remove(component);
        calcuateHeight();
        CallbackActions.OnClickRemoveSubConnection(component.OutPoint);
        outPoints.Remove(component.OutPoint);
    }

    private void calcuateHeight()
    {
        var fixedAmount = messageComponents.Count >= 3 ? 112 : 133;
        rect.height = fixedAmount + 18 * messageComponents.Count;

        modMessagePositions();
    }

    public void modMessagePositions()
    {
        var fixedAmount = messageComponents.Count >= 3 ? 36 : 55;
        for (var i = 0; i < messageComponents.Count; i++)
            messageComponents[i].OutPoint.changeYPosition(fixedAmount + 18 * i);
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        generateTitleField("Dialogue Selection");
        generateWeightField();

        if (messageComponents.Count < 3 && GUILayout.Button("Add Selection"))
        {
            SequenceConnectionPoint point;
            NodeSelectionComponent component;
            addNewMessageComponent(
                new EditorNodeSelectionComponent(
                    point = new SequenceConnectionPoint(this, SequenceConnectionPointType.Out, Styles.outPoint,
                        CallbackActions.OnClickOutPoint, false),
                    component = new NodeSelectionComponent("Default", 0, -1)));
            outPoints.Add(point);
            getNode<NodeSelection>().addComponent(component);
        }

        for (var index = 0; index < messageComponents.Count; index++)
        {
            var selectionComponent = messageComponents[index];
            GUILayout.BeginHorizontal();

            selectionComponent.Message = GUILayout.TextField(selectionComponent.Message,
                GUILayout.Width((rect.width - defaultNodeStyle.padding.left - defaultNodeStyle.padding.right) * 0.68f));

            EditorGUIUtility.labelWidth = 50f;
            EditorGUIUtility.fieldWidth = 20f;
            selectionComponent.Weighting = EditorGUILayout.FloatField("Weight: ", selectionComponent.Weighting,
                GUILayout.Width((rect.width - defaultNodeStyle.padding.left - defaultNodeStyle.padding.right) * 0.24f));

            if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.MaxHeight(15)))
                removeMessageComponent(messageComponents[index]);

            GUILayout.EndHorizontal();
//            GUILayout.Space(Styles.DEFAULT_COMPLEX_NODE_COMPONENT_SPACING);
        }

        GUILayout.EndArea();
    }
}
#endif