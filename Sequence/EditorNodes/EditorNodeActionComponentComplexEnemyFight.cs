﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentComplexEnemyFight : EditorNodeActionComponent
{
    public EditorNodeActionComponentComplexEnemyFight(EditorNodeAction parent, NodeActionComponent component) : base(
        parent, component)
    {
    }

    public List<ComplexEnemy> ComplexEnemies
    {
        get { return ((NodeActionComponentComplexEnemyFight) component).ComplexEnemies; }
    }

    public override float getHeight()
    {
        return ComplexEnemies.Count * 18 + base.getHeight();
    }

    public override void draw()
    {
        generateTitleField("Complex Enemy Fight Complex Enemy");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Complex Enemy"))
        {
            ComplexEnemies.Add(null);
            parent.externalCalculateHeight();
        }
        GUILayout.EndHorizontal();

        for (var i = 0; i < ComplexEnemies.Count; ++i)
        {
            GUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
            EditorGUILayout.LabelField("Complex Enemy");
            ComplexEnemies[i] =
                (ComplexEnemy) EditorGUILayout.ObjectField(ComplexEnemies[i], typeof(ComplexEnemy), true);

            if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.MaxHeight(15)))
            {
                ComplexEnemies.RemoveAt(i);
                parent.externalCalculateHeight();
            }

            GUILayout.EndHorizontal();
        }
    }
}
#endif