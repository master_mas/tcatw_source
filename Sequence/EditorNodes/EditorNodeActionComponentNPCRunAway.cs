﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeActionComponentNPCRunAway : EditorNodeActionComponent
{
    public EditorNodeActionComponentNPCRunAway(EditorNodeAction parent, NodeActionComponent component) : base(parent,
        component)
    {
    }

    public NPC NPC
    {
        get { return ((NodeActionComponentNPCRunAway) component).Npc; }
        set { ((NodeActionComponentNPCRunAway) component).Npc = value; }
    }

    public Transform MoveToLocation
    {
        get { return ((NodeActionComponentNPCRunAway) component).Location; }
        set { ((NodeActionComponentNPCRunAway) component).Location = value; }
    }

    public override void draw()
    {
        generateTitleField("NPC Run Away Action");

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("NPC");
//        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        NPC = (NPC) EditorGUILayout.ObjectField(NPC, typeof(NPC), true);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = LABEL_FIELD_SPACING;
        EditorGUILayout.LabelField("Run Away To");
//        EditorGUIUtility.fieldWidth = OBJECT_FIELD_SPACING;
        MoveToLocation = (Transform) EditorGUILayout.ObjectField(MoveToLocation, typeof(Transform), true);
        GUILayout.EndHorizontal();
    }

    public override float getHeight()
    {
        return 54;
    }
}
#endif