﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeMoralityChange : EditorNode
{
    public EditorNodeMoralityChange(Vector2 position, float width, float height, SequenceNode node) : base(position,
        width, height, node)
    {
        createInPoint();
        createOutPoint();

        rect.height = 128;
    }

    public float Amount
    {
        get { return getNode<NodeMoralityChange>().Change; }
        set { getNode<NodeMoralityChange>().Change = value; }
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        generateTitleField("Morality Change");
        generateWeightField();

        GUILayout.BeginHorizontal();

        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING;
        Amount = EditorGUILayout.FloatField("Amount", Amount);

        GUILayout.EndHorizontal();

        GUILayout.EndArea();
    }
}
#endif