﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class EditorNodeExit : EditorNode
{
    public EditorNodeExit(Vector2 position, float width, float height, SequenceNode node) : base(position, width,
        height, node)
    {
        createInPoint();

        rect.height = 68;
    }

    public bool DisableCollider
    {
        get { return getNode<NodeExit>().DisableCollider; }
        set { getNode<NodeExit>().DisableCollider = value; }
    }

    public override void Draw()
    {
        base.Draw();

        GUILayout.BeginArea(rect, "", style);
        GUILayout.Space(10);

        var titleSkin = new GUIStyle(GUI.skin.textField);
        titleSkin.alignment = TextAnchor.MiddleCenter;
        GUI.enabled = false;
        GUILayout.TextField("Exit", titleSkin);
        GUI.enabled = true;

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = Styles.DEFAULT_MAIN_SPACE_SPACING - 1;
        DisableCollider = EditorGUILayout.Toggle("Disable Collider", DisableCollider);
        GUILayout.EndHorizontal();

        GUILayout.EndArea();
    }
}
#endif