﻿using System;
using System.Collections.Generic;

/**
 * Bridge Script for Unity Engine to Unity Editor
 * Author: Sam Murphy
 */

[Serializable]
public static class ActiveSequenceContainer
{
    public static SequenceContainer editorUsingContainer = null;
    public static List<SequenceContainer> gameUsingContainer = new List<SequenceContainer>();

    public static List<SequenceNode> editorUsingNodes = new List<SequenceNode>();
    public static Dictionary<SequenceContainer, List<SequenceNode>> gameUsingNodes = new Dictionary<SequenceContainer, List<SequenceNode>>();

    public static Dictionary<SequenceContainer, List<SequenceNode>> armedNodes = new Dictionary<SequenceContainer, List<SequenceNode>>();

    public static void addToGameNodes(SequenceNode node, SequenceContainer container)
    {
        foreach (var sequenceNode in gameUsingNodes[container])
            if (sequenceNode.Id == node.Id)
                return;

        gameUsingNodes[container].Add(node);
    }

    public static void stopSequence(SequenceContainer container)
    {
        if (armedNodes.ContainsKey(container))
        {
            armedNodes[container].ForEach(x => x.destroy());

            armedNodes.Remove(container);
            gameUsingContainer.Remove(container);
            gameUsingNodes.Remove(container);
        }
    }

    public static bool checkIfEditorIDExists(int id)
    {
        foreach (var sequenceNode in editorUsingNodes)
            if (sequenceNode.Id == id)
                return true;

        return false;
    }
}