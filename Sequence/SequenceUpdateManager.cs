﻿using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * Centralised method for updating all Active Nodes.
 * Uses Singleton Pattern
 * Author: Sam Murphy
 */

public class SequenceUpdateManager : MonoBehaviour
{
    private static SequenceUpdateManager self;

    private void Awake()
    {
        if (self != null)
            enabled = false;

        self = this;
    }

    private void Update()
    {
        //Update all active Sequence Container Nodes
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.timer(Time.deltaTime));
        }
        catch (MissingReferenceException) { }
    }

    private void OnDrawGizmos()
    {
        //Draw all active Sequence Container Nodes Gizmos
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.GizmoDraw());
        }
        catch (MissingReferenceException) { }
    }
}