﻿using System.Collections.Generic;
using UnityEngine;

/**
 * Used as a passthrough from Entity Collision Management to here. Will pass actions to all nodes.
 * Author: Sam Murphy
 */

public static class SequenceListener
{
    public static void PlayerHitNPC(NPC npc)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.PlayerHitNPC(npc));
        }
        catch (MissingReferenceException) { }
    }

    public static void PlayerHitEnemy(GameObject enemy)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.PlayerHitEnemy(enemy));
        }
        catch (MissingReferenceException) { }
    }

    public static void PlayerKilledEnemy(GameObject enemy)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.PlayerKilledEnemy(enemy));
        }
        catch (MissingReferenceException) { }
    }

    public static void PlayerHitObject(GameObject obj)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.PlayerHitObject(obj));
        }
        catch (MissingReferenceException) { }
    }

    public static void PlayerKilledNPC(GameObject npc)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.PlayerKilledNPC(npc));
        }
        catch (MissingReferenceException) { }
    }

    public static void NPCHitPlayer(GameObject npc)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.NPCHitPlayer(npc));
        }
        catch (MissingReferenceException) { }
    }

    public static void NPCTargetsPlayer(GameObject npc)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.NPCTargetsPlayer(npc));
        }
        catch (MissingReferenceException) { }
    }

    public static void NPCHitsNPC(GameObject npcTarget, GameObject npcAttacker)
    {
        try
        {
            foreach (KeyValuePair<SequenceContainer, List<SequenceNode>> entry in ActiveSequenceContainer.armedNodes)
                entry.Value.ForEach(x => x.NPCHitsNPC(npcTarget, npcAttacker));
        }
        catch (MissingReferenceException) { }
    }
}