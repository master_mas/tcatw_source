﻿#if UNITY_EDITOR
using System;

/**
 * When in Node Editor clicking on a connection point, these are the passthrough methods for this.
 * Author: Sam Murphy
 */

public class CallbackActions
{
    public static Action<EditorNode> OnClickRemoveNode = null;

    public static Action<SequenceConnectionPoint> OnClickInPoint = null;
    public static Action<SequenceConnectionPoint> OnClickOutPoint = null;
    public static Action<SequenceConnectionPoint> OnClickRemoveSubConnection = null;

    public static Action<SequenceConnection> OnClickRemoveConnection = null;
}
#endif