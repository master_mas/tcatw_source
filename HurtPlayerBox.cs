﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayerBox : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == PlayerController.instance.gameObject)
        {
            PlayerController.instance.Hurt();
        }
    }
}
