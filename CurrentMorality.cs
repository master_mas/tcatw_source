﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentMorality : MonoBehaviour
{
    public Text text;
	
	void Update ()
	{
        if(text != null)
	        text.text = "Current Morality: " + Mathf.Round(MoralityController.Self.Weight);
	}
}
