﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    public int health = 2;

    Animator anim;
    [SerializeField]
    GameObject rootBone;
    Rigidbody[] rbs;
    public Collider largeBodyCollider;
    NavMeshAgent nav;
    public bool dead = false;
    public MeleeWeaponTrail trail;
    public EnemyWeapon weapon;
    public enum State
    {
        Idle,
        Follow, 
        Attack,
        Dead
    }

    public State myState = State.Idle;
	// Use this for initialization
	void Start () {
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        rbs = rootBone.GetComponentsInChildren<Rigidbody>();
    }

    void Update ()
    {
        switch (myState)
        {
            case State.Idle:
                if (Vector3.Distance(transform.position, PlayerController.instance.transform.position) < 10)
                {
                    RaycastHit hit;

                    if (Physics.Raycast(transform.position + Vector3.up,
                        ( PlayerController.instance.transform.position + Vector3.up) - (transform.position + Vector3.up),
                        out hit, 
                        10))
                    {
                        if (hit.collider.gameObject.CompareTag("Player"))
                        {
                            myState = State.Follow;
                        }
                    }
                }
            break;
            case State.Follow:
                nav.SetDestination(PlayerController.instance.transform.position);
                anim.SetFloat("Forward", nav.desiredVelocity.magnitude);
                if (Vector3.Distance(transform.position, PlayerController.instance.transform.position) < 2)
                {
                    if (anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
                    {
                        myState = State.Attack;
                        StartCoroutine(Attack());
                    }
                }
                break;
            case State.Attack:
                nav.SetDestination(transform.position);
                break;
            case State.Dead:

                break;
            default:
                break;
        }

    }
    IEnumerator Attack ()
    {
        nav.SetDestination(transform.position);
        nav.updateRotation = false;
        anim.SetFloat("Forward", 0);
        transform.LookAt(new Vector3(PlayerController.instance.transform.position.x, 0, PlayerController.instance.transform.position.z));
        yield return new WaitForSeconds(0.5f);
        if (Random.value < 0.5f)
        {
            anim.SetTrigger("Attack1");
        }
        else
        {
            anim.SetTrigger("Attack2");
        }
        yield return new WaitForSeconds(0.3f);
        while (!anim.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
        {
            yield return null;
        }
        nav.updateRotation = true;
        myState = State.Follow;
    }

    IEnumerator pause ()
    {
        float timer = 0;
        Time.timeScale = 0.001f;

        while (timer < 0.1f)
        {
            timer += Time.unscaledDeltaTime;
            yield return null;
        }
        Time.timeScale = 1f;
    }

    IEnumerator ragdollInSeconds ()
    {
        yield return new WaitForSeconds(Random.Range(0,0.2f));

        anim.enabled = false;
        foreach (Rigidbody item in rbs)
        {
            item.isKinematic = false;
            item.AddExplosionForce(
                100, 
                new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)),
                3);
        }
        this.enabled = false;
    }

    public void Ragdoll ()
    {
        largeBodyCollider.enabled = false;
        StartCoroutine(ragdollInSeconds());
    }
    public void Hurt()
    {
        if (!dead && !invulnerable)
        {
            WeaponOff();

            health--;
            StartCoroutine(pause());
            if (health >= 1)
            {
                anim.SetTrigger("Hurt");
            }
            else
            {
                Die();
            }
            MakeInvulnerable();
        }
    }

    bool invulnerable;
    float invulnerableTimer = 0;
    IEnumerator InvulnerableTimer()
    {
        while (invulnerableTimer > 0)
        {
            invulnerable = true;
            invulnerableTimer -= Time.deltaTime;
            yield return null;
        }
        invulnerable = false;
    }

    void MakeInvulnerable()
    {
        invulnerable = true;
        invulnerableTimer = 0.1f;
        StartCoroutine(InvulnerableTimer());
    }

    void Die ()
    {
        largeBodyCollider.enabled = false;
        //nav.enabled = false;
        anim.Play("Die");
        dead = true;
        myState = State.Dead;
    }

    public void WeaponOn ()
    {
        weapon.on = true;
        trail.Emit = true;
    }

    public void WeaponOff ()
    {
        weapon.on = false;
        trail.Emit = false;
    }
}
