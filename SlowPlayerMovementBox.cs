﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowPlayerMovementBox : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        //GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().isTrigger = true;
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController temp = other.GetComponent<PlayerController>();
            if (other.gameObject == PlayerManager.GetActivePlayer().gameObject)
            {
                PlayerManager.GetActivePlayer().SlowPlayerMovement();
            }
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
