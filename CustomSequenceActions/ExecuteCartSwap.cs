﻿using System.Collections.Generic;
using UnityEngine;

/**
 * Author: Sam Murphy
 */

public class ExecuteCartSwap : MonoBehaviour, NodeActionComponentCustomExecutor
{
    public Transform spawnLocation;

    public List<GameObject> theObjectToBeReplaced;
    public List<GameObject> toReplaceTheObject;

    public void execute(bool hi)
    {
        theObjectToBeReplaced.ForEach(x => x.gameObject.SetActive(false));

        toReplaceTheObject.ForEach(x => Instantiate(x, spawnLocation));
    }
}