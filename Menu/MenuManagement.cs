﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManagement : MonoBehaviour
{
    public MenuBase currentMenu;
    public Image fadeToBlack;

    public void Start()
    {
        ShowMenu(currentMenu);
    }

    public void ShowMenu(MenuBase menu)
    {
        if (currentMenu != null)
            currentMenu.IsOpen = false;

        currentMenu = menu;
        currentMenu.IsOpen = true;
    }
    public void LoadLevel(string levelName)
    {
        StartCoroutine(LoadLevelInSeconds(levelName, 1.5f));
    }

    public void QuitGame ()
    {
        Application.Quit();
    }

    IEnumerator LoadLevelInSeconds (string _levelName, float seconds)
    {
        Color col = new Color(0, 0, 0, 0);
        float counter = 0;
        while (counter < seconds)
        {
            counter += Time.deltaTime;
            col.a = counter;
            fadeToBlack.color = col;
            yield return null;
        }

        SceneManager.LoadScene(_levelName);
    }
}
