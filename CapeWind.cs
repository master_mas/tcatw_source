﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapeWind : MonoBehaviour {
    Cloth cloth;
    public float windMultiplier=1;
	// Use this for initialization
	void Start () {
        cloth = GetComponent<Cloth>();
	}
	
	// Update is called once per frame
	void Update () {
		if (WindManager.instance != null)
        {
            cloth.externalAcceleration = WindManager.GetCurrentWind() * windMultiplier;
        }
	}
}
