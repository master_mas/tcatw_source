﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreditsFlow : MonoBehaviour
{
    public static CreditsFlow instance;

    private List<Creator> creators = new List<Creator>();
    private List<Creator.CharacterStorageContainer> present = new List<Creator.CharacterStorageContainer>();

    public float waitTime = 3;
    public float currentWaitTime = 0;

    private bool move = true;
    private bool swap = false;
    private int upTo = 0;

    private bool disable = true;

    public bool Disable
    {
        get { return disable; }
        set
        {
            disable = value;
            present.ForEach(x => x.disable = value);
        }
    }

    public Text presentText;

    private void Awake()
    {
        instance = this;

        string firstLine = "The Child and The Wolf Team";

        foreach (var c in firstLine)
            present.Add(new Creator.CharacterStorageContainer(new []{Color.black, Color.grey}, 60, c));

        presentText.text = Creator.convertListToString(present);

        currentWaitTime = waitTime / 2;
    }

    public void register(Creator creator)
    {
        creators.Add(creator);
        creators.Sort((c1, c2) => c1.order.CompareTo(c2.order));
    }

    private void Update()
    {
        if (disable)
            return;

        present.ForEach(x => x.update(Time.deltaTime));
        presentText.text = Creator.convertListToString(present);

        currentWaitTime += Time.deltaTime;

        if (currentWaitTime >= waitTime)
        {
            if (upTo > 0 && upTo < creators.Count + 1)
            {
                creators[upTo - 1].FadeIn = false;
                creators[upTo - 1].Disable = false;
            }

            if (upTo < creators.Count)
            {
                creators[upTo].FadeIn = true;
                creators[upTo].Disable = false;
            }

            if (upTo == creators.Count - 1)
            {
                present.ForEach(x => x.Reverse = true);
                present.ForEach(x => x.disable = false);
            }

            if (upTo >= creators.Count + 2)
            {
                SceneManager.LoadScene("Menu", LoadSceneMode.Single);
            }

            upTo++;

            currentWaitTime = 0;
        }
    }
}