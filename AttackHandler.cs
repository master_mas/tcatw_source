﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHandler : MonoBehaviour {

    ComplexEnemy enemy;

    void Start()
    {
        enemy = transform.GetComponentInParent<ComplexEnemy>();
    }

    private void Update()
    {
        
    }

    public void AttackStartLeft()
    {
        enemy.AttackStartLeft();
    }

    public void AttackEndLeft()
    {
        enemy.AttackEndLeft();
    }

    public void AttackStartRight()
    {
        enemy.AttackStartRight();
    }

    public void AttackEndRight()
    {
        enemy.AttackEndRight();
    }

    public void AttackOn()
    {
        enemy.AttackStartLeft();
    }

    public void AttackOff()
    {
        enemy.AttackEndLeft();
    }
}
