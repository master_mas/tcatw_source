﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MoralityController))]
[CanEditMultipleObjects]
public class MoralityChange : Editor
{
    private float modAmount = 0;

    public override void OnInspectorGUI()
    {
        MoralityController controller = (MoralityController) target;

        GUILayout.Space(10);
        GUI.enabled = false;
        controller.Weight = EditorGUILayout.FloatField("Current: ", controller.Weight);
        GUI.enabled = true;
        GUILayout.Space(10);

        controller.moralityChangeOverTime = EditorGUILayout.Toggle("Change Morality", controller.moralityChangeOverTime);
        controller.moralityPerSecond = EditorGUILayout.FloatField("Morality Per Second", controller.moralityPerSecond);
        controller.moralityMinMax.x = EditorGUILayout.FloatField("Morality Minimum", controller.moralityMinMax.x);
        controller.moralityMinMax.y = EditorGUILayout.FloatField("Morality Maximum", controller.moralityMinMax.y);

        if (GUILayout.Button("Add 1 to Morality"))
        {
            controller.Weight += 1;
        }

        GUILayout.Space(5);

        if (GUILayout.Button("Minus 1 from Morality"))
        {
            controller.Weight -= 1;
        }
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        modAmount = EditorGUILayout.FloatField(modAmount);
        if (GUILayout.Button("Change"))
        {
            controller.Weight += modAmount;
        }
        GUILayout.EndHorizontal();

        EditorUtility.SetDirty(controller);
    }
}
#endif
