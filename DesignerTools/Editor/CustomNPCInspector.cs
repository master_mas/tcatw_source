﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NPC))]
[CanEditMultipleObjects]
public class CustomNPCInspector : Editor {

    NPC npc;
    SerializedProperty waypoints;
    GameObject waypoint;
    GameObject holder;
    bool showPosition = true;
    bool lastKnownPosition;
    NPCCreator npcCreator;

    NPC.Type  previousState;

    private void OnEnable()
    {
        npc = (NPC)target;
        holder = GameObject.Find("WaypointHolder");
        waypoints = serializedObject.FindProperty("waypointPath");
        waypoint = (GameObject)Resources.Load("Waypoint");
        showPosition = lastKnownPosition;

        npcCreator = npc.GetComponent<NPCCreator>();
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        previousState = npc.enemyType;

        npc.health = EditorGUILayout.FloatField("Health", npc.health);
        npc.maxHealth = EditorGUILayout.FloatField("MaxHealth", npc.maxHealth);
        npc.rotationSmooth = EditorGUILayout.FloatField("Attack Damage", npc.rotationSmooth);

        npc.canBeHurt = EditorGUILayout.Toggle("Can Be Hurt", npc.canBeHurt);

        npc.enemyType = (NPC.Type)EditorGUILayout.EnumPopup("Type", npc.enemyType);
        npc.currentState = (NPC.State)EditorGUILayout.EnumPopup("State", npc.currentState);
        if (npc.currentState == NPC.State.Path)
        {
            npc.pathType = (NPC.PathChoice)EditorGUILayout.EnumPopup("Path Type", npc.pathType);
            if (GUILayout.Button("Add Waypoint"))
            {
                GameObject tempwaypoint = Instantiate(waypoint);
                tempwaypoint.name = npc.name + "_waypoint_" + npc.waypointPath.Count;
                if (holder == null)
                {
                    holder = new GameObject("WaypointHolder");
                }
                tempwaypoint.transform.SetParent(holder.transform);
                tempwaypoint.transform.position = npc.transform.position + Vector3.up * 0.2f;

                npc.waypointPath.Add(tempwaypoint.transform);
                showPosition = true;
            }

            if (npc.waypointPath.Count > 0)
            {
                if (npc.waypointPath[0] == null)
                {
                    npc.waypointPath.Clear();
                }

                waypoints.isExpanded = EditorGUILayout.Foldout(waypoints.isExpanded, "Waypoint Path", true);
                
                if (waypoints.isExpanded)
                {
                    GUILayout.Label("Size: " + npc.waypointPath.Count);
                    for (int i = 0; i < npc.waypointPath.Count; i++)
                    {
                        GUILayout.BeginHorizontal();

                        GUILayout.Label("Element " + i);
                        bool allowSceneObjects = !EditorUtility.IsPersistent(target);
                        EditorGUILayout.ObjectField(npc.waypointPath[i], typeof(Transform), allowSceneObjects);
                        if (GUILayout.Button("X"))
                        {
                            GameObject toRemove = npc.waypointPath[i].gameObject;
                            npc.waypointPath.RemoveAt(i);
                            DestroyImmediate(toRemove);
                            for (int j = i; j < npc.waypointPath.Count; j++)
                            {
                                npc.waypointPath[j].name = npc.name + "_waypoint_" + j;
                            }
                            return;
                        }
                        GUILayout.EndHorizontal();
                    }
                }
            }
        }

        if (GUI.changed)
        {
            if (previousState != npc.enemyType)
            {
                SwapCharacter(npc.enemyType);
            }
        }

        serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(npc);
    }

    /// <summary>
    /// Disables all characters and enables selected one
    /// </summary>
    /// <param name="type"></param>
    void SwapCharacter(NPC.Type type)
    {
        npc.wolf.SetActive(false);
        npc.human.SetActive(false);
        npc.whiteWolf.SetActive(false);
        npc.woodCutter.SetActive(false);
        switch (type)
        {
            default:
                break;
            case NPC.Type.Human:
                npc.human.SetActive(true);
                npcCreator.hideFlags = HideFlags.None;
                break;
            case NPC.Type.Wolf:
                npc.wolf.SetActive(true);
                npcCreator.hideFlags = HideFlags.HideInInspector;
                break;
            case NPC.Type.WoodCutter:
                npc.woodCutter.SetActive(true);
                break;
            case NPC.Type.WhiteWolf:
                npc.whiteWolf.SetActive(true);
                npcCreator.hideFlags = HideFlags.HideInInspector;
                break;
        }
    }
}
