﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NPCCreator))]
[CanEditMultipleObjects]
public class CustomNPCCreatorInspector : Editor {

    NPCCreator db;

    void OnEnable()
    {
        db = (NPCCreator)target;
    }

    public override void OnInspectorGUI()
    {
        SetupNPC();
    }

    public void SetupNPC()
    {
        serializedObject.Update();

        db.gender = (NPCCreator.Gender)EditorGUILayout.EnumPopup("Gender", db.gender);
        db.skinColour = (NPCCreator.SkinColour)EditorGUILayout.EnumPopup("Skin Colour", db.skinColour);
        db.hairColour = (NPCCreator.HairColour)EditorGUILayout.EnumPopup("Hair Colour", db.hairColour);
        if (db.gender == NPCCreator.Gender.MALE)
        {
            db.maleHair = (NPCCreator.MaleHair)EditorGUILayout.EnumPopup("Hair Type", db.maleHair);
            db.maleClothes = (NPCCreator.MaleClothes)EditorGUILayout.EnumPopup("Clothes", db.maleClothes);
        }
        else
        {
            db.femaleHair = (NPCCreator.FemaleHair)EditorGUILayout.EnumPopup("Hair Type", db.femaleHair);
            db.femaleClothes = (NPCCreator.FemaleClothes)EditorGUILayout.EnumPopup("Clothes", db.femaleClothes);
        }

        db.weapon = (NPCCreator.Weapons)EditorGUILayout.EnumPopup("Weapon", db.weapon);

        if (GUI.changed)
        {
            TurnOnSelectedItems();
        }

        if (GUILayout.Button("Generate NPC"))
        {
            TurnOnSelectedItems();
        }


        EditorUtility.SetDirty(db);
    }

    /// <summary>
    /// Turns of all gameobjects
    /// </summary>
    void TurnOffAllItems()
    {
        db.male.SetActive(false);
        db.female.SetActive(false);
        db.malePonytail.SetActive(false);
        db.maleShort.SetActive(false);
        db.bun.SetActive(false);
        db.femalePonytail.SetActive(false);
        db.pigtails.SetActive(false);

        for (int i = 0; i < db.weapons.Count; i++)
        {
            db.weapons[i].SetActive(false);
        }
    }

    /// <summary>
    /// Returns value based on gender given
    /// </summary>
    /// <param name="gender">Selected gender</param>
    int SetSkin(NPCCreator.Gender gender)
    {
        if (gender == NPCCreator.Gender.FEMALE)
        {
            return 4;
        }
        return 3;
    }

    /// <summary>
    /// Activates all selected items
    /// </summary>
    public void TurnOnSelectedItems()
    {
        TurnOffAllItems();

        int skinClothesNum = 0;
        int hairColour = 0;

        switch (db.skinColour)
        {
            case NPCCreator.SkinColour.DARK:
                skinClothesNum += 0;
                break;
            case NPCCreator.SkinColour.PALE:
                skinClothesNum += SetSkin(db.gender);
                break;
            case NPCCreator.SkinColour.TAN:
                skinClothesNum += 2 * SetSkin(db.gender);
                break;
            default:
                break;
        }

        switch (db.gender)
        {
            case NPCCreator.Gender.MALE:

                db.male.SetActive(true);

                switch (db.maleHair)
                {
                    case NPCCreator.MaleHair.BUZZCUT:
                        hairColour = -1;
                        break;
                    case NPCCreator.MaleHair.PONYTAIL:
                        db.malePonytail.SetActive(true);
                        db.hairMaterial = db.malePonytail;
                        hairColour = 4;
                        break;
                    case NPCCreator.MaleHair.SHORT:
                        db.maleShort.SetActive(true);
                        db.hairMaterial = db.maleShort;
                        hairColour = 0;
                        break;
                    default:
                        break;
                }

                switch (db.maleClothes)
                {
                    case NPCCreator.MaleClothes.BROWN:
                        skinClothesNum += 1;
                        break;
                    case NPCCreator.MaleClothes.BLUE:
                        skinClothesNum += 0;
                        break;
                    case NPCCreator.MaleClothes.GREEN:
                        skinClothesNum += 2;
                        break;
                    default:
                        break;
                }

                if (hairColour != -1)
                {
                    switch (db.hairColour)
                    {
                        case NPCCreator.HairColour.BLONDE:
                            break;
                        case NPCCreator.HairColour.BRUNETTE:
                            hairColour += 1;
                            break;
                        case NPCCreator.HairColour.BLACK:
                            hairColour += 2;
                            break;
                        case NPCCreator.HairColour.GINGER:
                            hairColour += 3;
                            break;
                        default:
                            break;
                    }
                    db.hairMaterial.GetComponentInChildren<Renderer>().material = db.maleHairMaterials[hairColour];
                }

                if (db.weapons.Count > 0)
                {
                    switch (db.weapon)
                    {
                        case NPCCreator.Weapons.HOE:
                            db.weapons[3].SetActive(true);
                            break;
                        case NPCCreator.Weapons.PITCH_FORK:
                            db.weapons[4].SetActive(true);
                            break;
                        case NPCCreator.Weapons.SICKLE:
                            db.weapons[5].SetActive(true);
                            break;
                        default:
                            break;
                    }
                }

                db.maleSkinClothesMaterial.GetComponent<Renderer>().material = db.skinClothes[skinClothesNum];

                break;
            case NPCCreator.Gender.FEMALE:

                skinClothesNum += 9;

                db.female.SetActive(true);

                bool doPigTailsAsWell = false;

                switch (db.femaleHair)
                {
                    case NPCCreator.FemaleHair.BUN:
                        db.bun.SetActive(true);
                        db.hairMaterial = db.bun;
                        hairColour += 4;
                        break;
                    case NPCCreator.FemaleHair.PONYTAIL:
                        db.femalePonytail.SetActive(true);
                        db.hairMaterial = db.femalePonytail;
                        hairColour += 8;
                        break;
                    case NPCCreator.FemaleHair.PIGTAILS:
                        db.pigtails.SetActive(true);
                        db.hairMaterial = db.pigtails;
                        hairColour += 12;
                        break;
                    case NPCCreator.FemaleHair.PIGTAILS_WITH_BUN:
                        db.bun.SetActive(true);
                        db.hairMaterial = db.bun;
                        hairColour += 4;
                        doPigTailsAsWell = true;
                        break;
                    default:
                        break;
                }

                switch (db.femaleClothes)
                {
                    case NPCCreator.FemaleClothes.BROWN:
                        break;
                    case NPCCreator.FemaleClothes.BURGURDY:
                        skinClothesNum += 1;
                        break;
                    case NPCCreator.FemaleClothes.GREEN:
                        skinClothesNum += 2;
                        break;
                    case NPCCreator.FemaleClothes.PURPLE:
                        skinClothesNum += 3;
                        break;
                    default:
                        break;
                }


                switch (db.hairColour)
                {
                    case NPCCreator.HairColour.BLONDE:
                        db.femaleHairBase.GetComponentInChildren<Renderer>().material = db.femaleHairMaterials[0];
                        break;
                    case NPCCreator.HairColour.BRUNETTE:
                        db.femaleHairBase.GetComponentInChildren<Renderer>().material = db.femaleHairMaterials[1];
                        hairColour += 1;
                        break;
                    case NPCCreator.HairColour.BLACK:
                        db.femaleHairBase.GetComponentInChildren<Renderer>().material = db.femaleHairMaterials[2];
                        hairColour += 2;
                        break;
                    case NPCCreator.HairColour.GINGER:
                        db.femaleHairBase.GetComponentInChildren<Renderer>().material = db.femaleHairMaterials[3];
                        hairColour += 3;
                        break;
                    default:
                        break;
                }

                db.hairMaterial.GetComponentInChildren<Renderer>().material = db.femaleHairMaterials[hairColour];
                db.femaleSkinClothesMaterial.GetComponent<Renderer>().material = db.skinClothes[skinClothesNum];
                db.skirtMaterial.GetComponentInChildren<Renderer>().material = db.skinClothes[skinClothesNum];

                if (doPigTailsAsWell)
                {
                    db.pigtails.SetActive(true);
                    db.hairMaterial = db.pigtails;
                    hairColour += 8;
                    db.hairMaterial.GetComponentInChildren<Renderer>().material = db.femaleHairMaterials[hairColour];
                }

                if (db.weapons.Count > 0)
                {
                    switch (db.weapon)
                    {
                        case NPCCreator.Weapons.HOE:
                            db.weapons[0].SetActive(true);
                            break;
                        case NPCCreator.Weapons.PITCH_FORK:
                            db.weapons[1].SetActive(true);
                            break;
                        case NPCCreator.Weapons.SICKLE:
                            db.weapons[2].SetActive(true);
                            break;
                        default:
                            break;
                    }
                }

                break;
            default:
                break;
        }
    }
}
