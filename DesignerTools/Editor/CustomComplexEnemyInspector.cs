﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ComplexEnemy))]
[CanEditMultipleObjects]
public class CustomComplexEnemyInspector : Editor {
    ComplexEnemy db;
    private bool rangeVisible;
    private bool allySetup;
    private SpriteRenderer visualIndicator;
    private string rangeButtonText;
    private string allyButtonText;

    SerializedProperty forwardFinders;
    SerializedProperty horizontalFinders;
    SerializedProperty jumpFinders;
    SerializedProperty property;
    SerializedProperty allies;
    SerializedProperty runTo;

    ComplexEnemy.EnemyType previousState;
    NPCCreator npcCreator;

    private void OnEnable()
    {
        db = (ComplexEnemy)target;
        visualIndicator = db.GetComponentInChildren<SpriteRenderer>();
        rangeVisible = visualIndicator.enabled;
        allySetup = db.manualSetup;

        forwardFinders = serializedObject.FindProperty("forwardFinders");
        horizontalFinders = serializedObject.FindProperty("horizontalFinders");
        jumpFinders = serializedObject.FindProperty("jumpFinders");
        property = serializedObject.FindProperty("enemyTargets");
        allies = serializedObject.FindProperty("allies");
        runTo = serializedObject.FindProperty("runAwayPosition");

        npcCreator = db.GetComponent<NPCCreator>();
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        previousState = db.enemyType;

        if (db.enemyType == ComplexEnemy.EnemyType.Human)
        {
            if (npcCreator == null)
            {
                npcCreator = db.gameObject.AddComponent<NPCCreator>();
            }
            else if (npcCreator.hideFlags != HideFlags.None)
            {
                npcCreator.hideFlags = HideFlags.None;
            }
            
        }
        else if (db.enemyType != ComplexEnemy.EnemyType.Human)
        {
            if (npcCreator != null && npcCreator.hideFlags == HideFlags.None)
            {
                npcCreator.hideFlags = HideFlags.HideInInspector;
            }
        }

        EditorGUILayout.LabelField("Basic Values", EditorStyles.boldLabel);
        GUILayout.BeginVertical();

        db.health = EditorGUILayout.FloatField("Health", db.health);
        db.maxHealth = EditorGUILayout.FloatField("MaxHealth", db.maxHealth);
        db.lightDamage = EditorGUILayout.FloatField("Light Attack Damage", db.lightDamage);
        db.heavyDamage = EditorGUILayout.FloatField("Heavy Attack Damage", db.heavyDamage);

        GUILayout.EndVertical();
        GUILayout.Space(10);
        GUILayout.BeginVertical();

        EditorGUILayout.LabelField("ComplexEnemy Opitions", EditorStyles.boldLabel);
        db.currentState = (ComplexEnemy.State)EditorGUILayout.EnumPopup("Beginning State", db.currentState);
        db.enemyType = (ComplexEnemy.EnemyType)EditorGUILayout.EnumPopup("ComplexEnemy Type", db.enemyType);
        db.aggroState = (ComplexEnemy.AggroType)EditorGUILayout.EnumPopup("Aggro State", db.aggroState);

        if (db.aggroState == ComplexEnemy.AggroType.EnemyList && db.enemyType != ComplexEnemy.EnemyType.WhiteWolf)
        {
            EditorGUILayout.PropertyField(property, true);
            db.primaryTargetIsPlayer = EditorGUILayout.Toggle("Primary Target Is Player", db.primaryTargetIsPlayer);
            allySetup = false;
            db.packLeader = false;
            db.allies.Clear();
            db.manualSetup = false;
        }
        else
        {
            db.primaryTargetIsPlayer = false;
        }

        if (db.enemyType == ComplexEnemy.EnemyType.Human)
        {
            EditorGUILayout.LabelField("Health values to:");
            db.healthFleeAmount = EditorGUILayout.FloatField("Flee and heal", db.healthFleeAmount);
            db.healthReturnAmount = EditorGUILayout.FloatField("Return to fight", db.healthReturnAmount);
        }

        GUILayout.EndVertical();
        GUILayout.Space(10);
        GUILayout.BeginVertical();

        db.canLoseTarget = EditorGUILayout.Toggle("Can Lose Target", db.canLoseTarget);

        if (db.enemyType == ComplexEnemy.EnemyType.WhiteWolf)
        {
            EditorGUILayout.PropertyField(runTo, false);
        }

        GUILayout.Space(10);

        db.maxVisibleRange = EditorGUILayout.FloatField("Max Visible Range", db.maxVisibleRange);

        if (rangeVisible)
        {
            rangeButtonText = "Hide Visible Range";
        }
        else
        {
            rangeButtonText = "Show Visible Range";
        }

        if (GUILayout.Button(rangeButtonText))
        {
            rangeVisible = !rangeVisible;
            visualIndicator.enabled = rangeVisible;
        }

        if (GUI.changed)
        {
            visualIndicator.transform.localScale = new Vector3(db.maxVisibleRange * 2, db.maxVisibleRange * 2, 0);

            // Swapping active character
            if (previousState != db.enemyType)
            {
                SwapCharacter(db.enemyType);
            }
        }
        GUILayout.Space(10);

        db.checkTargetTimer = EditorGUILayout.FloatField("Check Target Timer", db.checkTargetTimer);
        db.attackTimer = EditorGUILayout.FloatField("Attack Timer", db.attackTimer);
        db.kickTimer = EditorGUILayout.FloatField("Kick Timer", db.kickTimer);
        db.dodgeTimer = EditorGUILayout.FloatField("Dodge Timer", db.dodgeTimer);

        GUILayout.Space(10);
        if (db.enemyType != ComplexEnemy.EnemyType.WoodCutter &&
        (db.aggroState == ComplexEnemy.AggroType.OnSightHuman || db.aggroState == ComplexEnemy.AggroType.OnSightWolf || db.aggroState == ComplexEnemy.AggroType.Provoked) ||
        db.enemyType == ComplexEnemy.EnemyType.WhiteWolf)
        {
            PackOptions();
        }

        EditorGUILayout.LabelField("Attack Locations", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(forwardFinders, true);
        EditorGUILayout.PropertyField(horizontalFinders, true);
        EditorGUILayout.PropertyField(jumpFinders, true);

        GUILayout.EndVertical();
        GUILayout.Space(20);

        serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(db);
    }

    /// <summary>
    /// Options to only avaliable to packs
    /// </summary>
    void PackOptions()
    {
        if (GUILayout.Button(allyButtonText))
        {
            allySetup = !allySetup;
            db.manualSetup = allySetup;
            if (allySetup && db.allies.Count == 0 && db.enemyType != ComplexEnemy.EnemyType.WhiteWolf)
            {
                db.allies.Add(db);
                db.packLeader = true;
            }
            if (!allySetup)
            {
                db.packLeader = false;
                db.allies.Clear();
            }
        }

        if (allySetup)
        {
            allyButtonText = "Turn off manual ally setup";
            EditorGUILayout.PropertyField(allies, true);

            if (db.enemyType != ComplexEnemy.EnemyType.WhiteWolf)
            {
                db.packLeader = EditorGUILayout.Toggle("Is leader", db.packLeader);
            }
        }
        else
        {
            allyButtonText = "Turn on manual ally setup";
        }

        if (db.enemyType == ComplexEnemy.EnemyType.Human && db.packLeader)
        {
            db.humanDistanceAngle = EditorGUILayout.IntSlider("Separator Value", db.humanDistanceAngle, 20, 60);
            if (db.humanDistanceAngle % 2 == 1)
            {
                db.humanDistanceAngle -= 1;
            }
            db.attackDelayMinMax = EditorGUILayout.Vector2Field("Attack Delay Min Max", db.attackDelayMinMax);
        }

        if (db.allies.Count > 0 && db.allies[0] != db && db.enemyType != ComplexEnemy.EnemyType.WhiteWolf)
        {
            db.allies[0] = db;
        }

        while (db.allies.Count > 5)
        {
            db.allies.RemoveAt(5);
        }

        GUILayout.Space(10);
    }

    /// <summary>
    /// Swaps the correct character to display and hides others
    /// </summary>
    /// <param name="type">Enemy type selected</param>
    void SwapCharacter(ComplexEnemy.EnemyType type)
    {
        db.wolf.SetActive(false);
        db.human.SetActive(false);
        db.whiteWolf.SetActive(false);
        db.woodCutter.SetActive(false);
        switch (type)
        {
            default:
                break;
            case ComplexEnemy.EnemyType.Human:
                db.human.SetActive(true);
                if (db.aggroState == ComplexEnemy.AggroType.OnSightHuman)
                {
                    db.aggroState = ComplexEnemy.AggroType.OnSightWolf;
                }
                break;
            case ComplexEnemy.EnemyType.WhiteWolf:
                db.whiteWolf.SetActive(true);
                break;
            case ComplexEnemy.EnemyType.Wolf:
                db.wolf.SetActive(true);
                if (db.aggroState == ComplexEnemy.AggroType.OnSightWolf)
                {
                    db.aggroState = ComplexEnemy.AggroType.OnSightHuman;
                }
                break;
            case ComplexEnemy.EnemyType.WoodCutter:
                db.woodCutter.SetActive(true);
                break;
        }
    }
}
