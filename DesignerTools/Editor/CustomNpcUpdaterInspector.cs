﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NpcUpdater))]
public class CustomNpcUpdaterInspector : Editor {

    NpcUpdater db;

    void OnEnable()
    {
        db = (NpcUpdater)target;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Force Update"))
        {
            FindandUpdate();
        }
    }

    /// <summary>
    /// Finds all npc creator in scene and forces them to update
    /// </summary>
    void FindandUpdate()
    {
        CustomNPCCreatorInspector [] npcs = FindObjectsOfType<CustomNPCCreatorInspector>();

        foreach (CustomNPCCreatorInspector npc in npcs)
        {
            npc.TurnOnSelectedItems();
        }
    }
}
