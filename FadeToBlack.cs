﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeToBlack : MonoBehaviour {

    public static FadeToBlack instance;

    public Image image;
    public float fadeInTime = 1.5f;
    public float delayTimerTillNextScene = 0.0f;
    public bool fadeToGame = true;
    public bool fadeToBlack = false;
    public bool returnToMainMenu = false;
    public bool gameEndWinWolf = false;
    public bool gameEndWinHuman = false;
    public bool gameEndLose = false;
    float alphaValue = 1;

    public void sequenceExecutor(bool _fadeToBlack)
    {
        if (_fadeToBlack)
        {
            fadeToBlack = true;
        }
        else
        {
            fadeToGame = true;
        }
    }

    // Use this for initialization
    void Start () {
        instance = this;

        image.enabled = true;

		if (fadeToBlack)
        {
            alphaValue = 0;
        }
	}
	
	// Update is called once per frame
	void Update () {

        // Delays the transfer of scenes by given time
        if (delayTimerTillNextScene > 0)
        {
            delayTimerTillNextScene -= Time.deltaTime;
            return;
        }

        if (returnToMainMenu || gameEndWinWolf || gameEndWinHuman || gameEndLose)
        {
            fadeToBlack = true;
        }

		if (fadeToBlack)
		{
            alphaValue += Time.deltaTime * fadeInTime;

            if (alphaValue >= 1)
            {
                alphaValue = 1;
                fadeToBlack = false;

                if (returnToMainMenu)
                {
                    SceneManager.LoadScene(0);
                }
                else if (gameEndWinWolf)
                {
                    SceneManager.LoadScene(5);
                }
                else if (gameEndWinHuman)
                {
                    SceneManager.LoadScene(3);
                }
                else if (gameEndLose)
                {
                    SceneManager.LoadScene(4);
                }
            }

            image.color = new Color(0, 0, 0, alphaValue);
        }
        else if (fadeToGame)
        {
            alphaValue -= Time.deltaTime * fadeInTime;

            if (alphaValue <= 0)
            {
                alphaValue = 0;
                fadeToGame = false;
            }

            image.color = new Color(0, 0, 0, alphaValue);
        }
	}
}
