﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ChangeMusic : MonoBehaviour {

    public MusicManager.MusicState setMusicTo;

	// Use this for initialization
	void Start ()
    {
        MeshRenderer temp = GetComponent<MeshRenderer>();
        if (temp != null)
        {
            temp.enabled = false;
        }
        GetComponent<Collider>().isTrigger = true;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (MusicManager.instance != null)
            {
                MusicManager.instance.ChangeMusicTo(setMusicTo, true);
            }
        }
    }
}
