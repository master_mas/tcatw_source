﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class NPCGestures : MonoBehaviour
{

    public enum GestureType
    {
        NONE,
        TALKING,
        AGREE,
        WAVE,
        ANGRY,
        SAD,
        DISMISS,
        ARM_GESTURE,
        HAND_FOWARD,
        LOOK_BEHIND,
        RELIEF_SIGH,
        ARM_GESTURE_MIRROR,
        DIE,
        INJURED_IDLE,
        INJURED_HURT,
        INJURED_STUMBLE,
        INJURED_WAVE,
        HOWL
    }

    private readonly Dictionary<GestureType, Action> controls = new Dictionary<GestureType, Action>();

    private Animator anim;

    private void Awake()
    {
        controls.Add(GestureType.NONE, None);
        controls.Add(GestureType.TALKING, Talking);
        controls.Add(GestureType.AGREE, Nod);
        controls.Add(GestureType.WAVE, Wave);
        controls.Add(GestureType.ANGRY, Angry);
        controls.Add(GestureType.SAD, Sad);
        controls.Add(GestureType.DISMISS, Dismiss);
        controls.Add(GestureType.ARM_GESTURE, ArmGesture);
        controls.Add(GestureType.HAND_FOWARD, HandFoward);
        controls.Add(GestureType.LOOK_BEHIND, LookBehind);
        controls.Add(GestureType.RELIEF_SIGH, ReliefSigh);
        controls.Add(GestureType.ARM_GESTURE_MIRROR, ArmGestureMirror);
        controls.Add(GestureType.DIE, Die);
        controls.Add(GestureType.INJURED_IDLE, Injured);
        controls.Add(GestureType.INJURED_HURT, InjuredHurt);
        controls.Add(GestureType.INJURED_STUMBLE, Stumble);
        controls.Add(GestureType.INJURED_WAVE, InjuredWave);
        controls.Add(GestureType.HOWL, Howl);
    }

    public AudioSource howl;
    public float delayTimeSeconds;

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }

    public void DynamicInvokeControl(GestureType gesture)
    {
        if (controls.ContainsKey(gesture))
            controls[gesture].Invoke();
        else
            Debug.LogWarning("No NPC Gesture Control for: " + gesture);
    }

    public void None()
    {
        anim.CrossFade("Default", 0.2f, 1);
    }

    public void Talking()
    {
        if (Random.value > 0.5)
        {
            anim.SetFloat("GestureValue", 1);
        }
        else
        {
            anim.SetFloat("GestureValue", 0);
        }
        anim.CrossFade("Talking", 0.2f, 1);
    }

    public void Nod()
    {
        if (Random.value > 0.5)
        {
            anim.SetFloat("GestureValue", 1);
        }
        else
        {
            anim.SetFloat("GestureValue", 0);
        }
        anim.Play("Agree", 1);
        anim.CrossFade("Talking", 0.2f, 1);
    }

    public void Wave()
    {
        anim.Play("Waving", 1);
    }

    public void Angry()
    {
        anim.CrossFade("Angry Gesture", 0.2f, 1);
    }

    public void ArmGesture()
    {
        anim.CrossFade("Arm Gesture", 0.2f, 1);
    }

    public void Sad()
    {
        if (Random.value > 0.5)
        {
            anim.SetFloat("GestureValue", 1);
        }
        else
        {
            anim.SetFloat("GestureValue", 0);
        }
        anim.CrossFade("Sad Idle", 0.2f, 1);
    }

    public void Dismiss()
    {
        anim.CrossFade("Dismissing Gesture", 0.2f, 1);
    }

    public void HandFoward()
    {
        anim.CrossFade("Hands Forward Gesture", 0.2f, 1);
    }

    public void LookBehind()
    {
        anim.CrossFade("Looking Behind", 0.2f, 1);
    }

    public void ReliefSigh()
    {
        anim.CrossFade("Relieved Sigh", 0.2f, 1);
    }

    public void ArmGestureMirror()
    {
        anim.CrossFade("Arm Gesture Mirror", 0.2f, 1);
    }

    public void Die()
    {
        anim.CrossFade("Death", 0.2f, 1);
    }

    public void Injured()
    {
        anim.CrossFade("Injured Idle", 0.2f, 1);
    }

    public void InjuredHurt()
    {
        anim.CrossFade("Injured Hurting Idle", 0.2f, 1);
    }

    public void Stumble()
    {
        anim.CrossFade("Injured Stumble", 0.2f, 1);
    }

    public void InjuredWave()
    {
        anim.CrossFade("Injured Wave", 0.2f, 1);
    }

    public void Howl()
    {
        anim.CrossFade("Howl", 0.2f, 1);

        if (howl != null)
        {
            Invoke("SoundPlay", delayTimeSeconds);
        }
    }

    void SoundPlay()
    {
        howl.Play();
    }
}
