﻿using UnityEngine;

public class ImageShake : MonoBehaviour
{
    private RectTransform _transform;

    private void Awake()
    {
        _transform = GetComponent<RectTransform>();
    }

    public float shakeDuration = 2f;

    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    private Vector3 originalPos;

    private void OnEnable()
    {
        originalPos = _transform.localPosition;
    }

    private void Update()
    {
        if (shakeDuration > 0)
        {
            _transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            _transform.localPosition = originalPos;
        }
    }
}