﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoralityController : MonoBehaviour
{
    private static MoralityController self = null;

    private float weight = 0;
    private bool playerOnPath = true;

    public float moralityPerSecond = 0.1f;
    public bool moralityChangeOverTime = true;
    public Vector2 moralityMinMax = new Vector2(-50,50);

    public List<Action> changeListeners = new List<Action>();

    void Awake ()
	{
	    if (self != null)
	    {
	        Debug.LogError("Can not have more than 1 " + GetType().Name + " in the scene");
            enabled = false;
	        return;
	    }

	    self = this;
	}

    void Start()
    {
        
    }

    void Update()
    {
        if (moralityChangeOverTime)
        {
            GraduallyChangeMorality();
        }
    }

    public static MoralityController Self
    {
        get { return self; }
    }

    public float Weight
    {
        get { return weight; }
        set
        {
            if (value >= 0 && weight < 0)
            {
                weight = value;
                PlayerManager.MakePlayerHuman();
                changeListeners.ForEach(x => x.Invoke());
            }
            else if(value < 0 && weight >= 0)
            {
                weight = value;
                PlayerManager.MakePlayerWolf();
                changeListeners.ForEach(x => x.Invoke());
            }

            weight = value;
//            if (Application.isEditor) Debug.Log("Morality weight set to " + weight);
            if (weight > moralityMinMax.y)
                weight = moralityMinMax.y;
            if (weight < moralityMinMax.x)
                weight = moralityMinMax.x;
        }
    }

    void OnDestroy()
    {
        if (Equals(self))
            self = null;
    }

    public bool PlayerOnPath
    {
        get { return playerOnPath; }
        set { playerOnPath = value; }
    }

    void GraduallyChangeMorality()
    {
        if (playerOnPath)
        {
            Weight += Time.deltaTime * moralityPerSecond;
        }
        else
        {
            Weight -= Time.deltaTime * moralityPerSecond;
        }
    }
}
