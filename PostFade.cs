﻿using UnityEngine;
using UnityEngine.UI;

public class PostFade : MonoBehaviour
{

    public float firstWait = 0;
    public float secondWait = 0;

    private float currentWaitFirst = 0;
    private float currentWaitSecond = 0;

    private bool[] waits = {true, false};

    private bool fadeIn = false;
    private bool fadeOut = false;

    private int currentFadeCount = 0;
    public int fadeCount = 30;

    private Color color;
    private Image image;

    void Start()
    {
        image = GetComponent<Image>();
        color = Color.white;
        color.a = 0;

        image.color = color;
    }

    void Update()
    {
        if (currentWaitFirst >= firstWait && waits[0])
        {
            waits[0] = false;
            fadeIn = true;
        }
        else if(waits[0])
            currentWaitFirst += Time.deltaTime;

        if (currentWaitSecond >= secondWait && waits[1])
        {
            waits[1] = false;
            fadeOut = true;
        }
        else if(waits[1])
            currentWaitSecond += Time.deltaTime;

        if (fadeIn)
        {
            if (currentFadeCount <= fadeCount)
            {
                color.a = 1f * (currentFadeCount / fadeCount);
                image.color = color;
                currentFadeCount++;
            }
            else
            {
                fadeIn = false;
                waits[1] = true;
                currentFadeCount = fadeCount;
            }
        }

        if (fadeOut)
        {
            if (currentFadeCount > -1)
            {
                color.a = 1f * (currentFadeCount / fadeCount);
                image.color = color;
                currentFadeCount--;
            }
            else
            {
                fadeOut = false;
            }
        }
    }
}
