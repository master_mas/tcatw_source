﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance;

    public enum PlayerState { PLAYER, WOLF };
    public static List<Action<PlayerState>> changeEventCallback = new List<Action<PlayerState>>();

    [SerializeField]
    PlayerController humanPlayer, wolfPlayer;

    [SerializeField]
    GameObject transformParticle;

    public bool playerIsHuman = true;
    
	void Awake () {
        instance = this;
    }

    private void Start()
    {
        wolfPlayer.gameObject.SetActive(false);
        humanPlayer.gameObject.SetActive(true);
    }


    public static PlayerController GetActivePlayer ()
    {
        if (instance == null)
        {
            GameObject temp = new GameObject();
            temp.AddComponent<PlayerManager>();
            PlayerController[] refs = FindObjectsOfType<PlayerController>();
            if (refs.Length == 0)
            {
                Debug.LogError("No player found!");
            }
            if (refs.Length == 1)
            {
                Debug.LogWarning("Only one player found");
                instance.wolfPlayer = refs[0];
                instance.humanPlayer = refs[0];
            }
            else
            {
                if (refs[0].name.Contains("Wolf"))
                {
                    instance.wolfPlayer = refs[0];
                    instance.humanPlayer = refs[1];
                }
                else
                {
                    instance.wolfPlayer = refs[1];
                    instance.humanPlayer = refs[0];
                }
            }
        }
        if (instance.playerIsHuman)
        {
            return instance.humanPlayer;
        }
        else
        {
            return instance.wolfPlayer;
        }
    }

    public static void MakePlayerHuman ()
    {
        if (!instance.playerIsHuman)
        {
            Instantiate(instance.transformParticle, instance.wolfPlayer.transform.position, Quaternion.identity);

            // swap the player models
            instance.wolfPlayer.gameObject.SetActive(false);
            instance.humanPlayer.gameObject.SetActive(true);

            // update data
            instance.humanPlayer.transform.position = instance.wolfPlayer.transform.position;
            instance.humanPlayer.transform.rotation = instance.wolfPlayer.transform.rotation;
            instance.humanPlayer.currentHealth = instance.wolfPlayer.currentHealth;
           
            instance.playerIsHuman = true;
            if (MusicManager.instance != null)
            {
                MusicManager.instance.ChangeMusicTo(MusicManager.MusicState.forrest, false);
            }
            changeEventCallback.ForEach(x => x(PlayerState.PLAYER));
        }
    }

    public static void MakePlayerWolf ()
    {
        if (instance.playerIsHuman)
        {

            Instantiate(instance.transformParticle, instance.humanPlayer.transform.position, Quaternion.identity);
            // swap the player models
            instance.wolfPlayer.gameObject.SetActive(true);
            instance.humanPlayer.gameObject.SetActive(false);

            // update data
            instance.wolfPlayer.transform.position = instance.humanPlayer.transform.position;
            instance.wolfPlayer.transform.rotation = instance.humanPlayer.transform.rotation;
            instance.wolfPlayer.currentHealth = instance.humanPlayer.currentHealth;

            instance.playerIsHuman = false;

            if (MusicManager.instance != null)
            {
                MusicManager.instance.ChangeMusicTo(MusicManager.MusicState.forrest, false);
            }
            changeEventCallback.ForEach(x => x(PlayerState.WOLF));
        }
    }

	public static bool IsPlayerHuman ()
    {
        return instance.playerIsHuman;
    }

    public GameObject GetPlayer()
    {
        if (instance.playerIsHuman)
        {
            // return human
            return instance.humanPlayer.gameObject;
        }
        else
        {
            // return wolf
            return instance.wolfPlayer.gameObject;
        }
    }
}
