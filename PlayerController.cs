﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public static PlayerController instance
    {
        get
        {
            return PlayerManager.GetActivePlayer();
        }
    }

    // value to be used in testing to set the player speed really fast. -J
    public bool testSpeed = false;

    bool debugMode = false;

    // values related to moving and animating the character
    CharacterController CC;
    public Animator anim;
    float currentSpeed;
    Quaternion lookDir;
    Quaternion lastInput;

    // the players weapon/s
    public Weapon rightWeapon, leftWeapon;

    // sounds
    [SerializeField]
    SoundPlayer footsteps, weaponSwing, dodgeSound;
    [SerializeField]
    VolumeByDistancePlayer weapSwingSound;

    // cape stuff
    [SerializeField]
    Renderer capeRenderer;
    Material capeMat;
    [SerializeField]
    ParticleSystem capeParticles;

    [HideInInspector]
    public bool isInputAllowed = true, slowMovement = false;
    // ###################################################################################
    // Adam added this, feel free to change later on when morality is implemented
    // Just let me know, please
    public bool playerIsHuman;
    // ###################################################################################

    public float attackRotationSpeed = 1;
    public float attackMovementSpeed = 0.75f;
    public float lightAttackDamage = 1;
    public float heavyAttackDamage = 2;

    public float slowedMovementSpeed=0.75f;

    public int maxHealth = 10;
    public float healAmount = 0.5f;
    public float hitHealDelay = 5.0f;
    private bool isHealing = false;
    float _currentHealth;
    public float currentHealth
    {
        get
        {
            return _currentHealth;
        }
        set
        {
            // updates the cape when health is modded -J
            if (capeMat != null)
                capeMat.SetFloat("_fade", value / maxHealth);

            _currentHealth = value;
        }
    }

    bool dead = false;
    bool invulnerable;
    
	void Start ()
    {
        debugMode = Application.isEditor;
        capeMat = capeRenderer.material;
        anim = GetComponent<Animator>();
        _currentHealth = maxHealth;

        if(leftWeapon != null)
            leftWeapon.SetUpDamage(lightAttackDamage, heavyAttackDamage);
        if (rightWeapon != null)
            rightWeapon.SetUpDamage(lightAttackDamage, heavyAttackDamage);
    }
	
	void Update ()
    {
        if (!dead)
        {
            SlowPlayerMovementTimer();

            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                debugMode = !debugMode;
            }

            if (debugMode)
            {
                // Manually change morality
                if (Input.GetKeyDown(KeyCode.KeypadPlus))
                {
                    MoralityController.Self.Weight += 1;
                }

                if (Input.GetKeyDown(KeyCode.KeypadMinus))
                {
                    MoralityController.Self.Weight -= 1;
                }

                if (Input.GetKeyDown(KeyCode.O))
                {
                    Hurt(1, true);
                }
                
                if (Input.GetKeyDown(KeyCode.L))
                {
                    GlobalSoundPlayer.Play("HumanNpcHurt", transform.position);
                }
            }

            //Added by Sam for Sequencer
            if (isInputAllowed)
            {
                // organises input into a more palatable format
                Vector3 inputAxis = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

                // used for slerping the character, doesn't do it if the input isn't definite from the player
                Quaternion inputRot;
                if (inputAxis.magnitude > 0.1f)
                {
                    inputRot = Quaternion.LookRotation(inputAxis, Vector3.up);
                    lastInput = inputRot;
                }
                else
                {
                    inputRot = lastInput;
                }

                // if the players input is large enough, sets aimedSpeed to the players input magnitude
                float aimedSpeed;
                if (inputAxis.magnitude > 0.05f)
                {
                    aimedSpeed = inputAxis.magnitude;
                    if (anim.layerCount >= 2)
                    {
                        if (!anim.GetCurrentAnimatorStateInfo(1).IsName("Movement"))
                        {
                            aimedSpeed = attackMovementSpeed;
                        }
                    }
                }
                else
                {
                    aimedSpeed = 0;
                    currentSpeed = 0;
                }
                // if the chatacter is the wolf and is attacking
                if (anim.layerCount >= 2 && !anim.GetCurrentAnimatorStateInfo(1).IsName("Movement"))
                {
                    lookDir = Quaternion.Slerp(lookDir, inputRot, Time.deltaTime * attackRotationSpeed);
                }
                // if the character is walking...
                else if (anim.isActiveAndEnabled && anim.GetCurrentAnimatorStateInfo(0).IsName("Movement"))
                {
                    // quickly rotate them to face input dir
                    lookDir = Quaternion.Slerp(lookDir, inputRot, Time.deltaTime * 360);
                }
                else
                {
                    // else rotate them slowly so players can make minor corrections while attacking/rolling
                    lookDir = Quaternion.Slerp(lookDir, inputRot, Time.deltaTime * attackRotationSpeed);
                }

                // actually rotate the object
                gameObject.transform.rotation = lookDir;
                // move the movement speed towards the speed the player wants to move at
                currentSpeed = Mathf.MoveTowards(currentSpeed, aimedSpeed, Time.deltaTime * 5);

                // if we are in the editor, let the player move at turbo speed, update the animator with this value
                if (Application.isEditor && testSpeed)
                {
                    if (anim.isActiveAndEnabled)
                        anim.SetFloat("Forward", currentSpeed * 3);
                }
                else
                {
                    if (anim.isActiveAndEnabled)
                    {
                        float moddedSpeed = Mathf.Clamp(currentSpeed, 0, slowedMovementSpeed); 
                        anim.SetFloat("Forward", Mathf.Lerp(currentSpeed,moddedSpeed, slowMoveTimer));
                    }
                }

                // if the trigger attacks/dodges based on input, the animator will handle the rest
                if (Input.GetButtonDown("Fire1"))
                {
                    anim.SetTrigger("Attack");
                }
                if (Input.GetButtonDown("Fire2"))
                {
                    anim.SetTrigger("HeavyAttack");
                }

                if ((Input.GetAxis("LeftTrigger") != 0 || Input.GetAxis("RightTrigger") != 0) && !slowMovement)
                {
                    if (aimedSpeed < 0.1f)
                    {
                        anim.SetTrigger("BackDodge");
                    }
                    else
                    {
                        anim.SetTrigger("Dodge");
                    }
                }
            }
            else
            {
                anim.SetFloat("Forward", 0);
            }

            if (isHealing)
            {
                HealOverTime();
            }
        }
    }

    public void SlowPlayerMovement ()
    {
        slowMoveTimer = 1;
    }

    // this is written like this to be sure that a character can never be stuck in slow movement
    float slowMoveTimer = 0;
    void SlowPlayerMovementTimer ()
    {
        if (slowMoveTimer > 0)
        {
            slowMovement = true;
            slowMoveTimer -= Time.deltaTime*2;
        }else
        {
            slowMovement = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("destructible") && anim.GetCurrentAnimatorStateInfo(0).IsName("Roll"))
        {
            collision.gameObject.SendMessage("Hurt", SendMessageOptions.DontRequireReceiver);
        }
    }

        // Slowly heal the health back to full
        void HealOverTime ()
    {
        currentHealth += Time.deltaTime * healAmount;
        if (currentHealth % 1 == 0)
        {
            currentHealth = currentHealth;
        }
        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
            isHealing = false;
        }
        // health check and turn bool on after hit
    }

    // counts this number down, makes the player vulnerable when it hits 0
    float invulnerableTimer = 0;
    IEnumerator InvulnerableTimer()
    {
        while (invulnerableTimer > 0)
        {
            invulnerable = true;
            invulnerableTimer -= Time.deltaTime;
            yield return null;
        }
        invulnerable = false;
    }

    /// <summary>
    /// The player becomes invulnerable for x seconds, if the player is set to invulnerable again in this time, they will be set to the higher number.
    /// </summary>
    /// <param name="time">The time the player will be invulnerable for in seconds</param>
    void MakeInvulnerable(float time = 1)
    {
        invulnerable = true;
        invulnerableTimer = Mathf.Max(time, invulnerableTimer);
        StartCoroutine(InvulnerableTimer());
    }


    /// <summary>
    /// Handles the player actually taking damage, as well as checking to see if it should take damage. -J
    /// </summary>
    /// <param name="i">The amount of damage the player takes, defaults to 1</param>
    /// <param name="heavy">If the attack is heavy, the player will be stunned</param>
    public bool Hurt (float i = 1, bool heavy = false)
    {
        // checks to see if the player should take the damage.
        if (!invulnerable && !dead && isInputAllowed)
        {
            // ends the attacks on both weapons to keep the weapons from being stuck on.
            AttackEndLeft();
            AttackEndRight();

            // Handles visual effects
            TimeManager.PauseTime();
            CamRig.CamShake(i * 0.15f);

            StopAllCoroutines();
            StartCoroutine(CapeVisualEffects(currentHealth));

            GlobalSoundPlayer.Play("ClawImpact", transform.position);
            GlobalSoundPlayer.Play("ElsieHurt", transform.position);

            // deals the damage, makes the player animate accordingly
            currentHealth -= 1;
            isHealing = false;

            StartCoroutine(WaitForHealDelay(hitHealDelay));

            anim.Play("Hurt");
            if (currentHealth <= 0)
            {
                dead = true;
                anim.Play("Death");
                FadeToBlack.instance.gameEndLose = true;
            }
            else if (heavy) 
            {
                anim.Play("Hurt");
            }

            // to keep the player from being combo'd to death, makes them unable to take damage for a bit.
            MakeInvulnerable();
            return true;
        }
        return false;
    }


    // Plays particles, makes the cape glow -J
    IEnumerator CapeVisualEffects (float health)
    {
        if (capeParticles != null)
        {
            capeParticles.Play();
        }
        float timer = 1;
        while (timer > 0)
        {
            capeMat.SetFloat("_EmissionPower", timer*2);
            timer -= Time.deltaTime;
            yield return null;
        }
    }

    // Delay for healing health
    IEnumerator WaitForHealDelay(float time)
    {
        float timer = time;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        isHealing = true;
    }

    //###################
    /// <summary>
    /// These functions are all called in the animator.
    /// They basically handle the weapon being turned off or on,
    /// as well as the code for handling their effects.
    /// One major issue we could run into with this code is if the animator
    /// changes state before "AttackEnd" is called, then the weapon will stay 
    /// "On", causing the player to have a stick that does damage when it touches things.
    /// 
    /// If this is a problem, We can add something to the basic anim state to turn
    /// the weapon off.
    /// </summary>

    public void AttackStartLeft ()
    {
        if (leftWeapon != null)
        {
            weaponSwing.Play();
            weapSwingSound.Play();
            leftWeapon.on = true;
        }
    }

    public void AttackStartRight ()
    {
        if (rightWeapon != null)
        {
            weaponSwing.Play();
            weapSwingSound.Play();
            rightWeapon.on = true;
        }
    }

    public void AttackEndLeft ()
    {
        if (leftWeapon != null)
        {
            leftWeapon.heavy = false;
            weapSwingSound.Stop();
            leftWeapon.sparkEmitter.Stop();
            leftWeapon.on = false;
        }
    }

    public void AttackEndRight ()
    {
        if (rightWeapon != null)
        {
            rightWeapon.heavy = false;
            weapSwingSound.Stop();
            rightWeapon.sparkEmitter.Stop();
            rightWeapon.on = false;
        }
    }

    public void WeaponsOff()
    {
        AttackEndLeft();
        AttackEndRight();
    }

    // called in the animator to denote more powerful attacks -J
    public void HeavyAttackOn()
    {
        if (rightWeapon != null) 
            rightWeapon.heavy = true;
        if (leftWeapon != null)
            leftWeapon.heavy = true;
    }

    public void FacePlayerToInput ()
    {
        Vector3 inputAxis = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        lookDir = Quaternion.LookRotation(inputAxis, Vector3.up);
        gameObject.transform.rotation = lookDir;
    }

    public void Roll ()
    {
        if (dodgeSound != null)
            dodgeSound.Play();
    }

    // called in the animator, not spawning particles on feet so nothing to say which foot is down for now -J
    public void FootstepEffect ()
    {
        footsteps.Play();
    }
    //###################
}
