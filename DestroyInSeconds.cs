﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInSeconds : MonoBehaviour {
    public float timeUntilDestroy;
	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, timeUntilDestroy);
    }
}
